<?php
//ข้อมูลการเก้บเกี่ยว

namespace App;

use Illuminate\Database\Eloquent\Model;

class harvest_information extends Model
{
    protected $table = "harvest_information";

    protected $fillable = [
        'famer_id','age_potato','high_meter','calculate_potato',
        'weight_potato','Evaluate_products','Trend_productivity','Provider_Name',
        'Data_collector_name',
    ];
    protected $primaryKey='id';
}
