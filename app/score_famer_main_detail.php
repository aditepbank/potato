<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class score_famer_main_detail extends Model
{
    protected $table = "score_famer_main_detail";

    protected $fillable = [
        'score_criteria_main_id','criteria_detail_id','form_id','topic_score_detail','answer','main_id','detail_id','topic_form','famer_id','topic_id'
    ];
    protected $primaryKey='id';

    public function get_Criteria_detail(){
        return $this->hasOne(Criteria_detail::class, 'id', 'criteria_detail_id');
    }
    public function famer(){
        return $this->hasOne(famer::class, 'id', 'famer_id');
    }
}
