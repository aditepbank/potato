<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Season extends Model
{
    protected $table="Season";
    protected $primaryKey = "season_id";
    protected $fillable = [
        'season_id', 'season_name', 'season_detail',
    ];
}
