<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Criteria extends Model
{
    protected $table="criteria";
    protected $fillable = [
        'id', 'criteria_season_name', 'criteria_season_detail',
    ];
}
