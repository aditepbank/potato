<?php
//ข้อมูลจัดการแปลงปลูกมันสำปะหลัง
namespace App;

use Illuminate\Database\Eloquent\Model;

class plantation_management_information extends Model
{
    protected $table = "plantation_management_information";

    protected $fillable = [
        'famer_id','plow_da','plow_pea','Foundation_fertilizer',
        'Groove','about_farm','give_water','give_water_calculate',
        'Make_up_fertilizer','Make_up_fertilizer_about','Weeding',
        'Weeding_about_people','Weeding_about_people_cal','Month_harvest'
    ];
    protected $primaryKey='id';
}
