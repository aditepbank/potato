<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\famer;
use App\score_famer_main;
use App\Criteria_main;
use App\score_famer_main_detail;


class score_famer extends Model
{
    protected $table = "score_famer";

    protected $fillable = [
        'famer_id','criteria_score','criteria_score','form_criteria_id',
    ];
    protected $primaryKey='id';

    public function famer(){
        return $this->hasOne(famer::class, 'id', 'famer_id');
    }

    public function score_famer_main_detailx(){
        return $this->hasOne(score_famer_main_detail::class, 'famer_id', 'famer_id');
    }

    public function score_famer_main(){
        return $this->hasMany(score_famer_main::class, 'score_id', 'id');
    }
    public function score_famer_main_1(){
        return $this->hasOne(score_famer_main::class, 'score_id', 'id');
    }
    public function score_famer_main_2(){
        return $this->hasOne(score_famer_main::class, 'score_id', 'id')->where('topic',2)->orderBy('topic_score', 'DESC');
    }
    public function score_famer_main_3(){
        return $this->hasOne(score_famer_main::class, 'score_id', 'id')->where('topic',3)->orderBy('topic_score', 'DESC');
    }
    public function score_famer_main_4(){
        return $this->hasOne(score_famer_main::class, 'score_id', 'id')->where('topic',4)->orderBy('topic_score', 'DESC');
    }
    public function score_famer_main_detail(){
        return $this->hasMany(score_famer_main_detail::class, 'score_criteria_main_id', 'id');
    }
    public function planting_information(){
        return $this->hasOne(planting_information::class, 'famer_id', 'famer_id');
    }

    public function criteria()
    {
        return $this->hasOne(Criteria::class, 'id', 'form_criteria_id');
    }
    public function score_famer_main_Where_topic(){
        return $this->hasOne(score_famer_main::class, 'score_id', 'id')->orderBy('topic_score', 'DESC');
    }
    public function criteria_main()
    {
        return $this->hasOne(Criteria_main::class, 'id', 'form_criteria_id');
    }
    public function harvest_information(){
        return $this->hasOne(harvest_information::class, 'famer_id', 'famer_id');
    }
    public function plantation_management_information(){
        return $this->hasOne(plantation_management_information::class, 'famer_id', 'famer_id');
    }
    public function garden_detail(){
        return $this->hasOne(garden_detail::class, 'id', 'id');
    }
    public function score_famer_main_1_min(){
        return $this->hasOne(score_famer_main::class, 'score_id', 'id')->where('topic',1)->orderBy('topic_score', 'ASC');
    }
    public function score_famer_main_2_min(){
        return $this->hasOne(score_famer_main::class, 'score_id', 'id')->where('topic',2)->orderBy('topic_score', 'ASC');
    }
    public function score_famer_main_3_min(){
        return $this->hasOne(score_famer_main::class, 'score_id', 'id')->where('topic',3)->orderBy('topic_score', 'ASC');
    }
    public function score_famer_main_4_min(){
        return $this->hasOne(score_famer_main::class, 'score_id', 'id')->where('topic',4)->orderBy('topic_score', 'ASC');
    }
}
