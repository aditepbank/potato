<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\score_famer_main_detail;

class Criteria_main extends Model
{
    protected $table="criteria_main";
    protected $fillable = [
        'id', 'criteria_main_name','criteria_main_Weight', 'criteria_id','id_main',
    ];

    public function criteria_main(){
        return $this->hasMany(Criteria_detail::class, 'criteria_main_id', 'id');
    }
    public function score_famer_main_detail(){
        return $this->hasMany(score_famer_main_detail::class, 'score_criteria_main_id', 'id');
    }

    public function getcriteria(){
        return $this->hasOne(Criteria::class, 'id', 'criteria_id');
    }

}
