<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\score_famer_main_detail;

class score_famer_main extends Model
{
    protected $table = "score_famer_main";

    protected $fillable = [
        'score_id','topic','topic_score','famer_id','form_id'
    ];
    protected $primaryKey='id';

    public function score_famer_main_detail(){
        return $this->hasMany(score_famer_main_detail::class, 'score_criteria_main_id', 'id');
    }
    public function Criteria_main(){
        return $this->hasOne(Criteria_main::class, 'id', 'topic');
    }
    public function famer(){
        return $this->hasOne(famer::class, 'id', 'famer_id');
    }
}
