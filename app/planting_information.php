<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class planting_information extends Model
{
    protected $table = "planting_information";

    protected $fillable = [
        'famer_id','breed_type','date_plant','Planting_distance','Planting_characteristics'
    ];
    protected $primaryKey='id';
}
