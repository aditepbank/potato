<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\garden_detail;
use App\score_famer;
use App\planting_information;

class famer extends Model
{
    protected $table = "famer";

    protected $fillable = [
        'famer_code','fname','lname','gender','status','birthday','number_chil','adderss','education','phone_number','name_prefix'
    ];
    protected $primaryKey='id';

    public function garden_detail(){
        return $this->hasOne(garden_detail::class, 'famer_id', 'id');
    }
    public function planting_information(){
        return $this->hasOne(planting_information::class, 'famer_id', 'id');
    }


}
