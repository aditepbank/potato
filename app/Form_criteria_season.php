<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Form_criteria_season extends Model
{
    protected $table="form_criteria_season";
    protected $fillable = [
        'id', 'famer_id','criteria_id', 'status',
    ];

    public function getfarmer(){
        return $this->hasOne(famer::class, 'id', 'famer_id'); 
    }
}
