<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\famer;
use App\score_famer;
use App\score_famer_main;
use App\score_famer_main_detail;
use App\Criteria_main;
use App\PlantingInformationController;
use App\Criteria;
use App\Criteria_detail;
use App\Form_criteria_season;
use Illuminate\Support\Facades\DB;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function farmerscorereport()
    {
        return view('report.farmerscorereport');
    }

    public function detailfarmerscorereport($id,$form)
    {
        $score_famer = score_famer::with('famer')->find($id);
        $score_famer_main = score_famer_main::with('score_famer_main_detail','Criteria_main')->where('score_id',$id)->get();
        $formname = Criteria::find($form);
        
        return view('report.famer-score-private',compact('score_famer','score_famer_main','formname'))->with(['id'=>$id,'form'=>$form]);
    }

    public function report_score_famer($id,$form)
    {   
        // dd($id);
        $score_famer = score_famer::with('famer')->find($id);
        $score_famer_main = score_famer_main::with('score_famer_main_detail','Criteria_main')->where('score_id',$id)->get();
        $formname = Criteria::find($form);
        return view('report.report-print',compact('score_famer','score_famer_main','formname'))->with(['id'=>$id,'form'=>$form]);
    }


    public function plan_total()
    {
        // dd($request->all());
        $score_famer =score_famer::with('famer','planting_information','plantation_management_information','harvest_information')->orderBy('criteria_score', 'DESC')->get();
        // dd($score_famer);
        return view('report.famer-plant-info-total',compact('famer','score_famer','plantation_management_information','harvest_information'));
    }

    public function chooseseasonmax()
    {
        // $Criteria = Criteria::get();
        $Report = Score_famer::with('criteria')->groupBy('form_criteria_id')->get();
        // dd($Report);
        return view('report.choose_season_max',compact('Report'));
    }

    public function chooseseasonmin()
    {
        // $Criteria = Criteria::get();
        $Report = Score_famer::with('criteria')->groupBy('form_criteria_id')->get();
        // dd($Report);
        return view('report.choose_season_min',compact('Report'));
    }

    public function chooseseasonmaxmin()
    {
        // $Criteria = Criteria::get();
        $Report = Score_famer::with('criteria')->groupBy('form_criteria_id')->get();
        // dd($Report);
        return view('report.choose_season_max_min',compact('Report'));
    }


    public function chooseseasonreport()
    {
        // $Criteria = Criteria::get();
        $Report = Score_famer::with('criteria')->groupBy('form_criteria_id')->get();
        // dd($Report);
        return view('report.choose_season_report',compact('Report'));
    }

    public function reportpointfarmer($id)
    {
        $criteriaMain = Criteria_main::All();
        $Criteria = Criteria::find($id);
        $scorce = score_famer::where('form_criteria_id',$id)->with('famer','score_famer_main')->orderBy('criteria_score', 'DESC')->get();
       
        return view('report.report_ponit_season_farmer',compact('scorce','criteriaMain','Criteria'))->with(['cri_id'=>$id]);
    }

    public function introducefarmerschooseseason()
    {
        $Report = Score_famer::with('criteria')->groupBy('form_criteria_id')->get();
        return view('report.introduce_farmers_season',compact('Report'));
    }

    public function introduce($id,Request $request)
    {
        // dd($request->amount);
        $amount = $request->amount;
        $Criteria = Criteria::find($id);
        $score = score_famer::where('form_criteria_id',$id)->with('famer','score_famer_main')->orderBy('criteria_score','DESC')->limit($amount)->get();
        return view('report.introduce_farmers',compact('score','Criteria','topic'));
    }

    public function selectnumberfarmers($id,Request $request)
    {
        $amount = (int) $request->get('amount');
        $Criteria = Criteria::find($id);
        // $formid = Form_criteria_season::find($Criteria_main->criteria_id);
        return view('report.select_number_farmer',compact('Criteria'))->with('testid',$id);
    }


    public function report_score_famer_total($id)
    {
        //dd($id);
        $scorce = score_famer::where('form_criteria_id',$id)->with('famer','score_famer_main')->get();
        return view('report.farmerscorereport-print')->with('testid',$id);
    }


    public function plan_detail($id)
    {
        //dd($id);
        //$famer=famer::with('planting_information')->get();
        $score_famer=score_famer::with('famer','planting_information')->find($id);
        // dd($score_famer);
        return view('report.famer-plant-detail',compact('score_famer'));
    }
    
    public function showGraph()
    {
        return view('report.report-graph');
    }

    public function reportpointindex()
    {
        $Criteria = Criteria::get();
        // dd($id);
        $scorce = score_famer::where('form_criteria_id','LIKE','%' .'1' . '%')->with('famer','score_famer_main')->orderBy('criteria_score', 'DESC')->limit(10)->get();
        return view('index.score_famer',compact('scorce','Criteria','topic'));
    }

    public function reportpointfarmer_total()
    {
       
        $scorce = score_famer::with('famer','score_famer_main')->orderBy('criteria_score', 'DESC')->get();
         //dd($scorce);
        return view('report.report_ponit_famer',compact('scorce'));
    }

    public function indextransport($id)
    {
        
        // dd($id);
        $array_result = [];
        $get_criteria_main = criteria_main::where('criteria_id', $id)->get();
        $array_result = $get_criteria_main->toArray();
        // dd($array_result);
        foreach($get_criteria_main as $key => $item){
            $results = DB::select( DB::raw("SELECT tt.score_criteria_main_id,
            tt.famer_id,
            tt.topic_id,
            tt.criteria_detail_id,
            tt.form_id,
            score_famer_main.topic_score,
            tt.topic_score_detail,
            criteria_detail.criteria_detail_name, 
            famer.name_prefix,
            famer.fname,
            famer.lname,
            famer.adderss,
            famer.phone_number,
            tt.answer,
            tt.created_at
                    FROM score_famer_main_detail tt
                    INNER JOIN score_famer_main ON tt.score_criteria_main_id = score_famer_main.id
                    INNER JOIN
                        (SELECT famer_id, min(topic_score_detail) AS min_score_detail
                        FROM score_famer_main_detail where topic_id = " . $item->id . " and form_id = ". $id ."
                        GROUP BY famer_id) groupedtt 
                    ON tt.famer_id = groupedtt.famer_id 
                    AND tt.topic_score_detail = groupedtt.min_score_detail 
                    
                    LEFT JOIN criteria_detail ON tt.criteria_detail_id = criteria_detail.id
                    INNER JOIN famer ON tt.famer_id = famer.id
                         WHERE tt.topic_id = " . $item->id . " and tt.form_id = ". $id ."
                         GROUP BY famer.id
                         ORDER BY score_famer_main.topic_score asc
                         LIMIT 10
            "));
            // array_push($array_result, $results);
            $array_result[$key] += ['results' => $results];
        } // end foreach
        // dd($array_result);

     
    
        return view('report.transport',compact('array_result'))->with(['form_id' => $id]);
    }

    public function indextransport1($form_id, $topic_id)
    {   
        $Criteria_main = Criteria_main::find($topic_id);
        $results = DB::select( DB::raw("SELECT tt.score_criteria_main_id,
        tt.famer_id,
        tt.topic_id,
        tt.criteria_detail_id,
        tt.form_id,
        score_famer_main.topic_score,
        tt.topic_score_detail,
        criteria_detail.criteria_detail_name, 
        famer.name_prefix,
        famer.fname,
        famer.lname,
        famer.adderss,
        famer.phone_number,
        tt.answer,
        tt.created_at
                FROM score_famer_main_detail tt
                INNER JOIN score_famer_main ON tt.score_criteria_main_id = score_famer_main.id
                INNER JOIN
                    (SELECT famer_id, min(topic_score_detail) AS min_score_detail
                    FROM score_famer_main_detail where topic_id = " . $topic_id . " and form_id = ". $form_id ."
                    GROUP BY famer_id) groupedtt 
                ON tt.famer_id = groupedtt.famer_id 
                AND tt.topic_score_detail = groupedtt.min_score_detail 
                
                LEFT JOIN criteria_detail ON tt.criteria_detail_id = criteria_detail.id
                INNER JOIN famer ON tt.famer_id = famer.id
                     WHERE tt.topic_id = " . $topic_id . " and tt.form_id = ". $form_id ."
                     GROUP BY famer.id
                     ORDER BY score_famer_main.topic_score ASC
                     LIMIT 20
        "));
        return view('report.transport1',compact('scorce','results','Criteria_main'))->with(['form_id' => $form_id]);
     }
    


    public function report_product_best($form_id, $topic_id)
    {     
        // dd($topic_id);
        $Criteria_main = Criteria_main::find($topic_id);
        $results = DB::select( DB::raw("SELECT tt.score_criteria_main_id,
        tt.famer_id,
        tt.topic_id,
        tt.criteria_detail_id,
        tt.form_id,
        score_famer_main.topic_score,
        tt.topic_score_detail,
        criteria_detail.criteria_detail_name, 
        famer.name_prefix,
        famer.fname,
        famer.lname,
        famer.adderss,
        famer.phone_number,
        tt.answer,
        tt.created_at
                FROM score_famer_main_detail tt
                INNER JOIN score_famer_main ON tt.score_criteria_main_id = score_famer_main.id
                INNER JOIN
                    (SELECT famer_id, MAX(topic_score_detail) AS max_score_detail
                    FROM score_famer_main_detail where topic_id = " . $topic_id . " and form_id = ". $form_id ."
                    GROUP BY famer_id) groupedtt 
                ON tt.famer_id = groupedtt.famer_id 
                AND tt.topic_score_detail = groupedtt.max_score_detail 
                
                LEFT JOIN criteria_detail ON tt.criteria_detail_id = criteria_detail.id
                INNER JOIN famer ON tt.famer_id = famer.id
                     WHERE tt.topic_id = " . $topic_id . " and tt.form_id = ". $form_id ."
                     ORDER BY score_famer_main.topic_score DESC
                     LIMIT 10
        
        "));
        $data = array();
        foreach($results as $key => $item){
            $resultx[$item->famer_id] = $item;
            $data[$item->famer_id][] = $item->criteria_detail_name;
        }

        // dd($results);
        return view('report.report-product-best',compact('scorce','results','Criteria_main','data','resultx'))->with(['form_id' => $form_id]);
    }

    public function criterriasearch(Request $request,$id){
        // dd($request->select);
        $sCserach = array();

        $sCserach = score_famer::where('form_criteria_id','LIKE','%' . $id . '%')
        ->with(['famer',
        'score_famer_main_Where_topic' => function($query) use($request){
            return $query->where(["topic" =>$request->select],'ASC');
        }
        ])->limit($request->quantity)->get();
       // dd($sCserach);
        return view('report.report_select_main_criteria',compact('sCserach'));
    }
    public function select_famer(Request $request)
    {
        $arr = $request->all();
        unset($arr["_token"]);
        $select_detail = array();
        // dd($arr);
        foreach ($arr as $key => $value) {
            # code...
            array_push($select_detail,$key);
        }
        // dd($select_detail);
        $score_detail = score_famer_main_detail::with('famer')->whereIn('criteria_detail_id',$select_detail)->where('answer',1)->get();
        $Criteria_dt = Criteria_detail::whereIn('id',$select_detail)->get();
        
        //dd($Criteria_dt);
        return view('report.select-famer',compact('score_detail','Criteria_dt'));
    }


    // เกษตรกรที่มีความร่วมมือต่ำ
    public function choose_season_cooperation_low()
    {
        $Report = Score_famer::with('criteria')->groupBy('form_criteria_id')->get();
        return view('report.choose_season_cooperation',compact('Report'));
    }



    public function farmer_cooperation_low($id)
    {
        $score_famer_main_detail = DB::select(DB::raw("select main.*,cd.criteria_detail_name,cd.criteria_detail_low from (
            select b.name_prefix,b.fname,b.lname,b.adderss,b.phone_number,a.* from score_famer_main_detail a
            left join famer b on b.id = a.famer_id
            WHERE a.criteria_detail_id in (1,2,3,4,5,6) and a.answer >= 0.50 and a.form_id = " . $id . "
            UNION
            select b.name_prefix,b.fname,b.lname,b.adderss,b.phone_number,a.* from score_famer_main_detail a
            left join famer b on b.id = a.famer_id
            WHERE a.criteria_detail_id in (11,12,13,14) and a.answer <= 0.50 and a.form_id = " . $id . ") as main
            left join criteria_detail cd on cd.id = main.criteria_detail_id
            order by main.famer_id,main.criteria_detail_id asc"));
        $countx = [];
        $famery = [];
        foreach($score_famer_main_detail as $item){
            if($item->answer <=0.5){
                $famery[$item->famer_id][] = $item;
            }
            $countx[$item->criteria_detail_id][] = $item;
        }
        // dd($score_famer_main_detail);
        return view('report.farmer_cooperation_low',compact('score_famer_main_detail','countx','famery'))->with(['id'=>$id]);
        
    }

    
    public function chooseseasonreportx()
    {
        // $Criteria = Criteria::get();
        $Report = Score_famer::with('criteria')->groupBy('form_criteria_id')->get();
        // dd($Report);
        return view('report.choose_season_reportx',compact('Report'));
    }

    public function reportpointtransport($id)
    {
        // dd($id);
        $score_famer_main_detail = DB::select(DB::raw("select main.*,cd.criteria_detail_name,cd.criteria_detail_low from (
            select b.name_prefix,b.fname,b.lname,b.adderss,b.phone_number,a.* from score_famer_main_detail a
            left join famer b on b.id = a.famer_id
            WHERE a.criteria_detail_id in (1,2,3,4,5,6) and a.answer >= 0.50 and a.form_id = " . $id . "
            UNION
            select b.name_prefix,b.fname,b.lname,b.adderss,b.phone_number,a.* from score_famer_main_detail a
            left join famer b on b.id = a.famer_id
            WHERE a.criteria_detail_id in (7,8,9,10) and a.answer <= 0.50 and a.form_id = " . $id . ") as main
            left join criteria_detail cd on cd.id = main.criteria_detail_id
            order by main.famer_id,main.criteria_detail_id asc"));
        // dd($score_famer_main_detail_low);
        
        $famery = [];
        $countx = [];
        foreach($score_famer_main_detail as $item){
            //array_push($countx[$item->criteria_detail_id],$item);
            if($item->answer <= 0.5){
                $famery[$item->famer_id][] = $item;
            }
            $countx[$item->criteria_detail_id][] = $item;
        }
        // dd($score_famer_main_detail);
        return view('report.report_min_transport',compact('score_famer_main_detail','countx','famery'))->with(['id'=>$id]);
    }
    
    
    public function report_cuttom($id)
    {   
        $score=score_famer::with('famer')->where(["form_criteria_id" => $id])->orderBy('criteria_score','DESC')->first();
        //dd($score);
        $score_detail=score_famer_main_detail::with('get_Criteria_detail')->where(['famer_id' => $score->famer_id,"form_id" => $id])->orderBy('topic_score_detail','DESC')->limit(3)->get();
        //dd($score_detail);
        $score_min=score_famer::with('famer')->where(["form_criteria_id" => $id])->orderBy('criteria_score','ASC')->first();
        $score_detail_min=score_famer_main_detail::with('get_Criteria_detail')->where(['famer_id' => $score_min->famer_id,"form_id" => $id])->orderBy('topic_score_detail','ASC')->limit(3)->get();
        // dd($score_min);

        $Criteria = Criteria::find($id);
        // dd($score);
        return view('report.report-cuttom',compact('score','score_maim','score_detail','score_min','score_maim_min','score_detail_min','score_detailx','Criteria'))->with(['id'=>$id]);
    }

    public function report_criteria_best($id)
    {
        // dd($id);
        $array_result = [];
        $get_criteria_main = criteria_main::where('criteria_id', $id)->get();
        $array_result = $get_criteria_main->toArray();
        foreach($get_criteria_main as $key => $item){
            // dd($item->id_main);
            $results = DB::select( DB::raw("SELECT tt.score_criteria_main_id,
            tt.famer_id,
            tt.topic_id,
            tt.criteria_detail_id,
            score_famer_main.topic_score,
            tt.topic_score_detail,
            criteria_detail.criteria_detail_name, 
            famer.name_prefix,
            famer.fname,
            famer.lname,
            famer.adderss,
            famer.phone_number,
            tt.answer,
            tt.created_at
                    FROM score_famer_main_detail tt
                    INNER JOIN score_famer_main ON tt.score_criteria_main_id = score_famer_main.id
                    INNER JOIN
                        (SELECT famer_id, max(topic_score_detail) AS max_score_detail
                        FROM score_famer_main_detail where topic_id = " . $item->id . "
                        GROUP BY famer_id) groupedtt 
                    ON tt.famer_id = groupedtt.famer_id 
                    AND tt.topic_score_detail = groupedtt.max_score_detail 
                    
                    LEFT JOIN criteria_detail ON tt.criteria_detail_id = criteria_detail.id
                    INNER JOIN famer ON tt.famer_id = famer.id
                         WHERE tt.topic_id = " . $item->id . "
                         GROUP BY famer.id
                         ORDER BY score_famer_main.topic_score desc
                         LIMIT 10
            "));
            // dd($results);
            // array_push($array_result, $results);
            $array_result[$key] += ['results' => $results];
        } // end foreach
        // dd($array_result);
        return view('report.report-criteria',compact('array_result'))->with(['form_id' => $id]);
    }
    
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
