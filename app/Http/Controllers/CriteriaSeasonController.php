<?php

namespace App\Http\Controllers;

use App\Form_criteria_season;
use Illuminate\Http\Request;
use App\Criteria;
use App\famer;

class CriteriaSeasonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Criteria = Criteria::get();
        return view('criteria_season.form_criteria_season',compact('Criteria'));
    }

    public function addfamer($id)
    {
        // dd($id);
        $farmer_id = collect([]);
        $criteria = Form_criteria_season::where('criteria_id',$id)->get('famer_id');
        $Criteria = Criteria::find($id);
        foreach($criteria as $item){
            $farmer_id->push($item->famer_id);
            // dd($farmer_id->toArray());
        }
        // dd($criteria->toArray());
        $famer = famer::whereNotIn('id',$farmer_id)->get();
        alert()->success('', 'เพิ่มสำเร็จ');
        return view('criteria_season.add_famer_criteria_season',compact('famer','Criteria'))->with('formid',$id)->with('testid', $id);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        // dd($request->all());
        $arr = $request->all();
        unset($arr["_token"]);
        unset($arr["add_length"]);
        $title;
        // dd($arr);
        foreach($arr as $key => $item){
            // dd($key);
            Form_criteria_season::create([
                'famer_id' => $key,
                'criteria_id' => $id
            ]);
        }

        return redirect()->back()->with('success','บันทึกข้อมูลเรียบร้อย');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Form_criteria_season  $form_criteria_season
     * @return \Illuminate\Http\Response
     */
    public function show(Form_criteria_season $form_criteria_season)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Form_criteria_season  $form_criteria_season
     * @return \Illuminate\Http\Response
     */
    public function edit(Form_criteria_season $form_criteria_season)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Form_criteria_season  $form_criteria_season
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Form_criteria_season $form_criteria_season)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Form_criteria_season  $form_criteria_season
     * @return \Illuminate\Http\Response
     */
    public function destroy(Form_criteria_season $form_criteria_season)
    {
        //
    }
}
