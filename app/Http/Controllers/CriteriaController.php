<?php

namespace App\Http\Controllers;

use App\Criteria;
use App\Criteria_main;
use App\Criteria_detail;
use Illuminate\Http\Request;


class CriteriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index()
    {
        $Criteria = Criteria::get();
        return view('rules.rules_list',compact('Criteria'));
    }


    public function createcriteria()
    {
        $Criteria = Criteria::get();
        return view('rules.rules_add',compact('Criteria'));
    }

    public function info($id)
    {
        // dd($id);
        $Criteria_main = Criteria_main::where('criteria_id', $id)->with('criteria_main')->get();
        $Criteria  = Criteria::find($id);
        // dd($Criteria_main);
        return view('rules.infocriteria',compact('Criteria_main','Criteria'));
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */



    public function store(Request $request)
    {
        \Validator::make($request->all(),[
            'criteria_season_name	' => 'required',
            'criteria_season_detail' => 'required',
        ]);

        $Criteria = new Criteria([
            'criteria_season_name' => $request->input('criteria_season_name'),
            'criteria_season_detail' => $request->input('criteria_season_detail'),
        ]);
        $Criteria->save();
        return redirect()->route('criteria')->with('success','บันทึกข้อมูลเรียบร้อย');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Criteria  $criteria
     * @return \Illuminate\Http\Response
     */
    public function show(Criteria $criteria)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Criteria  $criteria
     * @return \Illuminate\Http\Response
     */
    public function edit(Criteria $criteria)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Criteria  $criteria
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $update = Criteria::find($request->id)->update(['criteria_season_name'=>$request->criteria_season_name,'criteria_season_detail'=>$request->criteria_season_detail]);
        // dd($test);
        return redirect()->back()->with('success','บันทึกข้อมูลเรียบร้อย');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Criteria  $criteria
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        // dd($request->id);
        $delete = Criteria::findOrfail($request->id);
        $delete->delete();
        return back();
    }
}
