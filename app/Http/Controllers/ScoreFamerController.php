<?php

namespace App\Http\Controllers;

use App\score_famer;
use Illuminate\Http\Request;
use App\score_famer_main;

class ScoreFamerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function savePointFamer(Request $request)
    {   
       
    }
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\score_famer  $score_famer
     * @return \Illuminate\Http\Response
     */
    public function show(score_famer $score_famer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\score_famer  $score_famer
     * @return \Illuminate\Http\Response
     */
    public function edit(score_famer $score_famer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\score_famer  $score_famer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, score_famer $score_famer)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\score_famer  $score_famer
     * @return \Illuminate\Http\Response
     */
    public function destroy(score_famer $score_famer)
    {
        //
    }
}
