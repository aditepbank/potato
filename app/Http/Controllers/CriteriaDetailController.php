<?php

namespace App\Http\Controllers;

use App\Criteria_detail;
use App\Criteria;
use App\Criteria_main;
use Illuminate\Http\Request;

class CriteriaDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id, $testid)
    {
        // dd($id);
        $Criteria_detail = Criteria_detail::where('id_season',$testid)->where('criteria_main_id',$id)->get();
        $Criteria_main = Criteria_main::find($id);
        $Criteria = Criteria::find($Criteria_main->criteria_id);

        $t = Criteria_main::where('criteria_id', $id)->with('criteria_main')->get();
        $tt  = Criteria::find($id);
        // dd($testid);
        return view('rules.criteriadetail',compact('Criteria_detail','Criteria','Criteria_main','t','tt'))->with(['id'=>$id, 'testid'=>$testid]);
    }

    public function createcriteriadetail($id)
    {
        return view('rules.create_criteriadetail')->with(['id'=>$id, 'testid'=>$testid]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id, $testid)
    {
        // dd($testid);
        \Validator::make($request->all(),[
            'id_detail' => 'required',
            'criteria_detail_name	' => 'required',
            'criteria_detail_low' => 'required',
            'criteria_detail_middle	' => 'required',
            'criteria_detail_high' => 'required',
            'criteria_detail_Weight	' => 'required',
        ]);

        // dd($request->all());
        $Criteria_detail = new Criteria_detail([
            'id_detail' => $request->input('detail'),
            'criteria_detail_name' => $request->input('criteria_detail_name'),
            'criteria_detail_low' => $request->input('criteria_detail_low'),
            'criteria_detail_middle' => $request->input('criteria_detail_middle'),
            'criteria_detail_high' => $request->input('criteria_detail_high'),
            'criteria_detail_Weight' => $request->input('criteria_detail_Weight'),
            'criteria_main_id' => $id,
            'id_season' => $testid,
        ]);
        // dd($Criteria_detail);
        $Criteria_detail->save();
        return redirect()->route('criteriadetail',[$id, $testid])->with('success','บันทึกข้อมูลเรียบร้อย');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Criteria_detail  $criteria_detail
     * @return \Illuminate\Http\Response
     */
    public function show(Criteria_detail $criteria_detail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Criteria_detail  $criteria_detail
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // with('garden_detail')->where(['id' => $id])->first()
        $Criteria_detail = Criteria_detail::find($id);
        // dd($id);
        return view('rules.edit_Criteriadetail',compact('Criteria_detail'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Criteria_detail  $criteria_detail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        // dd($request->all());
        $update = Criteria_detail::find($request->id)->update(
            [ 'criteria_detail_name'=>$request->criteria_detail_name,
              'criteria_detail_low'=>$request->criteria_detail_low,
              'criteria_detail_middle'=>$request->criteria_detail_middle,
              'criteria_detail_high'=>$request->criteria_detail_high,
              'criteria_detail_Weight'=>$request->criteria_detail_Weight ]);
        // dd($update);
        return redirect()->back()->with('success','บันทึกข้อมูลเรียบร้อย');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Criteria_detail  $criteria_detail
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        // dd($request->id);
        $delete = Criteria_detail::findOrfail($request->id);
        $delete->delete();
        return back();
    }
}
