<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\garden_detail;
use App\famer;
use App\Criteria_main;
use App\Criteria_detail;
use App\Criteria;
use App\score_famer;
use App\score_famer_main;
use App\score_famer_main_detail;
use App\Form_criteria_season;


class RateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $famer = famer::with('garden_detail')->get();
        $formcriteriaseason = Form_criteria_season::get();
        // dd($formcriteriaseason);
        return view('rate_ Agriculture.rate_Agriculture-list',compact('famer','formcriteriaseason'));
    }
    public function rate_famer($id)
    {   
        $formcriteriaseason = Form_criteria_season::find($id);
        $famer = famer::with('garden_detail')->find($formcriteriaseason->famer_id);
        $Criteria = Criteria::find($formcriteriaseason->criteria_id);
        $session = $formcriteriaseason->criteria_id;
        $Criteria_main = Criteria_main::where('criteria_id',$formcriteriaseason->criteria_id)->with(['criteria_main' => function($query) use ($session) {
            return $query->where(['id_season' => $session]);
        }
        ])->get();
        return view('rate_ Agriculture.rate_add_point',compact('famer','Criteria_main','Criteria'));
    }

    public function rate_famer_cal(Request $request,$id)
    {
        
        $Form_criteria = Form_criteria_season::find($id);
        $arr = $request->all();
        
        unset($arr["_token"]);
        unset($arr["save"]);
        $total = 0;
        $title = array();
        foreach($arr as $key => $item){
            $arrx = explode("_",$key);
            $cm = Criteria_detail::find($arrx[1]);
            $totalx = $cm->criteria_detail_Weight * $item;
            $total += $totalx;
            $title[$arrx[0]][$arrx[1]]['score'] = $totalx;
            $title[$arrx[0]][$arrx[1]]['answer'] = $item;
        }
        
        $sf = score_famer::create([
            "famer_id" => $Form_criteria->famer_id,
            "criteria_score" => $total,
            "form_criteria_id" => $Form_criteria->criteria_id,
        ]);
        foreach($title as $key => $item){
            // dd($key);
            $main = 0;
            foreach($item as $keyx => $itemx){
                $main += $itemx['score'];
            }
            $mainx = score_famer_main::create([
                "famer_id" => $Form_criteria->famer_id,
                "form_id" => $Form_criteria->criteria_id,
                "score_id" => $sf->id,
                "topic" => $key,
                "topic_score" => $main,
            ]);
            foreach($item as $keyx => $itemx){
                score_famer_main_detail::create([
                    "score_criteria_main_id" => $mainx->id,
                    "famer_id" => $Form_criteria->famer_id,
                    "form_id" => $Form_criteria->criteria_id,
                    "topic_id" => $key,
                    "criteria_detail_id" => $keyx,
                    "topic_score_detail" => $itemx['score'],
                    "answer"=> $itemx['answer']
                ]);
            }
        }
        // dd($main);
        $Form_criteria->update(['status'=>'success']);
        alert()->success('', 'ประเมินเรียบร้อย !');
        return redirect()->route('choose');
    }

    public function choose()
    {
        $Criteria = Criteria::get();
        return view('rate_ Agriculture.choose_season_criteria',compact('Criteria'));
    }


    public function choosefarmer($id)
    {
        $Form_criteria_season = Form_criteria_season::where('status','start')->where('criteria_id',$id)->with('getfarmer')->get();
        $Criteria = Criteria::find($id);
        return view('rate_ Agriculture.choose_farmer_criteria',compact('Form_criteria_season','Criteria'))->with('testid', $id);
    }

    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('rate_ Agriculture.rate_add_point');
    }
    public function scorce()
    {   
        $scorce = score_famer::with('famer','score_famer_main')->orderBy('criteria_score', 'DESC')->get();
        return view('report.farmerscorereport',compact('scorce'));
        
    }
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
