<?php

namespace App\Http\Controllers;

use App\planting_information;
use Illuminate\Http\Request;
use App\famer;
use App\plantation_management_information;
use App\harvest_information;

class PlantingInformationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {   
        $famer =famer::where(['id' => $id])->first();
        //dd($famer);
        return view('famer.famer-add-plan-info',compact('famer'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request ,$id)
    {   
        //dd($id);
        // dd($request->all());
        $this->validate($request, [
            
            //planting_information
            'breed_type' => ['required'],
            'date_plant' => ['required'],
            'Planting_distance' => ['required'],
            'Planting_characteristics' => ['required'],

            //plantation_management_information
            'plow_da' => ['required'],
            'plow_pea' => ['required'],
            'Foundation_fertilizer' => ['required'],
            'Groove' => ['required'],
            'about_farm' => ['required'],
            'give_water' => ['required'],
            'give_water_calculate' => ['required'],
            'Make_up_fertilizer' => ['required'],
            'Make_up_fertilizer_about' => ['required'],
            'Weeding' => ['required'],
            'Weeding_about_people' => ['required'],
            'Weeding_about_people_cal' => ['required'],
            'Month_harvest' => ['required'],

            //harvest_information
        ]);
        $planting_information=planting_information::create([
            
            'famer_id' => $id,
            'breed_type' => $request->get('breed_type'),
            'date_plant' => $request->get('date_plant'),
            'Planting_distance' => $request->get('Planting_distance'),
            'Planting_characteristics' => $request->get('Planting_characteristics'),   
            
        ]);
        $plantation_management_information=plantation_management_information::create([
            
            'famer_id' => $id,
            'plow_da' => $request->get('plow_da'),
            'plow_pea' => $request->get('plow_pea'),
            'Foundation_fertilizer' => $request->get('Foundation_fertilizer'),
            'Groove' => $request->get('Groove'), 
            'about_farm' => $request->get('about_farm'), 
            'give_water' => $request->get('give_water'),
            'give_water_calculate' => $request->get('give_water_calculate'),
            'Make_up_fertilizer' => $request->get('Make_up_fertilizer'),
            'Make_up_fertilizer_about' => $request->get('Make_up_fertilizer_about'), 
            'Weeding' => $request->get('Weeding'),
            'Weeding_about_people' => $request->get('Weeding_about_people'),
            'Weeding_about_people_cal' => $request->get('Weeding_about_people_cal'),
            'Month_harvest' => $request->get('Month_harvest'),     
            
        ]);
        $harvest_information=harvest_information::create([
            
            'famer_id' => $id,
            'age_potato' => $request->get('age_potato'),
            'plow_pea' => $request->get('plow_pea'),
            'high_meter' => $request->get('high_meter'),
            'calculate_potato' => $request->get('calculate_potato'), 
            'weight_potato' => $request->get('weight_potato'), 
            'Evaluate_products' => $request->get('Evaluate_products'),
            'Trend_productivity' => $request->get('Trend_productivity'),
            'Provider_Name' => $request->get('Provider_Name'),
            'Data_collector_name' => $request->get('Data_collector_name'),      
            
        ]);
        //dd($harvest_information);
        return redirect()->route('famer-list')->with('success','บันทึกสำเร็จ');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\planting_information  $planting_information
     * @return \Illuminate\Http\Response
     */
    public function show(planting_information $planting_information)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\planting_information  $planting_information
     * @return \Illuminate\Http\Response
     */
    public function edit(planting_information $planting_information)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\planting_information  $planting_information
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, planting_information $planting_information)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\planting_information  $planting_information
     * @return \Illuminate\Http\Response
     */
    public function destroy(planting_information $planting_information)
    {
        //
    }
}
