<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Criteria_main;
use App\Criteria;

class RulesController extends Controller
{
    public function indexrules(){
        $Criteria = Criteria::get();
        return view('rules.rules_list',compact('Criteria'));
    }

    public function rulesadd(){
        return view('rules.rules_add');
    }
    public function setrules(){
        return view('rules.setrules');
    }
}
