<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\famer;
use App\score_famer;
use App\score_famer_main;
use App\score_famer_main_detail;
use App\Criteria_main;
use App\PlantingInformationController;
use App\Criteria;
use App\Criteria_detail;
use App\Form_criteria_season;
use Illuminate\Support\Facades\DB;

class PrintController extends Controller
{
    public function score_famer($id)
    {
        // dd($id);
        $criteriaMain = Criteria_main::All();
        $Criteria = Criteria::find($id);
        $scorce = score_famer::where('form_criteria_id',$id)->with('famer','score_famer_main')->orderBy('criteria_score', 'DESC')->get();
        // $Criteria = Criteria::find($id);
        // dd($Criteria);
        return view('print.score-famer-print',compact('scorce','criteriaMain','Criteria'))->with(['cri_id'=>$id]);
    }
    public function criteria_max($form_id, $topic_id)
    {     
        //dd($topic_id);
        $Criteria_main = Criteria_main::find($topic_id);
        $results = DB::select( DB::raw("SELECT tt.score_criteria_main_id,
        tt.famer_id,
        tt.topic_id,
        tt.criteria_detail_id,
        score_famer_main.topic_score,
        tt.topic_score_detail,
        criteria_detail.criteria_detail_name, 
        famer.name_prefix,
        famer.fname,
        famer.lname,
        famer.adderss,
        famer.phone_number,
        tt.answer,
        tt.created_at
                FROM score_famer_main_detail tt
                INNER JOIN score_famer_main ON tt.score_criteria_main_id = score_famer_main.id
                INNER JOIN
                    (SELECT famer_id, MAX(topic_score_detail) AS max_score_detail
                    FROM score_famer_main_detail where topic_id = " . $topic_id . "
                    GROUP BY famer_id) groupedtt 
                ON tt.famer_id = groupedtt.famer_id 
                AND tt.topic_score_detail = groupedtt.max_score_detail 
                
                LEFT JOIN criteria_detail ON tt.criteria_detail_id = criteria_detail.id
                INNER JOIN famer ON tt.famer_id = famer.id
                     WHERE tt.topic_id = " . $topic_id . "
                     ORDER BY score_famer_main.topic_score DESC
                     LIMIT 10
        
        "));

        // dd($results);
        return view('print.criteria-max-print',compact('scorce','results','Criteria_main'))->with(['form_id' => $form_id,'topic_id'=>$topic_id]);
    }

    public function criteria_min($form_id, $topic_id)
    {   
        // dd($topic_id);
        $Criteria_main = Criteria_main::find($topic_id);
        $results = DB::select( DB::raw("
        SELECT tt.score_criteria_main_id,
        tt.famer_id,
        tt.topic_id,
        tt.criteria_detail_id,
        score_famer_main.topic_score,
        tt.topic_score_detail,
        criteria_detail.criteria_detail_name, 
        famer.name_prefix,
        famer.fname,
        famer.lname,
        famer.adderss,
        famer.phone_number,
        tt.answer,
        tt.created_at
                FROM score_famer_main_detail tt
                INNER JOIN score_famer_main ON tt.score_criteria_main_id = score_famer_main.id
                INNER JOIN
                    (SELECT famer_id, min(topic_score_detail) AS min_score_detail
                    FROM score_famer_main_detail where topic_id = " . $topic_id . "
                    GROUP BY famer_id) groupedtt 
                ON tt.famer_id = groupedtt.famer_id 
                AND tt.topic_score_detail = groupedtt.min_score_detail 
                
                LEFT JOIN criteria_detail ON tt.criteria_detail_id = criteria_detail.id
                INNER JOIN famer ON tt.famer_id = famer.id
                     WHERE tt.topic_id = " . $topic_id . "
                     GROUP BY famer.id
                     ORDER BY score_famer_main.topic_score ASC
                     LIMIT 10
        "));
        return view('print.criteria-min-print',compact('scorce','results','Criteria_main'))->with(['form_id' => $form_id]);
    }
    public function famer_max_min($id)
    {   
        //  dd($id);
        $score=score_famer::with('famer')->orderBy('criteria_score','DESC')->first();
        $score_detail=score_famer_main_detail::with('get_Criteria_detail')->where('famer_id',$score->famer_id)->orderBy('topic_score_detail','DESC')->limit(3)->get();

        $score_min=score_famer::with('famer')->orderBy('criteria_score','ASC')->first();
        $score_detail_min=score_famer_main_detail::with('get_Criteria_detail')->where('famer_id',$score_min->famer_id)->orderBy('topic_score_detail','ASC')->limit(3)->get();
        // dd($score_detail);

        $Criteria = Criteria::find($id);
        
        
        return view('print.famer-max-min-print',compact('score','score_maim','score_detail','score_min','score_maim_min','score_detail_min','score_detailx','Criteria'))->with(['id' => $id]);
    }
    public function transport_low($id)
    {   
        // dd($id);
        $score_famer_main_detail = DB::select(DB::raw("select main.*,cd.criteria_detail_name from (
            select b.name_prefix,b.fname,b.lname,b.adderss,b.phone_number,a.* from score_famer_main_detail a
            left join famer b on b.id = a.famer_id
            WHERE a.topic_id in (1,2) and a.answer >= 0.50
            UNION
            select b.name_prefix,b.fname,b.lname,b.adderss,b.phone_number,a.* from score_famer_main_detail a
            left join famer b on b.id = a.famer_id
            WHERE a.topic_id in (3) and a.answer <= 0.50) as main
            left join criteria_detail cd on cd.id = main.criteria_detail_id
            order by main.famer_id,main.topic_id,main.criteria_detail_id asc"));

        
        // dd($detail);
        
        $famery = [];
        $countx = [];
        foreach($score_famer_main_detail as $item){
            //array_push($countx[$item->criteria_detail_id],$item);
            if($item->answer <= 0.5){
                $famery[$item->famer_id][] = $item;
            }
            $countx[$item->criteria_detail_id][] = $item;
        }
        // dd($famery);
        return view('print.transport-low-print',compact('score_famer_main_detail','countx','famery'))->with(['id'=>$id]);
    }
    public function cooperation_low($id)
    {
        $score_famer_main_detail = DB::select(DB::raw("select main.*,cd.criteria_detail_name from (
            select b.name_prefix,b.fname,b.lname,b.adderss,b.phone_number,a.* from score_famer_main_detail a
            left join famer b on b.id = a.famer_id
            WHERE a.topic_id in (1,2) and a.answer >= 0.50
            UNION
            select b.name_prefix,b.fname,b.lname,b.adderss,b.phone_number,a.* from score_famer_main_detail a
            left join famer b on b.id = a.famer_id
            WHERE a.topic_id in (4) and a.answer <= 0.50) as main
            left join criteria_detail cd on cd.id = main.criteria_detail_id
            order by main.famer_id,main.topic_id,main.criteria_detail_id asc"));
        $countx = [];
        $famery = [];
        foreach($score_famer_main_detail as $item){
            if($item->answer <=0.5){
                $famery[$item->famer_id][] = $item;
            }
            $countx[$item->criteria_detail_id][] = $item;
        }
        // dd($score_famer_main_detail);
        return view('print.cooperation-low-print',compact('score_famer_main_detail','countx','famery'))->with(['id'=>$id]);
        
    }
    

}