<?php

namespace App\Http\Controllers;

use App\plantation_management_information;
use Illuminate\Http\Request;

class PlantationManagementInformationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\plantation_management_information  $plantation_management_information
     * @return \Illuminate\Http\Response
     */
    public function show(plantation_management_information $plantation_management_information)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\plantation_management_information  $plantation_management_information
     * @return \Illuminate\Http\Response
     */
    public function edit(plantation_management_information $plantation_management_information)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\plantation_management_information  $plantation_management_information
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, plantation_management_information $plantation_management_information)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\plantation_management_information  $plantation_management_information
     * @return \Illuminate\Http\Response
     */
    public function destroy(plantation_management_information $plantation_management_information)
    {
        //
    }
}
