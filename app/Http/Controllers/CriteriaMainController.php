<?php

namespace App\Http\Controllers;

use App\Criteria_main;
use App\Criteria;
use Illuminate\Http\Request;

class CriteriaMainController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        // dd($id);
        $Criteria_main = Criteria_main::where('criteria_id',$id)->get();
        $Criteria = Criteria::find($id);
        // dd($Criteria);
        return view('rules.criteriamain',compact('Criteria_main','Criteria'))->with('testid', $id);
    }

    public function back()
    {
        return view('rules.criteriamain');
    }

    public function createcriteriamain($id)
    {
        // dd($id);
        return view('rules.createcriteriamain')->with('testid',$id);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        // dd($id);
        \Validator::make($request->all(),[
            'id_main' => 'required',
            'criteria_main_name	' => 'required',
            'criteria_main_Weight' => 'required',
        ]);

        $Criteria_main = new Criteria_main([
            'id_main' => $request->input('id_main'),
            'criteria_main_name' => $request->input('criteria_main_name'),
            'criteria_main_Weight' => $request->input('criteria_main_Weight'),
            'criteria_id' => $id,
        ]);
        $Criteria_main->save();
        return redirect()->route('criteriamain',$id)->with('success','บันทึกข้อมูลเรียบร้อย');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Criteria_main  $criteria_main
     * @return \Illuminate\Http\Response
     */
    public function show(Criteria_main $criteria_main)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Criteria_main  $criteria_main
     * @return \Illuminate\Http\Response
     */
    public function edit(Criteria_main $criteria_main)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Criteria_main  $criteria_main
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        // dd($request->all());
        $update = Criteria_main::find($request->id)->update(
            ['criteria_main_name'=>$request->criteria_main_name,
            'criteria_main_Weight'=>$request->criteria_main_Weight]);
        // dd($test);
        return redirect()->back()->with('success','บันทึกข้อมูลเรียบร้อย');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Criteria_main  $criteria_main
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        // dd($request->id);
        $delete = Criteria_main::findOrfail($request->id);
        $delete->delete();
        return back();
    }
}
