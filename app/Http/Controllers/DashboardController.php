<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\famer;
use App\score_famer;
use App\score_famer_main;
use App\score_famer_main_detail;
use App\Criteria_main;
use App\PlantingInformationController;
use App\Criteria;
use App\Criteria_detail;
use App\Form_criteria_season;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function test()
    {
        
        // $season = criteria::get();
        $season = criteria::orderBy('id', 'DESC')->get();
        // $famer = Form_criteria_season::where('criteria_id',1)->count();
        // $famerx = Form_criteria_season::where('criteria_id',1)->where('status','success')->count();
        // $famery = Form_criteria_season::where('criteria_id',1)->where('status','start')->count();

        $score1 = score_famer_main_detail::where('form_id',1)->where('topic_id',1)->where('answer',0.2)->count();
        $scorex1 = score_famer_main_detail::where('form_id',1)->where('topic_id',1)->where('answer',0.6)->count();
        $scorey1 = score_famer_main_detail::where('form_id',1)->where('topic_id',1)->where('answer',1)->count();

        $score2 = score_famer_main_detail::where('form_id',1)->where('topic_id',2)->where('answer',0.2)->count();
        $scorex2 = score_famer_main_detail::where('form_id',1)->where('topic_id',2)->where('answer',0.6)->count();
        $scorey2 = score_famer_main_detail::where('form_id',1)->where('topic_id',2)->where('answer',1)->count();

        $score3 = score_famer_main_detail::where('form_id',1)->where('topic_id',3)->where('answer',0.2)->count();
        $scorex3 = score_famer_main_detail::where('form_id',1)->where('topic_id',3)->where('answer',0.6)->count();
        $scorey3 = score_famer_main_detail::where('form_id',1)->where('topic_id',3)->where('answer',1)->count();

        $score4 = score_famer_main_detail::where('form_id',1)->where('topic_id',4)->where('answer',0.2)->count();
        $scorex4 = score_famer_main_detail::where('form_id',1)->where('topic_id',4)->where('answer',0.6)->count();
        $scorey4 = score_famer_main_detail::where('form_id',1)->where('topic_id',4)->where('answer',1)->count();

        $array = array();

        $arrayx['1'] = [$score1,$scorex1,$scorey1];
        $arrayx['2'] = [$score2,$scorex2,$scorey2];
        $arrayx['3'] = [$score3,$scorex3,$scorey3];
        $arrayx['4'] = [$score4,$scorex4,$scorey4];

        $array = [
            // 'famer' => $famer,
            // 'famerx' => $famerx,
            // 'famery' => $famery,
            'score1' => $score1,
            'scorex1' => $scorex1,
            'scorey1' => $scorey1,
            'score2' => $score2,
            'scorex2' => $scorex2,
            'scorey2' => $scorey2,
            'score3' => $score3,
            'scorex3' => $scorex3,
            'scorey3' => $scorey3,
            'score4' => $score4,
            'scorex4' => $scorex4,
            'scorey4' => $scorey4,
        ];

     
        // dd($season);
        // return view('dashboard.dashboard',compact('array','arrayx','season'));
        return view('dashboard.dashboard',compact('arrayx','season'));
    }

    public function get_data_dashboard($id = null)
    {
        $array = [];
        $Criteria  = Criteria::find($id);
        $famer = Form_criteria_season::where('criteria_id',$id)->count();
        $famerx = Form_criteria_season::where('criteria_id',$id)->where('status','success')->count();
        $famery = Form_criteria_season::where('criteria_id',$id)->where('status','start')->count();
        $array = array_merge(['famer' => $famer], ['famerx' => $famerx], ['famery' => $famery], ['Criteria' => $Criteria]);
        return response()->json($array);
    }

    public function index()
    {
        $criteriaMain = Criteria_main::All();
        $scorce = score_famer::where('form_criteria_id')->with('famer','score_famer_main')->orderBy('criteria_score', 'DESC')->get();
        // dd($scorce);
        return view('testreport.report',compact('scorce','criteriaMain'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
