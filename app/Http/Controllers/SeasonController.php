<?php

namespace App\Http\Controllers;

use App\Season;
use Illuminate\Http\Request;

class SeasonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function homeseason()
    {
        $Season = Season::get();
        return view('season.theseason',compact('Season'));
    }

    public function createseason()
    {
        $Season = Season::get();
        return view('season.createseason',compact('Season'));
    }

    public function infoseason()
    {
        return view('season.infoseason');
    }
    
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        \Validator::make($request->all(),[
            'season_name' => 'required',
            'season_detail' => 'required',
        ]);

        $Season = new Season([
            'season_name' => $request->input('season_name'),
            'season_detail' => $request->input('season_detail'),
        ]);
        $Season->save();
        return redirect()->back()->with('success','บันทึกข้อมูลเรียบร้อย');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Season  $season
     * @return \Illuminate\Http\Response
     */
    public function show(Season $season)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Season  $season
     * @return \Illuminate\Http\Response
     */
    public function edit(Season $season)
    {
        //
    }

    public function edits($season_id)
    {
        $Season = Season::Where(['season_id' => $season_id])->first();
        return view('season.editseason',compact('Season'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Season  $season
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $season_id)
    {
        // dd($request->all());
        Season::find($season_id)->update($request->only(['season_name','season_detail']));

        return redirect()->route('homeseason')->with('success','บันทึกข้อมูลเรียบร้อย');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Season  $season
     * @return \Illuminate\Http\Response
     */
    public function destroy($season_id)
    {
        Season::find($season_id)->delete();
        return redirect()->route('homeseason')->with('success','ลบข้อมูล');
    }
}
