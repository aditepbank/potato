<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Charts;
use App\famer;
use DB;

class ChartController extends Controller
{
    public function index()
    {
    	$famer = famer::where(DB::raw("(DATE_FORMAT(created_at,'%Y'))"),date('Y'))
    				->get();
        $chart = Charts::database($famer, 'bar', 'highcharts')
			      ->title("Monthly new Register famer")
			      ->elementLabel("Total famer")
			      ->dimensions(1000, 500)
			      ->responsive(false)
			      ->groupByMonth(date('Y'), true);
        return view('chart',compact('chart'));
    }

    function pre_chart()
    {
        $chart_prev= famer::all();
        $name='';
        $count='';
        $ab_count='';
        foreach ($chart_prev as $row)
        {
            $name=$name."'".$row['gender']."'," ;
            $count=$count."'".$row['famer_code']."'," ;
            $ab_count=$ab_count."'".$row['famer_code']."'," ;

        }
        $name = substr($name,0,strlen($name)-1);
        $count = substr($count,0,strlen($count)-1);
        $ab_count = substr($ab_count,0,strlen($ab_count)-1);
        $out = array(
        'gender'=>$name,
        'famer_code'=>$count,
        'id'=>$ab_count
        );
        echo json_encode($out);

    }
    public function test()
    {
        $data = DB::table('score_famer_main_detail')
            ->select(
                DB::raw('answer as answer'),
                DB::raw('count(*) as number'))
            ->groupBy('answer')
            ->get();
            $array[] = ['answer','Number'];
            foreach ($data as $key => $value) {
                $array[$key++] = [$value->answer, $value->number];
            }
        return view('dashboard.dashboard')->with('answer', json_encode(
            $array
        ));
    }

    

}
