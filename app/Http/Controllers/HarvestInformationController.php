<?php

namespace App\Http\Controllers;

use App\harvest_information;
use Illuminate\Http\Request;

class HarvestInformationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\harvest_information  $harvest_information
     * @return \Illuminate\Http\Response
     */
    public function show(harvest_information $harvest_information)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\harvest_information  $harvest_information
     * @return \Illuminate\Http\Response
     */
    public function edit(harvest_information $harvest_information)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\harvest_information  $harvest_information
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, harvest_information $harvest_information)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\harvest_information  $harvest_information
     * @return \Illuminate\Http\Response
     */
    public function destroy(harvest_information $harvest_information)
    {
        //
    }
}
