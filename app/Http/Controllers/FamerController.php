<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\garden_detail;
use App\famer;
use App\planting_information;
use App\plantation_management_information;
use App\harvest_information;
class FamerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //dd($request-all());
        $famer=famer::with('garden_detail')->get();
       
        // dd($famer);
        return view('famer.famer-list',compact('famer'));
    }
    public function index_plan()
    {
        //dd($request-all());
        $famer=famer::with('garden_detail')->doesntHave('planting_information')->get();
        $query  = famer::doesntHave('planting_information')->get();
        //dd($famer);
        return view('famer.famer-list-plan',compact('famer','$query'));
    }

    public function information($id)
    {
        //dd($id);
        $famer = famer::with('garden_detail')->where(['id' => $id])->first();
        //dd($famer);
        return view('famer.famer-list-total',compact('famer'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('famer.famer-add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $this->validate($request, [

            //famer
            // 'famer_code' => ['required'],
            'name_prefix' =>['required'],
            'fname' => ['required'],
            'lname' => ['required'],
            'gender' => ['required'],
            'birthday' => ['required'],
            'adderss' => ['required'],
            'status' => ['required'],
            'number_chil' => ['required'],
            'education' => ['required'],
            'phone_number' => ['required'],

            //gadern
            'distance' => ['required'],
            'latitude' => ['required'],
            'longtitude' => ['required'],
            'total_garden' => ['required'],
            'soli_look' => ['required'],
            'HP' => ['required'],
            'enviroment' => ['required'],
        ]);
        $famer=famer::create([

            // 'famer_code' => $request->famer_code,
            'name_prefix' => $request->name_prefix,
            'fname' => $request->fname,
            'lname' => $request->lname,
            'gender' => $request->gender,
            'birthday' => $request->birthday,
            'adderss' => $request->adderss,
            'status' => $request->status,
            'number_chil' => $request->number_chil,
            'education' => $request->education,
            'phone_number' => $request->phone_number,

        ]);

        $garden_detail=garden_detail::create([

            'famer_id' => $famer->id,
            'distance' => $request->distance,
            'latitude' => $request->latitude,
            'longtitude' => $request->longtitude,
            'total_garden' => $request->total_garden,
            'soli_look' => $request->soli_look,
            'HP' => $request->HP,
            'enviroment' => $request->enviroment,
        ]);
        // alert()->success('', 'บันทึกสำเร็จ');
        // dd($famer);
        return redirect()->route('famer-list')->with('success','บันทึกสำเร็จ');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $famer = famer::with('garden_detail')->where(['id' => $id])->first();
        // dd($famer);
        return view('famer.famer-edit',compact('famer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $famer= famer::find($id)->update([
            'famer_code' => $request->get('famer_code'),
            'fname' => $request->get('fname'),
            'lname' => $request->get('lname'),
            'gender' => $request->get('gender'),
            'birthday' => $request->get('birthday'),
            'adderss' => $request->get('adderss'),
            'status' => $request->get('status'),
            'number_chil' => $request->get('number_chil'),
            'education' => $request->get('education'),
            'phone_number' => $request->get('phone_number'),
        ]);
            
        $garden_detail = garden_detail::where('famer_id', $id)->update([
            
            //'famer_id' => $famer->id,
           
            
            'distance' => $request->get('distance'),
            'latitude' => $request->get('latitude'),
            'longtitude' => $request->get('longtitude'),
            'total_garden' => $request->get('total_garden'),
            'soli_look' => $request->get('soli_look'),
            'HP' => $request->get('HP'),
            'enviroment' => $request->get('enviroment'),
        ]);


        // alert()->success('', 'บันทึกสำเร็จ');
        return redirect()->route('famer-list')->with('success','อัพเดทสำเร็จ !!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
            famer::where(['id' => $id])->delete();
            garden_detail::where(['famer_id' => $id])->delete();


            return redirect()->route('famer-list')->with('success','ลบสำเร็จ');
    }

}
