<?php

namespace App\Http\Controllers;

use App\score_famer_main;
use Illuminate\Http\Request;

class ScoreFamerMainController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\score_famer_main  $score_famer_main
     * @return \Illuminate\Http\Response
     */
    public function show(score_famer_main $score_famer_main)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\score_famer_main  $score_famer_main
     * @return \Illuminate\Http\Response
     */
    public function edit(score_famer_main $score_famer_main)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\score_famer_main  $score_famer_main
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, score_famer_main $score_famer_main)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\score_famer_main  $score_famer_main
     * @return \Illuminate\Http\Response
     */
    public function destroy(score_famer_main $score_famer_main)
    {
        //
    }
}
