<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Criteria_detail extends Model
{
    protected $table="criteria_detail";
    protected $fillable = [
        'id', 'criteria_detail_name', 'criteria_detail_low', 'criteria_detail_middle', 'criteria_detail_high', 'criteria_detail_Weight','criteria_main_id','id_detail','id_season',
    ];
    public function score_famer_main_detail(){
        return $this->hasOne(score_famer_main_detail::class, 'criteria_detail_id', 'id');
    }
}
