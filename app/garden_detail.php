<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\famer;

class garden_detail extends Model
{
    protected $table = "garden_detail";

    protected $fillable = [
        'famer_id','distance','latitude','longtitude','total_garden','soli_look','HP','enviroment',
    ];
    protected $primaryKey='id';
}
