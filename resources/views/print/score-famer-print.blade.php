<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>สรุปผลการประเมินเกษตรกรทั้งหมด</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/bootstrap-extended.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/colors.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/components.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/themes/dark-layout.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/themes/semi-dark-layout.css') }}">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/core/menu/menu-types/vertical-menu.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/core/colors/palette-gradient.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/pages/invoicex.css') }}">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/style.css') }}">
    <!-- END: Custom CSS-->
</head>
<body onload="javascript:window.print()">
<div class="container mt-5 mb-5">
<section class="card invoice-page">
    <div id="invoice-template" class="card-body">
        
    <div class="row">
  <div class="col-md-12">
    <div class="card">
            <div class="card-body">
              <div class="row">
                  <div class="col">
                  <!-- <a class="btn btn-primary fa fa-print"   href="javascript:window.print()"> พิมพ์</a> -->
                  </div>
                  <div class="col align-self-center">
                  <h2 class="text-center">สรุปผลการประเมินเกษตรกรทั้งหมด</h2>
                  <h4 class="text-center">รอบ : {{ $Criteria->criteria_season_detail }}</h4>
                </div>
                <div class="col align-self-center">
                  
                </div>
              </div>



            <div class="card">
            <div class="card-body">
           
           <h5>คำอธิบาย</h5>
            <h6 class="text">อันดับเรียงตามคะแนนรวมจากมากไปหาน้อย</h6>
           
              <table id="add" class="table text-center">
                <thead class="thead-dark text-center">
                  <tr>
                    <th scope="col">อันดับที่ได้</th>
                    <th scope="col">ชื่อเกษตรกร</th>
                    <th scope="col">ที่อยู่</th>
                    <th scope="col">โทรศัพท์</th>
                    <th scope="col">คะแนนรวม</th>
                    
                  </tr>
                </thead>
                <tbody>
                  @foreach ($scorce as $key=>$item)
                  <tr>
                    <td scope="row">{{$key+1}}</td>
                    <td>{{$item->famer->name_prefix .''.$item->famer->fname .' '.$item->famer->lname}}</td>
                    <td>{{$item->famer->adderss}}</td>
                    <td>{{$item->famer->phone_number}}</td>
                    <td>{{$item->criteria_score}}</td>
                    
                </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            </div>
             
              </div>
        <!-- end content-->
    </div>
    <!--  end card  -->
  </div>
</div>



    <div class="col-12">
    <!-- <button class="btn btn-primary col-12 hidden-print" onclick="printElem('invoice-template')">ปริ้น</button> -->
    <!-- <a class="btn btn-info col-12 hidden-print" href="#">ย้อนกลับ</a> -->
    <!-- <a class="btn btn-primary col-12 hidden-print" href="javascript:window.print()">พิมพ์รายงาน</a> -->
</div>
</section>
</div>
@push('scripts')
<script>

  $(document).ready( function () {
    $('#add').DataTable();

    $("#btnshow").click(function(){
        $("#advanced").toggle();;
    });

  } );

</script>

@endpush
</body>

</html>
