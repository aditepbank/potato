<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>เกษตรกรที่ตกคุณสมบัติด้านการขนส่ง</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/bootstrap-extended.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/colors.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/components.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/themes/dark-layout.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/themes/semi-dark-layout.css') }}">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/core/menu/menu-types/vertical-menu.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/core/colors/palette-gradient.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/pages/invoicex.css') }}">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/style.css') }}">
    <!-- END: Custom CSS-->
</head>
<body onload="javascript:window.print()">

<div class="container mt-5 mb-5">
<div class="row">
    <div class="col-md-4 ml-auto mr-auto">
      <div class="card card-stats">
        <div class="card-body text-center">
          <h4 class="text">เกณฑ์การคัดเลือก</h4>
        </div>
        <div class="col-2 col-md-12">
          <div class="icon-success">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <i class="fas fa-check-circle fa-2x text-success"></i> &nbsp;&nbsp;&nbsp;&nbsp;<span>ด้านผลิตภัณฑ์ ได้ผลประเมิน ปานกลาง และสูง</span>
            <br>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <i class="fas fa-check-circle fa-2x text-success"></i> &nbsp;&nbsp;&nbsp;&nbsp;<span>ด้านกระบวนการผลิต ได้ผลประเมิน ปานกลาง และสูง</span>
            <br>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <i class="fas fa-times-circle fa-2x text-danger"></i> &nbsp;&nbsp;&nbsp;&nbsp;<span>ด้านการขนส่ง ได้ผลประเมินต่ำเรื่องใดเรื่องหนึ่ง</span>
          </div>
        </div>
        <div class="card-footer ">
        </div>
      </div>
    </div>
  </div>
  <div class="card">
  <br>
  <h4 class="text-center">ปัจจัยที่ส่งผลให้เกษตรกรที่ตกคุณสมบัติด้านการขนส่ง</h4>
  <br>
<div class="row">
    <div class="col-lg-3 col-md-6 col-sm-6">
      <div class="card card-stats">
        <div class="card-body ">
          <div class="row">
            <div class="col-5 col-md-4">
              <div class="icon-big text-center icon-warning">
                <i class="fas fa-truck-moving text-success"></i>
              </div>
            </div>
            <div class="col-7 col-md-8">
              <div class="numbers">
                <p class="card-category">ด้านศักยภาพในการขนส่ง</p>
                <p class="card-title">{{ isset($countx[7])?count($countx[7]):0 }} คน
                  </p></div>
            </div>
          </div>
        </div>
        <div class="card-footer ">
        </div>
      </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6">
      <div class="card card-stats">
        <div class="card-body ">
          <div class="row">
            <div class="col-5 col-md-4">
              <div class="icon-big text-center icon-warning">
                <i class="fas fa-clock text-warning"></i>
              </div>
            </div>
            <div class="col-7 col-md-8">
              <div class="numbers">
                <p class="card-category">ด้านส่งมอบได้ตามเวลา</p>
                <p class="card-title">{{ isset($countx[8])?count($countx[8]):0 }} คน
                  </p></div>
            </div>
          </div>
        </div>
        <div class="card-footer ">
        </div>
      </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6">
      <div class="card card-stats">
        <div class="card-body ">
          <div class="row">
            <div class="col-5 col-md-4">
              <div class="icon-big text-center icon-warning">
                <i class="fas fa-boxes text-primary"></i>
              </div>
            </div>
            <div class="col-7 col-md-8">
              <div class="numbers">
                <p class="card-category">ด้านส่งมอบได้ตามปริมาณ</p>
                <p class="card-title">{{ isset($countx[9])?count($countx[9]):0 }} คน
                  </p><p>
              </p></div>
            </div>
          </div>
        </div>
        <div class="card-footer ">
        </div>
      </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6">
      <div class="card card-stats">
        <div class="card-body ">
          <div class="row">
            <div class="col-5 col-md-4">
              <div class="icon-big text-center icon-warning">
                <i class="fas fa-road text-danger"></i>
              </div>
            </div>
            <div class="col-7 col-md-8">
              <div class="numbers">
                <p class="card-category">ด้านระยะห่างจากแปลงปลูกมายังโรงงาน</p>
                <p class="card-title">{{ isset($countx[10])?count($countx[10]):0 }} คน
                  </p><p>
              </p></div>
            </div>
          </div>
        </div>
        <div class="card-footer ">
          
        </div>
      </div>
    </div>
  </div>
  {{-- <div class="row">
    <div class="col-md-6 ml-auto mr-auto">
        <div class="card card-stats">
          <div class="card-body text-center">
            <div class="row">
              <div class="col-5 col-md-4">
                <div class="icon-big text-center icon-warning">
                  <i class="nc-icon nc-single-02 text-warning"></i>
                </div>
              </div>
              <div class="col-7 col-md-8">
                <div class="numbers">
                  <p class="card-category">รวมจำนวนเกษตรกรที่มีปัญหาด้านการขนส่ง</p>
                  <p class="card-title">{{ count($famery)?count($famery):0 }} คน
                    </p><p>
                </p></div>
              </div>
            </div>
          </div>
          <div class="card-footer ">
          </div>
        </div>
      </div>
  </div> --}}
</div>
<section class="card invoice-page">
    <div id="invoice-template" class="card-body">

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <h4 class="text-center">รายชื่อเกษตรกรที่ตกคุณสมบัติด้านการขนส่ง</h4>
        <div class="card">
            <div class="card-body">
                <table id="add" class="table">
                    <thead class="thead-dark" cellspacing="0" width="100%">
                        <tr>
                            <th width=7%>ลำดับ</th>
                            <th width=12%>ชื่อเกษตรกร</th>
                            <th width=25%>ทีอยู่</th>
                            <th width=12%>โทรศัพท์</th>
                            <th width=30%>ปัจจัยที่ส่งผลให้เกษตรกรตกคุณสมบัติด้านการขนส่ง</th> 
                        </tr>
                    </thead>
                    <tbody>
                      <?php $i = 0 ?>
                    @foreach($score_famer_main_detail as $item)
                        @php
                            if($item->criteria_detail_id <= 6) continue;
                        @endphp
                        <?php $i++ ?>
                        <tr>
                            <td>{{ $i }}.</td>
                            <td>{{ $item->name_prefix.''.$item->fname.' '.$item->lname }}</td>
                            <td>{{ $item->adderss }}</td>
                            <td>{{ $item->phone_number }}</td>
                            <td>{{ $item->criteria_detail_name  }}</td>
                        </tr>
                    @endforeach   
                    </tbody>
                </table>
            </div>
            </div>
                </div>
            </div>
        </div>
    </div>
</section>
</div>
@push('scripts')
<script>

  $(document).ready( function () {
    $('#add').DataTable();

    $("#btnshow").click(function(){
        $("#advanced").toggle();;
    });

  } );

</script>

@endpush
</body>

</html>