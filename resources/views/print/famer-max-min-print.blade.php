<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>วิเคราะห์ผลประเมิน</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/bootstrap-extended.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/colors.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/components.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/themes/dark-layout.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/themes/semi-dark-layout.css') }}">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/core/menu/menu-types/vertical-menu.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/core/colors/palette-gradient.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/pages/invoicex.css') }}">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/style.css') }}">
    <!-- END: Custom CSS-->
</head>
<body onload="javascript:window.print()">
<div class="container mt-5 mb-5">
<section class="card invoice-page">
    <div id="invoice-template" class="card-body">







    <div class="row">
  <div class="col-md-12">
    <div class="card">
            <div class="card-body">
            <div class="row">
                
                <div class="col-md-11"> <h3 class="text-center">ผลวิเคราะห์ปัจจัยที่ส่งเสริมค่าคะแนนของเกษตรกร</h3>
                <h4 class="text-center">รอบการประเมิน {{ $Criteria->criteria_season_detail }}</h4>

                <div class="card">
    
    <div class="card-body">

        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <h3 class="text-center text-success">เกษตรกรที่ได้ผลประเมินสูงที่สุด</h3>
                        <hr>
                    </div>
                    
                        <div class="card-body">
                        
                        <img src="{{ asset('happy_0.png')}}" class="rounded mx-auto d-block" alt="Responsive image">
                        
                    
                    <p class="text-center text-success"> <h4 class="text-center ">{{$score->famer->name_prefix.''.$score->famer->fname.' '.$score->famer->lname}}</h4></p><br>
                    
                    <h5 class="text-left ">ปัจจัยบวก ที่ส่งเสริมให้ผลประเมินดี</h5><br>

                    @foreach($score_detail as $item)
                            <h5 class="text-success"><b><i class="nc-icon nc-check-2 text-success"></i> ด้าน{{$item->get_Criteria_detail->criteria_detail_name}}</b></h5>
                            <!-- <h5 class="text-success"><b><i class="nc-icon nc-check-2 text-success"></i> ปัจจัยรองด้านเปอร์เซ็นต์แป้งในมันสำปะหลัง</b></h5> -->
                            <!-- <p><i class="nc-icon nc-check-2 text-success"></i> ด้าน......... </p>
                            <p><i class="nc-icon nc-check-2 text-success"></i> ด้าน......... </p> -->
                        @endforeach
                             

                            
                            
                            <!-- <p><i class="nc-icon nc-check-2 text-success"></i> ด้าน......... </p>
                            <p><i class="nc-icon nc-check-2 text-success"></i> ด้าน......... </p> -->
                      
                        <br><hr><br>
                        <h5 class="text-center text-success">ค่าคะแนนรวม : <b>{{$score->criteria_score}}</b> </h5>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <h3 class="text-center text-danger">เกษตรกรที่ได้ผลประเมินต่ำที่สุด</h3>
                        <hr>
                    </div>
                    <div class="card-body">
                    <img src="{{ asset('sad_0.png')}}" class="rounded mx-auto d-block" alt="Responsive image">
                    
                    <p class="text-center text-danger"><h4 class="text-center">{{$score->famer->name_prefix.''.$score_min->famer->fname.' '.$score_min->famer->lname}}</h4></p><br>
                    <h5 class="text-left ">ปัจจัยลบ ที่ทำให้ผลประเมินต่ำ</h5><br>
                    
                    @foreach($score_detail_min as $item)
                            <h5 class="text-success"><b><i class="nc-icon nc-check-2 text-success"></i> ด้าน{{$item->get_Criteria_detail->criteria_detail_name}}</b></h5>
                            <!-- <h5 class="text-success"><b><i class="nc-icon nc-check-2 text-success"></i> ปัจจัยรองด้านเปอร์เซ็นต์แป้งในมันสำปะหลัง</b></h5> -->
                            <!-- <p><i class="nc-icon nc-check-2 text-success"></i> ด้าน......... </p>
                            <p><i class="nc-icon nc-check-2 text-success"></i> ด้าน......... </p> -->
                        @endforeach
                    
                        <br><hr><br>
                        <h5 class="text-center text-danger">ค่าคะแนนรวม : <b>{{$score_min->criteria_score}}</b></h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
                </div>
            </div>
    
        <div class="col-12">
    <!-- <button class="btn btn-primary col-12 hidden-print" onclick="printElem('invoice-template')">ปริ้น</button> -->
    <!-- <a class="btn btn-info col-12 hidden-print" href="#">ย้อนกลับ</a> -->
    <!-- <a class="btn btn-primary col-12 hidden-print" href="javascript:window.print()">พิมพ์รายงาน</a> -->
        </div>
    </div>

</section>
</div>
@push('scripts')
<script>

  $(document).ready( function () {
    $('#add').DataTable();

    $("#btnshow").click(function(){
        $("#advanced").toggle();;
    });

  } );

</script>

@endpush
</body>

</html>
       