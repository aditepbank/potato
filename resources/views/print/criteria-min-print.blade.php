<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>รายละเอียดเกษตรกร ด้าน{{$Criteria_main->criteria_main_name}}</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/bootstrap-extended.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/colors.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/components.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/themes/dark-layout.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/themes/semi-dark-layout.css') }}">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/core/menu/menu-types/vertical-menu.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/core/colors/palette-gradient.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/pages/invoicex.css') }}">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/style.css') }}">
    <!-- END: Custom CSS-->
</head>
<body onload="javascript:window.print()">
<div class="container mt-5 mb-5">
<section class="card invoice-page">
    <div id="invoice-template" class="card-body">
        
    <div class="row">
  <div class="col-md-12">
    <div class="card">
            <div class="card-body">
            <div class="row">
                <!-- <div class="col-md-1"> <a class="btn btn-primary fa fa-print"  target="_bank" href="{{route('criteria-max-print',[$form_id,request()->topic_id])}}"> พิมพ์</a></div> -->
                <!-- <div class="col-md-1"> <a class="btn btn-danger"href="{{route('report-criteria-best', $form_id)}}"> < ย้อนกลับ</a></div> -->
                <div class="col-md-11"> <h3 class="text-center">10 อันดับเกษตรกรที่มีคุณสมบัติด้อย ด้าน{{$Criteria_main->criteria_main_name}}</h3>
                
                </div>
            </div>



            <div class="card">
            <div class="card-body">
           
           <!-- <h5>คำอธิบาย</h5>
            <h6 class="text">อันดับเรียงตามคะแนนรวมจากมากไปหาน้อย</h6> -->
           
            <table class="table text-center">
                        <thead class="thead-dark text-center">
                        <th scope="col">ลำดับที่</th>
                   
                    <th scope="col">ชื่อเกษตรกร</th>
                    <th scope="col">ที่อยู่</th>
                    <th scope="col">โทรศัพท์</th>
                    <th scope="col">ปัจจัยที่ส่งผลให้ประเมินได้ต่ำ</th>
                        </thead>
                        <tbody>
                        @foreach ($results as $key=>$item)
                            <tr>
                                <td scope="row">{{$key+1}}</td>
                                <th>{{$item->name_prefix.' '.$item->fname.' '.$item->lname}}</th>
                                <td>{{$item->adderss}}</td>
                                <td>{{$item->phone_number}}</td>
                                <th>{{$item->criteria_detail_name}}</th>
                            </tr>
                          @endforeach
                        </tbody>
                    </table>
            </div>
            </div>
             
              </div>
        <!-- end content-->
    </div>
    <!--  end card  -->
  </div>
</div>



    <div class="col-12">
    <!-- <button class="btn btn-primary col-12 hidden-print" onclick="printElem('invoice-template')">ปริ้น</button> -->
    <!-- <a class="btn btn-info col-12 hidden-print" href="#">ย้อนกลับ</a> -->
    <!-- <a class="btn btn-primary col-12 hidden-print" href="javascript:window.print()">พิมพ์รายงาน</a> -->
</div>
</section>
</div>
</body>

</html>
