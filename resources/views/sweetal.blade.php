<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script>
@if(\Session::has('success'))
Swal.fire(
  'ทำงานสำเร็จ!',
  '{{ \Session::get('success')}}',
  'success'
)
@endif
@if(\Session::has('error'))
Swal.fire(
  'ผิดพลาด!',
  '{{ \Session::get('error')}}',
  'error'
)
@endif
@if(count($errors) > 0)
Swal.fire({
  title:'กรุณากรอกข้อมูลให้ครับ!',
  html:'<ul>@foreach($errors->all() as $errors)<li>{{$errors}}</li>@endforeach</ul>',
  type: 'error',
}
)
                     
  @endif
</script>