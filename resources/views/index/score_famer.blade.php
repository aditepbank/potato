@extends('layouts.dashboard')
@section('title','หน้าหลัก')
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card">
        <div class="card-header">
          <h3 class="text-center">แนะนำเกษตรที่น่าสนใจ</h3>

            <div class="card-body">
            <div class="dropdown">
            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                เลือกฤดูกาล
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    @foreach($Criteria as $value)
                       <button class="dropdown-item" value= "{{ $value->id }}">{{ $value->criteria_season_name  }}</button>
                    @endforeach
            </div>
            </div>
            <div class="content">

        <div class="row">
          <div class="col">
          <div class="card">
          <div class="card-header">
                <h5 class="card-title">แนะนำเกษตรที่ควรทำสัญญาต่อ</h5>
              </div>
              <div class="card-body">
                    <table class="table">
                        <thead >
                            <th>
                                ลำดับ
                            </th>
                            <th>
                                ชื่อ
                            </th>
                            <th>
                                คะแนนรวม
                            </th>
                            <th>
                                ดูเพิ่มเติม
                            </th>
                        </thead>
                        <tbody>
                        @foreach ($scorce as $key=>$item)
                            <tr>
                                <th scope="row">{{$key+1}}</th>
                                <td>{{$item->famer->fname .' '.$item->famer->lname}}</td>
                                <td>{{$item->criteria_score}}</td>
                                <td>
                                <a href="{{ route('detailfarmerscorereport',$item->id) }}" type="button" class="btn btn-info"><i class="fas fa-info-circle"></i></a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <a href="#">ดูผลประเมินทั้งหมด</a>
              </div>
          </div>
          </div>
          <div class="col">
          <div class="card">
          <div class="card-header">
                <h5 class="card-title">แนะนำเกษตรผลิตภัณฑ์ดี</h5>
              </div>
              <div class="card-body">
                    <table class="table">
                        <thead >
                            <th>
                                ลำดับ
                            </th>
                            <th>
                                ชื่อ
                            </th>
                            <th>
                                คะแนนนวม
                            </th>
                            <th>
                                ดูเพิ่มเติม
                            </th>
                        </thead>
                        <tbody>
                        @foreach ($scorce as $key=>$item)
                        <tr>
                            <th scope="row">{{$key+1}}</th>
                            <td>{{$item->famer->fname .' '.$item->famer->lname}}</td>
                            @foreach($item->score_famer_main as $itemx)
                            <td>{{$itemx->topic_score}}</td>
                            @endforeach
                            <td>
                            <a href="{{ route('detailfarmerscorereport',$item->id) }}" type="button" class="btn btn-info"><i class="fas fa-info-circle"></i></a>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <a href="#">ดูผลประเมินทั้งหมด</a>
              </div>
          </div>
          </div>
        </div>
        <div class="row">
          <div class="col">
          <div class="card">
          <div class="card-header">
                <h5 class="card-title">แนะนำเกษตรที่กระบวนการผลิตดี</h5>
              </div>
              <div class="card-body">
                    <table class="table">
                        <thead >
                            <th>
                                ลำดับ
                            </th>
                            <th>
                                ชื่อ
                            </th>
                            <th>
                                คะแนนรวม
                            </th>
                            <th>
                                ดูเพิ่มเติม
                            </th>
                        </thead>
                        <tbody>
                        @foreach ($scorce as $key=>$item)
                            <tr>
                                <th scope="row">{{$key+1}}</th>
                                <td>{{$item->famer->fname .' '.$item->famer->lname}}</td>
                                <td>{{$item->criteria_score}}</td>
                                <td>
                                <a href="{{ route('detailfarmerscorereport',$item->id) }}" type="button" class="btn btn-info"><i class="fas fa-info-circle"></i></a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <a href="#">ดูผลประเมินทั้งหมด</a>
              </div>
          </div>
          </div>
          <div class="col">
          <div class="card">
          <div class="card-header">
                <h5 class="card-title">แนะนำเกษตรที่ให้ความร่วมมือ</h5>
              </div>
              <div class="card-body">
                    <table class="table">
                        <thead >
                            <th>
                                ลำดับ
                            </th>
                            <th>
                                ชื่อ
                            </th>
                            <th>
                                คะแนนนวม
                            </th>
                            <th>
                                ดูเพิ่มเติม
                            </th>
                        </thead>
                        <tbody>
                        @foreach ($scorce as $key=>$item)
                        <tr>
                            <th scope="row">{{$key+1}}</th>
                            <td>{{$item->famer->fname .' '.$item->famer->lname}}</td>
                            @foreach($item->score_famer_main as $itemx)
                            <td>{{$itemx->topic_score}}</td>
                            @endforeach
                            <td>
                            <a href="{{ route('detailfarmerscorereport',$item->id) }}" type="button" class="btn btn-info"><i class="fas fa-info-circle"></i></a>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <a href="#">ดูผลประเมินทั้งหมด</a>
              </div>
          </div>
          </div>
        </div>






            </div>
        </div>
        <!-- end content-->
    </div>
    <!--  end card  -->
  </div>
</div>
<!-- <script>
    $(document).ready(function() {
      // Javascript method's body can be found in assets/assets-for-demo/js/demo.js
      demo.initChartPageCharts();
    });
  </script> -->
@endsection
