@extends('layouts.master')
@section('title','หน้าหลัก')
@section('content')
<nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
  <div class="container-fluid">
    <div class="navbar-wrapper">
      <div class="navbar-minimize">
        <button id="minimizeSidebar" class="btn btn-icon btn-round">
          <i class="nc-icon nc-minimal-right text-center visible-on-sidebar-mini"></i>
          <i class="nc-icon nc-minimal-left text-center visible-on-sidebar-regular"></i>
        </button>
      </div>
      <div class="navbar-toggle">
        <button type="button" class="navbar-toggler">
          <span class="navbar-toggler-bar bar1"></span>
          <span class="navbar-toggler-bar bar2"></span>
          <span class="navbar-toggler-bar bar3"></span>
        </button>
      </div>
      <a class="navbar-brand" href="#pablo">จัดการรายชื่อเกษตรกร</a>
    </div>
  </div>
</nav>

<div class="row">
  <div class="col-md-12">
    <div class="card">
        <div class="card-header">
        
        <a  href="{{route('famer-list')}}" type="button" class="btn btn-primary " >ย้อนกลับ</a>
        
          <h4 class="card-title">ข้อมูลเพิ่มเติมของ : {{$famer->name_prefix.''.$famer->fname .' '. $famer->lname}}</h4>
        </div>
            <div class="card-body">
                <hr>
                
                    <h4>ข้อมูลส่วนตัว</h4>
                <div class="form-row"> 
                    <div class="form-group col-md-4">  
                        <p>ชื่อ-นามสกุล : {{ $famer->name_prefix.''.$famer->fname .' '. $famer->lname}}</p>
                    </div>
                    <!-- <div class="form-group col-md-4">  
                        <p>รหัสเกษตรกร : {{ $famer->famer_code}}</p>
                    </div> -->
                    <div class="form-group col-md-4"> 
                        <p>วัน/เดือน/ปี เกิด : {{$famer->birthday}}</p>
                    </div>
                </div>   
                <div class="form-row">   
                    <div class="form-group col-md-4"> 
                        <p>เพศ : {{ $famer->gender}}</p>
                    </div>
                    <div class="form-group col-md-4"> 
                        <p>สถานะภาพ : {{ $famer->status}}</p>
                    </div>
                    <div class="form-group col-md-4"> 
                        <p>จำนวนสมาชิกในครอบครัว : {{ $famer->number_chil}} คน</p>
                    </div>
                </div>
                <div class="form-row">  
                    <div class="form-group col-md-4"> 
                        <p>ระดับการศึกษา : {{ $famer->education}} </p>
                    </div>
                    <div class="form-group col-md-4"> 
                        <p>หมายเลขโทรศัพท์ : {{ $famer->phone_number}}</p>
                    </div>
                </div>
                    
                    <p>ที่อยู่ : {{ $famer->adderss}} </p>
                    
                </div>        
                    
                <hr>
                <h4>ข้อมูลไร่มัน</h4>
                <div class="form-row">
                    <div class="form-group col-md-4"> 
                        <p>ระยะห่างโรงงาน : {{ $famer->garden_detail->distance}} กิโลเมตร</p>
                    </div>
                    <div class="form-group col-md-4">
                        <p>ละติจูด : {{ $famer->garden_detail->latitude}} </p>
                    </div>
                    <div class="form-group col-md-4">
                     <p>ลองติจูด :{{ $famer->garden_detail->longtitude}} </p>
                    </div>
                </div>
                <div class="form-row"> 
                    <div class="form-group col-md-4"> 
                         <p>จำนวณไร่ : {{ $famer->garden_detail->total_garden}} ไร่</p>
                    </div>
                    <div class="form-group col-md-4"> 
                        <p>ลักษณะดิน : {{ $famer->garden_detail->soli_look}}</p>
                    </div> 
                    <div class="form-group col-md-4"> 
                     <p>ค่า HP : {{ $famer->garden_detail->HP}}</p>
                    </div> 
                    <div class="form-group col-md-4"> 
                        <p>สิ่งแวดล้อม : {{ $famer->garden_detail->enviroment}} </p>
                    </div> 
                </div>    
                <hr>   
                
                




            </div>
        <!-- end content-->
    </div>
    <!--  end card  -->
   </div>
</div>


@endsection