@extends('layouts.master')
@section('title','หน้าหลัก')
@section('content')
<nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
  <div class="container-fluid">
    <div class="navbar-wrapper">
      <div class="navbar-minimize">
        <button id="minimizeSidebar" class="btn btn-icon btn-round">
          <i class="nc-icon nc-minimal-right text-center visible-on-sidebar-mini"></i>
          <i class="nc-icon nc-minimal-left text-center visible-on-sidebar-regular"></i>
        </button>
      </div>
      <div class="navbar-toggle">
        <button type="button" class="navbar-toggler">
          <span class="navbar-toggler-bar bar1"></span>
          <span class="navbar-toggler-bar bar2"></span>
          <span class="navbar-toggler-bar bar3"></span>
        </button>
      </div>
      <a class="navbar-brand" href="#pablo">จัดการรายชื่อเกษตรกร</a>
    </div>
  </div>
</nav>

<div class="row">
  <div class="col-md-12">
    <div class="card">
        <div class="card-header">
          <h4 class="card-title text-center">รายชื่อเกษตรกร</h4>
        </div>
            <div class="card-body">

            @if(count($errors) > 0)
            <div class="alert alert-danger">
              <ul>@foreach($errors->all() as $errors)
              <li>{{$errors}}</li>
              @endforeach
              </ul>
          </div>
          @endif
                <a href="{{route('famer-add')}}" type="button" class="btn btn-primary" >เพิ่มรายชื่่อ</a>
                <table id="add" class="table text-center">
                    <thead class="thead-dark text-center">
                        <tr>
                            <th scope="col">ลำดับ</th>
                            <th scope="col">ชื่อเกษตรกร</th>
                            <th scope="col">โทรศัพท์</th>
                            <th scope="col">ที่อยู่</th>
                            <th class="disabled-sorting text-right">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($famer as $key => $item)
                        <tr>
                            <td>{{ $key+1 }}</td>
                            <td>{{ $item->name_prefix .''.$item->fname .' '. $item->lname}}</td>
                            <td>{{$item->phone_number }}</td>
                            <td>{{$item->adderss }}</td>
                            <td class="text-right">
                                <a href="{{route('famer-list-information',$item->id)}}" class="btn btn-info">ดูข้อมูลเพิ่มเติม</a>
                                <a href="{{route('famer-edit-get',$item->id)}}" class="btn btn-warning">แก้ไข</a>
                                <a onclick="return confirm('ต้องการลบ ???')" href="{{route('famer-delete',$item->id)}}"  class="btn btn-danger">ลบ</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        <!-- end content-->
    </div>
    <!--  end card  -->
   </div>
</div>
@section('javascript')
@include('sweetal')

@endsection


@endsection

@push('scripts')
<script>
  $(document).ready( function () {
      $('#add').DataTable();
  } );
</script>
@endpush
