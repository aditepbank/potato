@extends('layouts.master')
@section('title','หน้าหลัก')
@section('content')

<nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
  <div class="container-fluid">
    <div class="navbar-wrapper">
      <div class="navbar-minimize">
        <button id="minimizeSidebar" class="btn btn-icon btn-round">
          <i class="nc-icon nc-minimal-right text-center visible-on-sidebar-mini"></i>
          <i class="nc-icon nc-minimal-left text-center visible-on-sidebar-regular"></i>
        </button>
      </div>
      <div class="navbar-toggle">
        <button type="button" class="navbar-toggler">
          <span class="navbar-toggler-bar bar1"></span>
          <span class="navbar-toggler-bar bar2"></span>
          <span class="navbar-toggler-bar bar3"></span>
        </button>
      </div>
      <a class="navbar-brand" href="#pablo">จัดการรายชื่อเกษตรกร</a>
    </div>
  </div>
</nav>
<div class="row">
  <div class="col-md-12">
    <div class="card">
        <div class="card-header">
          <h4 class="card-title">เพิ่มรายชื่อเกษตรกร</h4>
        </div>
            <div class="card-body">
                <div class="card-body">
                @if(count($errors) > 0)
                     <div class="alert alert-danger">
                        <ul>@foreach($errors->all() as $errors)
                        <li>{{$errors}}</li>
                        @endforeach
                        </ul>
                    </div>
                    @endif
                    @if(\Session::has('success'))
                    <div class="alert alert-success">
                        <p>{{ \Session::get('success')}}</p>
                    </div>
                    @endif
                    <form method="post" action="{{ route('famer-update-post',$famer->id) }}">
                         {{csrf_field()}}
                                <div class="row">
                                    <div class="col">
                                     <label for="inputName">รหัสเกษตรกร</label>
                                    <input type="number" class="form-control col-3" name="famer_code" value="{{ $famer->famer_code}}" placeholder="รหัสเกษตรกร" >
                                    </div>
                                </div>
                        <div class="form-group">
                            <label for="inputName">ชื่อ - นามสกุล : </label>
                            <div class="row">
                                    <div class="col">
                                    <input type="text" class="form-control"  name="fname" placeholder="ชื่อ" value="{{ $famer->fname}}" >
                                    </div>
                                    
                                    <div class="col">
                                    <input type="text" class="form-control"  name="lname" placeholder="นามสกุล" value="{{ $famer->lname}}">
                                    </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputDate">วันดือนปีเกิด : </label>
                            <input type="date" class="form-control" name="birthday" value="{{ $famer->birthday}}">
                        </div>
                        <div class="form-group">
                            <label for="inputDate">เพศ</label><br>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="gender" id="inlineRadio1" chacked value="mele"
                                {{ $famer->gender == 'mele' ? 'checked' : '' }} >

                                <label class="form-check-label" for="inlineRadio1">ชาย</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="gender" id="inlineRadio2" chacked value="femele"
                                {{ $famer->gender == 'femele' ? 'checked' : '' }} >
                                <label class="form-check-label" for="inlineRadio2">หญิง</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="inputStatus">สถานะภาพ : </label>
                                    <input type="text" class="form-control col-6" name="status" value="{{ $famer->status}}" >
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label for="inputfamily">จำนวนสมาชิกในครอบครัว : </label>
                                    <input type="number" class="form-control col-6" name="number_chil" min="0" value="{{ $famer->number_chil}}">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputaddress">ที่อยู่</label>
                            <input type="text" class="form-control" name="adderss" value="{{ $famer->adderss}}">
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="inputPhone">หมายเลขโทรศัพท์ : </label>
                                    <input type="text" class="form-control col-6" name="phone_number" maxlength="10" min="0" placeholder="กรุณากรอกตัวเลข" value="{{ $famer->phone_number}}">
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label for="inputEdu">ระดับการศึกษา : </label>
                                    <input type="text" class="form-control col-6" name="education" value="{{ $famer->education}}">
                                </div>
                            </div>
                        </div>
                        <hr>
                        <h6>ข้อมูลไร่มัน</h6>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="inputrai">ระยะห่างโรงงาน : </label>
                                    <input type="number" class="form-control" name="distance" min="0" value="{{ $famer->garden_detail->distance}}">
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label for="inputrai">ละติจูด : </label>
                                    <input type="text" class="form-control" name="latitude" value="{{ $famer->garden_detail->latitude}}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="inputrai">ลองติจูด : </label>
                                    <input type="text" class="form-control" name="longtitude" value="{{ $famer->garden_detail->longtitude}}">
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label for="inputrai">จำนวณไร่ : </label>
                                    <input type="number" class="form-control" name="total_garden" min="0" value="{{ $famer->garden_detail->total_garden}}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="inputrai">ลักษณะดิน : </label>
                                    <input type="text" class="form-control" name="soli_look" value="{{ $famer->garden_detail->soli_look}}">
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label for="inputrai">ค่า HP : </label>
                                    <input type="number" step="0.01" class="form-control " name="HP" value="{{ $famer->garden_detail->HP}}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col md-6">
                                <div class="form-group">
                                    <label for="inputrai">สิ่งแวดล้อม : </label>
                                    <input type="text" class="form-control " name="enviroment" value="{{ $famer->garden_detail->enviroment}}">
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        <!-- end content-->
    </div>
    <!--  end card  -->
   </div>
</div>
@section('javascript')
@include('sweetal')

@endsection


@endsection

@pushscripts
<script>
$( "#ELEMENTID" ).blur(function() {
    this.value = parseFloat(this.value).toFixed(2);
});
</script>
@endscritps
