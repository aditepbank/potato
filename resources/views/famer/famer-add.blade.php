@extends('layouts.master')
@section('title','หน้าหลัก')
@section('content')
<nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
  <div class="container-fluid">
    <div class="navbar-wrapper">
      <div class="navbar-minimize">
        <button id="minimizeSidebar" class="btn btn-icon btn-round">
          <i class="nc-icon nc-minimal-right text-center visible-on-sidebar-mini"></i>
          <i class="nc-icon nc-minimal-left text-center visible-on-sidebar-regular"></i>
        </button>
      </div>
      <div class="navbar-toggle">
        <button type="button" class="navbar-toggler">
          <span class="navbar-toggler-bar bar1"></span>
          <span class="navbar-toggler-bar bar2"></span>
          <span class="navbar-toggler-bar bar3"></span>
        </button>
      </div>
      <a class="navbar-brand" href="#pablo">จัดการรายชื่อเกษตรกร</a>
    </div>
  </div>
</nav>


<div class="row">
  <div class="col-md-12">
    <div class="card">
        <div class="card-header">
          <h4 class="card-title">เพิ่มข้อมูลเกษตรกร</h4>
        </div>
            <div class="card-body">
                <div class="card-body">
                @if(count($errors) > 0)
                     <div class="alert alert-danger">
                        <ul>@foreach($errors->all() as $errors)
                        <li>{{$errors}}</li>
                        @endforeach
                        </ul>
                    </div>
                    @endif
                    @if(\Session::has('success'))
                    <div class="alert alert-success">
                        <p>{{ \Session::get('success')}}</p>
                    </div>
                    @endif
                    <form method="post" action="{{ route('famer-add-post') }}">
                         {{csrf_field()}}
                                <!-- <div class="row">
                                    <div class="col">
                                        <label for="inputName">รหัสเกษตรกร</label>
                                        <input type="number" class="form-control col-3" name="famer_code" placeholder="รหัสเกษตรกร" required>
                                    </div>

                                </div> -->
                           <br>
                                <div class="form-group">
                                   
                                    <div class="row">
                                               
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="name_prefix" id="inlineRadio1" value="นาย">
                                                <label class="form-check-label" for="inlineRadio1">นาย</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="name_prefix" id="inlineRadio2" value="นาง">
                                                <label class="form-check-label" for="inlineRadio2">นาง</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="name_prefix" id="inlineRadio3" value="นางสาว">
                                                <label class="form-check-label" for="inlineRadio2">นางสาว</label>
                                            </div>
                                            <div class="col-6">
                                                <input type="text" class="form-control col-9"  name="fname" placeholder="ชื่อ" required>
                                            </div>
                                            <div class="col">
                                                <input type="text" class="form-control col-9"  name="lname" placeholder="นามสกุล" required>
                                            </div>
                                    </div>
                                    
                                </div>

                                <div class="form-group">
                                    <label for="inputDate">วันเดือนปีเกิด : </label>
                                    <input type="date" class="form-control col-3" name="birthday" required>
                                </div>

                                <div class="form-group" >
                                    <label for="inputDate">เพศ</label><br>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="gender" id="inlineRadio1" value="male">
                                        <label class="form-check-label" for="inlineRadio1">ชาย</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="gender" id="inlineRadio2" value="female">
                                        <label class="form-check-label" for="inlineRadio2">หญิง</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label for="inputStatus">สถานะภาพ : </label>
                                        <input type="text" class="form-control col-9" name="status" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputfamily">จำนวนสมาชิกในครอบครัว : </label>
                                        <input type="number" class="form-control col-9" min="0" name="number_chil" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputaddress">ที่อยู่</label>
                                    <input type="text" class="form-control" name="adderss" required>
                                    </div>
                                <div class="form-group">
                                    <label for="inputEdu">ระดับการศึกษา : </label>
                                    <input type="text" class="form-control col-3" name="education" required>
                                </div>
                                <div class="form-group">
                                    <label for="inputEdu">หมายเลขโทรศัพท์ : </label>
                                    <input type="tel" class="form-control col-3"  maxlength="10" name="phone_number" placeholder="กรุณากรอกตัวเลข" required>
                                </div>

                                    <hr>
                                    <h6>ข้อมูลไร่มัน</h6>



                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="inputrai">ละติจูด : </label>
                                                <input type="text" class="form-control col-10" name="latitude" required>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="inputrai">ลองติจูด : </label>
                                                <input type="text" class="form-control col-10" name="longtitude" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="inputrai">ระยะห่างโรงงาน : </label>
                                                <input type="number" class="form-control col-6" min="0" name="distance" required>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="inputrai">จำนวณไร่ : </label>
                                                <input type="number" class="form-control col-6" min="0" name="total_garden" required>
                                            </div>
                                        </div>
                                    </div>
                                        <div class="form-group">
                                            <label for="inputrai">ลักษณะดิน : </label>
                                            <input type="text" class="form-control col-6" name="soli_look" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputrai">ค่า HP : </label>
                                            <input type="number" step="0.01" class="form-control col-6" name="HP" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputrai">สิ่งแวดล้อม : </label>
                                            <input type="text" class="form-control col-6" name="enviroment" required>
                                        </div>

                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </form>






                </div>
            </div>
        <!-- end content-->
    </div>
    <!--  end card  -->
   </div>
</div>

@endsection
@section('javascript')
@include('sweetal')

@endsection
@pushscripts
<script>
$( "#ELEMENTID" ).blur(function() {
    this.value = parseFloat(this.value).toFixed(2);
});
</script>
@endscritps

