@extends('layouts.master')
@section('title','หน้าหลัก')
@section('content')
<nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
  <div class="container-fluid">
    <div class="navbar-wrapper">
      <div class="navbar-minimize">
        <button id="minimizeSidebar" class="btn btn-icon btn-round">
          <i class="nc-icon nc-minimal-right text-center visible-on-sidebar-mini"></i>
          <i class="nc-icon nc-minimal-left text-center visible-on-sidebar-regular"></i>
        </button>
      </div>
      <div class="navbar-toggle">
        <button type="button" class="navbar-toggler">
          <span class="navbar-toggler-bar bar1"></span>
          <span class="navbar-toggler-bar bar2"></span>
          <span class="navbar-toggler-bar bar3"></span>
        </button>
      </div>
      <a class="navbar-brand" href="#pablo">จัดการรายชื่อเกษตรกร</a>
    </div>
  </div>
</nav>


<div class="row">
  <div class="col-md-12">
    <div class="card">
        <div class="card-header">
          <h4 class="card-title">เพิ่มข้อมูลการเพราะปลูกและผลิต : {{ $famer->fname.' '.$famer->lname}}</h4>
        </div>
            <div class="card-body">
                <div class="card-body">
                    <form method="post" action="{{route('famer-add-plan-info-post',$famer->id)}}">
                         {{csrf_field()}}
                                <div class="form-row">
                                    <div class="form-group col-md-5">
                                        <label for="inputName">รหัสเกษตรกร : </label>
                                        <input type="text" class="form-control col-3" name="" placeholder="รหัสเกษตรกร" value="{{$famer->famer_code}}">
                                    </div>

                                    <div class="form-group col-md-5">
                                        <label for="inputName">ชื่อ - นามสกุล : </label>
                                        <div class="form-group">
                                            <input type="text" class="form-control col-3" name="" value="{{ $famer->fname.' '.$famer->lname}}" >
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-5">
                                            <label for="inputStatus">ข้อมูลพันธุ์ปลูกมันสำปะหลัง : </label>
                                            <input type="text" class="form-control col-6" name="breed_type" required>
                                    </div>

                                    <div class="form-group col-md-5">
                                        <label for="inputDate">วันที่เริ่มปลูก : </label>
                                        <input type="date" class="form-control col-5" name="date_plant" required>
                                    </div>


                                        <div class="form-group col-md-5">
                                            <label for="inputStatus">ระยะการปลูก/เมตร : </label>
                                            <input type="number" class="form-control col-11" name="Planting_distance" required>
                                        </div>

                                        <div class="form-group col-md-5">
                                            <label for="inputfamily">ลักษณะการปลูก : </label>
                                            <input type="text" class="form-control col-11" name="Planting_characteristics" required>
                                    </div>
                                </div>
                                <hr>
                                <br>
                                <h5>ข้อมูลจัดการแปลงปลูกมันสำปะหลัง</h5>
                                <div class="form-row">
                                    <div class="form-group col-md-5">
                                        <label for="inputaddress">ไถดะ/ครั้ง</label>
                                        <input type="number" class="form-control col-5" name="plow_da" required>
                                    </div>
                                    <div class="form-group col-md-5">
                                        <label for="inputEdu">ไถแปล/ครั้ง : </label>
                                        <input type="number" class="form-control col-5" name="plow_pea" required>
                                    </div>
                                    <div class="form-group col-md-5">
                                        <label for="inputEdu">ปุ๋ยรองพื้น </label>
                                        <input type="text" class="form-control col-5" name="Foundation_fertilizer" required>
                                    </div>
                                    <div class="form-group col-md-5">
                                        <label for="inputEdu">ซักร่องง: </label>
                                        <input type="text" class="form-control col-5" name="Groove" required>
                                    </div>
                                    <div class="form-group col-md-5">
                                        <label for="inputaddress">ประมาณ/ไร่ </label>
                                        <input type="number" class="form-control col-5" name="about_farm" required>
                                    </div>
                                    <div class="form-group col-md-5">
                                        <label for="inputEdu">การให้น้ำ </label>
                                        <input type="text" class="form-control col-5" name="give_water" required>
                                    </div>
                                    <div class="form-group col-md-5">
                                        <label for="inputEdu">การให้น้ำ/ครั้ง : </label>
                                        <input type="number" class="form-control col-5" name="give_water_calculate" required>
                                    </div>
                                    <div class="form-group col-md-5">
                                        <label for="inputEdu">ปุ๋ยแต่งหน้า : </label>
                                        <input type="text" class="form-control col-5" name="Make_up_fertilizer" required>
                                    </div>
                                    <div class="form-group col-md-5">
                                        <label for="inputaddress">ใส่ปุ๋ยแต่งหน้า/ประมาณไร่ : </label>
                                        <input type="number" class="form-control col-5" name="Make_up_fertilizer_about" required>
                                    </div>
                                    <div class="form-group col-md-5">
                                        <label for="inputEdu">กำจัดวัชพืช : </label>
                                        <input type="text" class="form-control col-5" name="Weeding" required>
                                    </div>
                                    <div class="form-group col-md-5">
                                        <label for="inputEdu">ประมาณคนที่ใส่ปุ๋ย/ไร่ : </label>
                                        <input type="number" class="form-control col-5" name="Weeding_about_people" required>
                                    </div>
                                    <div class="form-group col-md-5">
                                        <label for="inputEdu">จำนวนครั้งที่ใส่ปุ๋ย : </label>
                                        <input type="number" class="form-control col-5" name="Weeding_about_people_cal" required>
                                    </div>
                                    <div class="form-group col-md-5">
                                        <label for="inputaddress">คาดการณ์เก็บเกี่ยวผลผลิต/เดือน : </label>
                                        <input type="text" class="form-control col-5" name="Month_harvest" required>
                                    </div>
                                </div>
                        <hr>
                        <h6>ข้อมูลการเก็บเกี่ยว</h6>
                        <div class="form-row">
                                    <div class="form-group col-md-5">
                                        <label for="inputaddress">อายุมันสำปะหลัง/เดือน :</label>
                                        <input type="number" class="form-control col-5" name="age_potato" required>
                                    </div>
                                    <div class="form-group col-md-5">
                                        <label for="inputEdu">ความสูง/ซม. : </label>
                                        <input type="number" class="form-control col-5" name="high_meter" required>
                                    </div>
                                    <div class="form-group col-md-5">
                                        <label for="inputEdu">จำนวนมันสำปะหลัง หัว/ต้น : </label>
                                        <input type="number" class="form-control col-5" name="calculate_potato" required>
                                    </div>
                                    <div class="form-group col-md-5">
                                        <label for="inputEdu">น้ำหนัก กก./ต้น : </label>
                                        <input type="number" class="form-control col-5" name="weight_potato" required>
                                    </div>
                                    <div class="form-group col-md-5">
                                        <label for="inputaddress">ประเมินผลผลิต : </label>
                                        <input type="text" class="form-control col-5" name="Evaluate_products" required>
                                    </div>
                                    <div class="form-group col-md-5">
                                        <label for="inputEdu">แนวโน้มผลผลิต : </label>
                                        <input type="text" class="form-control col-5" name="Trend_productivity" required>
                                    </div>
                                    <div class="form-group col-md-5">
                                        <label for="inputEdu">ชื่อผู้ให้ข้อมูล :  </label>
                                        <input type="text" class="form-control col-5" name="Provider_Name" required>
                                    </div>
                                    <div class="form-group col-md-5">
                                        <label for="inputEdu">ชื่อผู้เก้บข้อมูล : </label>
                                        <input type="text" class="form-control col-5" name="Data_collector_name" required>
                                    </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>






                </div>
            </div>
        <!-- end content-->
    </div>
    <!--  end card  -->
   </div>
</div>
@endsection
