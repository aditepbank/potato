@extends('layouts.master')
@section('title','หน้าหลัก')
@section('content')
<nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
  <div class="container-fluid">
    <div class="navbar-wrapper">
      <div class="navbar-minimize">
        <button id="minimizeSidebar" class="btn btn-icon btn-round">
          <i class="nc-icon nc-minimal-right text-center visible-on-sidebar-mini"></i>
          <i class="nc-icon nc-minimal-left text-center visible-on-sidebar-regular"></i>
        </button>
      </div>
      <div class="navbar-toggle">
        <button type="button" class="navbar-toggler">
          <span class="navbar-toggler-bar bar1"></span>
          <span class="navbar-toggler-bar bar2"></span>
          <span class="navbar-toggler-bar bar3"></span>
        </button>
      </div>
      <a class="navbar-brand" href="#pablo">เพิ่มรายชื่อเกษตรกรเข้าแบบประเมิน</a>
    </div>
  </div>
</nav>
    

<div class="row">
  <div class="col-md-12">
    <div class="card">
        <div class="card-header">
          <h4 class="text-center">เพิ่มรายชื่อเกษตรกรเข้าแบบประเมิน</h4>
          <h4 align="center">{{ $Criteria->criteria_season_name }}&ensp;{{ $Criteria->criteria_season_detail }}</h4>
        </div>
            <div class="card-body">
                
                <form method="post" action="{{route('addfamerpost',$formid)}}" >
                    @csrf
                <button type="submit" class="btn btn-primary" style="margin-left: 17px;">เพิ่ม</button>
                <table id="add" class="table">
                    <thead class="thead-dark">
                        <tr>
                            <th width=7%></th>
                            <th width=7% scope="col-2">ลำดับ</th>
                            <th width=30% scope="col">ชื่อ - สกุล</th>
                            <th width=30%>โทรศัพท์</th> 
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($famer as $key => $item)
                        <tr>
                            <td ><input type="checkbox" name="{{$item->id}}"></td>
                            <td>{{ $key+1 }}</td>
                            <td>{{ $item->name_prefix	.''.$item->fname .' '. $item->lname}}</td>
                            <td>{{ $item->phone_number }}</td>
                            
                        </tr>
                    @endforeach   
                    </tbody>
                </table>
                </form>
            </div>
        <!-- end content-->
    </div>
    <!--  end card  -->
   </div>
</div>



@endsection

@push('scripts')
<script>
  $(document).ready( function () {
      $('#add').DataTable();
  } );
</script>
@endpush