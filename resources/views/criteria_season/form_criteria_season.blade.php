@extends('layouts.master')
@section('title','หน้าหลัก')
@section('content')
<nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
  <div class="container-fluid">
    <div class="navbar-wrapper">
      <div class="navbar-minimize">
        <button id="minimizeSidebar" class="btn btn-icon btn-round">
          <i class="nc-icon nc-minimal-right text-center visible-on-sidebar-mini"></i>
          <i class="nc-icon nc-minimal-left text-center visible-on-sidebar-regular"></i>
        </button>
      </div>
      <div class="navbar-toggle">
        <button type="button" class="navbar-toggler">
          <span class="navbar-toggler-bar bar1"></span>
          <span class="navbar-toggler-bar bar2"></span>
          <span class="navbar-toggler-bar bar3"></span>
        </button>
      </div>
      <a class="navbar-brand" href="#pablo">เพิ่มรายชื่อเกษตรกรเข้าแบบประเมิน</a>
    </div>
  </div>
</nav>


<div class="row">
  <div class="col-md-12">
    <div class="card">
        <div class="card-header">
          <h3 class="text-center">เพิ่มรายชื่อเกษตรกรเข้าแบบประเมิน</h3>
         
        {{-- <a a href="" type="button" class="btn btn-success" style="margin-left: 1400px;">เพิ่มฤดูกาลเพาะปลูก</a> --}}
        </div>
        <div class="card-body">
            <table id="add" class="table">
              <thead class="thead-dark">
                <tr>
                  <th scope="col">ลำดับ</th>
                  <th scope="col">รอบประเมิน</th>
                  <th scope="col">ช่วงระยะเวลา</th>
                  <th scope="col">รายละเอียดแบบประเมิน</th>
                  <th scope="col">เพิ่มเกษตรกรเข้าแบบประเมิน</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($Criteria as $key=> $item)
                <tr>
                <td>{{ $key+1 }}</td>
                  <td>{{ $item->criteria_season_name }}</td>
                  <td>{{ $item->criteria_season_detail }}</td>
                  <td>
                    <a href="{{ route('infocriteria',$item->id) }}" type="button" class="btn btn-info"><i class="fas fa-info-circle"></i></a>
                    {{-- <a href="" type="button" class="btn btn-warning"><i class="fas fa-user-edit"></i></i></a>
                    <a onclick="return confirm('คุณต้องการที่จะลบข้อมูลไหม')" href="" type="button" class="btn btn-danger"><i class="fas fa-trash"></i></a> --}}
                  </td>
                  <td>
                  <a href=" {{ route('addfamer',$item->id)}} " type="button" class="btn btn-success">เพิ่มเกษตรกรเข้าแบบประเมิน</a>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
        </div>
        <!-- end content-->
    </div>
    <!--  end card  -->
   </div>
</div>



@endsection

@push('scripts')
<script>
  $(document).ready( function () {
      $('#add').DataTable();
  } );
</script>
@endpush
