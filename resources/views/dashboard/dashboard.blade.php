@extends('layouts.master')
@section('title','หน้าหลัก')
@section('content')
  <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
    <div class="container-fluid">
      <div class="navbar-wrapper">
        <div class="navbar-minimize">
          <button id="minimizeSidebar" class="btn btn-icon btn-round">
            <i class="nc-icon nc-minimal-right text-center visible-on-sidebar-mini"></i>
            <i class="nc-icon nc-minimal-left text-center visible-on-sidebar-regular"></i>
          </button>
        </div>
        <div class="navbar-toggle">
          <button type="button" class="navbar-toggler">
            <span class="navbar-toggler-bar bar1"></span>
            <span class="navbar-toggler-bar bar2"></span>
            <span class="navbar-toggler-bar bar3"></span>
          </button>
        </div>
        <a class="navbar-brand">หน้าแรก</a>
      </div>
      
      <div class="input-group mb-12">
        <div class="col-10"></div>
        <select class="custom-select" id="select_seasion" onchange="get_data_dashborad()">
          {{-- <option selected>Choose...</option> --}}
          @foreach ($season as $item)
            <option value="{{ $item->id }}">{{ $item->criteria_season_name }}</option>
          @endforeach
        </select>
      </div>
    </div>
  </nav>


  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-body">

          <h5 class="text-center">ยินดีต้อนรับเข้าสู่ระบบคัดเลือกเกษตรกรเพื่อทำสัญญาซื้อขายกับโรงงาน</h5>
          <h5 class="text-center" id="criteria_season_detail"></h5>
          <br>
          <div class="row">
            {{-- <div class="col-md-6">
              <div class="card ">
                <div class="card-body "><div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;"><div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div></div><div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:200%;height:200%;left:0; top:0"></div></div></div>
                  <div id="columnchart_material" style="width: 750px; height: 450px;"></div>
                </div>
              </div>
            </div> --}}

            <div class="col-lg-4 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-body ">
                  <div class="row">
                    <div class="col-5 col-md-4">
                      <div class="icon-big text-center icon-warning">
                        <i class="fas fa-users text-info"></i>
                      </div>
                    </div>
                    <div class="col-7 col-md-8">
                      <div class="numbers">
                        <p class="card-category">จำนวนเกษตรกร</p>
                        <p class="card-title" id="farmer_count">0 คน</p><p>
                      </p></div>
                    </div>
                  </div>
                </div>
                <div class="card-footer ">
                  <hr>
                  {{-- <div class="stats"><a href="#pablo">รายละเอียดเพิ่มเติม</a></div> --}}
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-body ">
                  <div class="row">
                    <div class="col-5 col-md-4">
                      <div class="icon-big text-center icon-warning">
                        <i class="fas fa-user-check text-success"></i>
                      </div>
                    </div>
                    <div class="col-7 col-md-8">
                      <div class="numbers">
                        <p class="card-category">ประเมินแล้ว</p>
                        <p class="card-title" id="farmer_success">0 คน</p><p>
                      </p></div>
                    </div>
                  </div>
                </div>
                <div class="card-footer ">
                  <hr>
                  {{-- <div class="stats"><a href="#pablo" class="text-success">รายละเอียดเพิ่มเติม</a></div> --}}
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-body ">
                  <div class="row">
                    <div class="col-5 col-md-4">
                      <div class="icon-big text-center icon-warning">
                        <i class="fas fa-user-times text-danger"></i>
                      </div>
                    </div>
                    <div class="col-7 col-md-8">
                      <div class="numbers">
                        <p class="card-category">ยังไม่ประเมิน</p>
                        <p class="card-title" id="farmer_unsuccess">0 คน</p><p>
                      </p></div>
                    </div>
                  </div>
                </div>
                <div class="card-footer ">
                  <hr>
                  {{-- <div class="stats"><a href="#pablo" class="text-danger">รายละเอียดเพิ่มเติม</a></div> --}}
                </div>
              </div>
            </div>
          </div>
        {{-- <img src="{{asset('assets/img/header.jpg') }}" class="img-fluid" alt="Responsive image"> --}}
        </div>
      </div>
    </div>
  </div>
  

    

@endsection

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
  google.charts.load('current', {'packages':['bar']});
  google.charts.setOnLoadCallback(drawChart);

  function drawChart() {

    var text1 = ["ด้านผลิตภัณฑ์"];
    var score1 = {!! json_encode(array_values($arrayx['1'])) !!};
    var array1 = text1.concat(score1);

    var text2 = ["ด้านกระบวนการผลิต"];
    var score2 = {!! json_encode(array_values($arrayx['2'])) !!};
    var array2 = text2.concat(score2);

    var text3 = ["ด้านการขนส่ง"];
    var score3 = {!! json_encode(array_values($arrayx['3'])) !!};
    var array3 = text3.concat(score3);

    var text4 = ["ด้านการให้ความร่วมมือ"];
    var score4 = {!! json_encode(array_values($arrayx['4'])) !!};
    var array4 = text4.concat(score4);

    var data = google.visualization.arrayToDataTable([
      ['xxxxxxxx', 'ค่าคะแนนต่ำ', 'ค่าคะแนนกลาง', 'ค่าคะแนนสูง'],
      array1,
      array2,
      array3,
      array4
    ]);

    var options = {
      chart: {
        title: 'xxxx',
        subtitle: 'xxxx'
      },
      vAxis: {
          title: 'จำนวนค่าคะแนนการประเมิน'
        }
      };

    var chart = new google.charts.Bar(document.getElementById('columnchart_material'));

    chart.draw(data, google.charts.Bar.convertOptions(options));
  }
</script>
   
   




@push('scripts')
<script>
  $(document).ready( function () {
    get_data_dashborad()
  });

  function get_data_dashborad(){
    var id_seasion = document.getElementById('select_seasion').value
    $.ajax({
				type: 'get',
				url: '{{ url('dashboard/get_data/') }}' + '/' + id_seasion,
				dataType : 'json',
				success: function(data) {
          document.getElementById('criteria_season_detail').innerHTML = "รอบ  : " + data.Criteria.criteria_season_detail
          document.getElementById('farmer_count').innerHTML = data.famer + " คน"
          document.getElementById('farmer_success').innerHTML = data.famerx + " คน"
          document.getElementById('farmer_unsuccess').innerHTML = data.famery + " คน"
    		},
			})
  }
</script>

{{-- <script type="text/javascript">
  document.ready( function () {
      $('#select_seasion').value
      console.log("test")
  });
</script> --}}
@endpush




