<div class="sidebar" data-color="brown" data-active-color="danger">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red | yellow"
    -->
      <div class="logo">
        <a href="http://www.creative-tim.com" class="simple-text logo-mini">
          <div class="logo-image-small">
            {{-- <img src="../../assets/img/logo-small.png"> --}}
          </div>
        </a>
        {{-- <a href="http://www.creative-tim.com" class="simple-text logo-normal">
          ระบบคัดเลือกเกษตรกร
          <div class="logo-image-big">
            <img src="../../assets/img/logo-big.png">
          </div> 
        </a> --}}
      </div>
      <div class="sidebar-wrapper ps-container ps-theme-default ps-active-y" data-ps-id="2deb1a08-16b4-09d8-1fe9-a245c1f80de7">
        <div class="user">
          <div class="photo">
            <img src="{{ asset('assets/img/faces/images.png') }}">
          </div>
          <div class="info">
            <a data-toggle="collapse" href="#collapseExample" class="collapsed">
              <span>
                {{ Auth::user()->name }}
                {{-- <b class="caret"></b> --}}
              </span>
            </a>
            {{-- <div class="clearfix"></div> --}}
            {{-- <div class="collapse" id="collapseExample">
              <ul class="nav">
                <li>
                  <a href="#">
                    <span class="sidebar-mini-icon">MP</span>
                    <span class="sidebar-normal">My Profile</span>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <span class="sidebar-mini-icon">EP</span>
                    <span class="sidebar-normal">Edit Profile</span>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <span class="sidebar-mini-icon">S</span>
                    <span class="sidebar-normal">Settings</span>
                  </a>
                </li>
              </ul>
            </div> --}}
          </div>
        </div>
        <ul class="nav">
          <li>
          <a href=" {{ route('dashboard') }} ">
              <i class="nc-icon nc-bank"></i>
              <p>หน้าแรก</p>
            </a>
          </li>
          <li>
            <a data-toggle="collapse" href="#pagesExamples">
              <i class="fas fa-cog"></i>
              <p>
                ตั้งค่า
                <b class="caret"></b>
              </p>
            </a>
            <div class="collapse " id="pagesExamples">
              <ul class="nav">
              
                <li>
                  <a href="{{route('famer-list')}}">
                    <span class="sidebar-mini-icon"><i class="fas fa-user-cog"></i></span>
                    <span class="sidebar-normal"> จัดการรายชื่อเกษตรกร </span>
                  </a>
                </li>
                 <li>
                  <a href="{{Route('criteria')}}">
                    <span class="sidebar-mini-icon"><i class="fas fa-cogs"></i></span>
                    <span class="sidebar-normal"> ตั้งค่าเกณฑ์ให้คะแนน </span>
                  </a>
                </li>
                <!-- <li>
                  <a href="{{route('factory-detail')}}">
                    <span class="sidebar-mini-icon">s</span>
                    <span class="sidebar-normal"> ตั้งค่าข้อมูลโรงงาน </span>
                  </a>
                </li> -->
                <li>
                  <a href=" {{ route('formcriteriaseason')}} ">
                    <span class="sidebar-mini-icon"><i class="far fa-address-book"></i></span>
                    <span class="sidebar-normal"> เพิ่มรายชื่อเกษตรกรเข้าแบบประเมิน </span>
                  </a>
                </li>
                {{-- <li>
                  <a href=" {{ route('famer-list_plan')}} ">
                    <span class="sidebar-mini-icon"><i class="far fa-address-book"></i></span>
                    <span class="sidebar-normal"> เพิ่มข้อมูลการเพราะปลูก </span>
                  </a>
                </li> --}}
              </ul>
            </div>
          </li>
         
          <li>
            <a href="{{route('choose')}}">
              <i class="fas fa-check-circle"></i>
              <p>
                ประเมินเกษตรกร
              </p>
            </a>
            <div class="collapse " id="componentsExamples">
              <ul class="nav">
                <li>
                <a href="{{route('choose')}}">
                    <span class="sidebar-mini-icon"><i class="fas fa-check-circle"></i></span>
                    <span class="sidebar-normal"> ประเมินเกษตรกร </span>
                  </a>
                </li>
               <!--   <li>
                  <a href="  ">
                    <span class="sidebar-mini-icon">G</span>
                    <span class="sidebar-normal"> ประเมินเกษตร </span>
                  </a>
                </li>  -->
              </ul>
            </div>
          </li>
          <li>
            <a data-toggle="collapse" href="#tablesExamples">
              <i class="nc-icon nc-single-copy-04"></i>
              <p>
                รายงาน
                <b class="caret"></b>
              </p>
            </a>
            <div class="collapse " id="tablesExamples">
              <ul class="nav">
                <li>
                <a href="{{ route('chooseseasonreport')}}">
                    <span class="sidebar-mini-icon"><i class="fas fa-clipboard-list"></i></span>
                    <span class="sidebar-normal"> สรุปผลการประเมิน </span>
                  </a>
                </li>
                <li>
                  <a data-toggle="collapse" href="#mapsExamples">
                    <i class="fas fa-clipboard-list"></i>
                    <p>
                      วิเคราะห์ผลแยกตามหลักเกณฑ์ 
                      <b class="caret"></b>
                    </p>
                  </a>
                  <div class="collapse " id="mapsExamples">
                    <ul class="nav">
                     <li> 
                        <a href="{{route('chooseseasonmax')}}">
                          <span  class="sidebar-mini-icon"> </span>
                          <span class="sidebar-normal"> - - - - - - - - ผู้ที่ได้คะแนนสูงสุดแต่ละด้าน</span>
                          
                        </a>
                      </li> 
                      <li>
                        <a href="{{route('chooseseasonmin')}}">
                          <span class="sidebar-mini-icon"></i></span>
                          <span class="sidebar-normal"> - - - - - - - - ผู้ที่ได้คะแนนต่ำสุดแต่ละด้าน</span>
                        </a>
                      </li>
                
                <!-- <li>
                  <a href="../examples/maps/vector.html">
                    <span class="sidebar-mini-icon">VM</span>
                    <span class="sidebar-normal"> Vector Map </span>
                  </a>
                </li> -->
              </ul>
            </div>
          </li>
                <!-- <li>
                  <a href="{{ route('introducefarmerschooseseason')}}">
                    <span class="sidebar-mini-icon"><i class="fas fa-clipboard-list"></i></span>
                    <span class="sidebar-normal"> แนะนำเกษตรกรที่ควรทำสัญญา </span>
                  </a>
                </li>  -->
                <li>
                  <a href="{{ route('chooseseasonreportx')}}">
                    <span class="sidebar-mini-icon"><i class="fas fa-clipboard-list"></i></span>
                    <span class="sidebar-normal">  ผู้ที่ตกคุณสมบัติด้านการขนส่ง </span>
                  </a>
                </li>
                <li>
                  <a href="{{route('choose_season_cooperation_low')}}">
                    <span class="sidebar-mini-icon"><i class="fas fa-clipboard-list"></i></span>
                    <span class="sidebar-normal"> ผู้ที่ตกคุณสมบัติด้านการให้ความร่วมมือ</span>
                  </a>
                </li>
                <!-- <li>
                  <a href="{{route('select-famer')}}">
                    <span class="sidebar-mini-icon">ET</span>
                    <span class="sidebar-normal"> คัดกรองเกษตรกร </span>
                  </a>
                </li>  -->
                <!-- <li>
                  <a href="{{route('chooseseasonmax')}}">
                    <span class="sidebar-mini-icon">ET</span>
                    <span class="sidebar-normal"> 10ลำดับ เกษตรที่ได้คะแนนสูงสุด<br/>ในแต่ละด้าน</span>
                    
                  </a>
                </li> 
                <li>
                  <a href="{{route('chooseseasonmin')}}">
                    <span class="sidebar-mini-icon">ET</span>
                    <span class="sidebar-normal"> 10ลำดับ เกษตรที่ได้คะแนนน้อยสุด<br/>แต่ละด้าน </span>
                  </a>
                </li> -->
                <!-- <li>
                  <a href="{{route('famer-plant-info-total')}}">
                    <span class="sidebar-mini-icon">ET</span>
                    <span class="sidebar-normal"> ข้อมูลการเพาะปลูก/ผลผลิต </span>
                  </a>
                </li> -->
                 <!-- <li>
                  <a href="{{route('report-graph')}}">
                    <span class="sidebar-mini-icon">ET</span>
                    <span class="sidebar-normal"> กราฟ </span>
                  </a>
                </li>  -->
                <!-- <li>
                  <a href="{{route('reportpointfarmer-total')}}">
                    <span class="sidebar-mini-icon">ET</span>
                    <span class="sidebar-normal"> คะแนนรวมการประเมินเกษตรกร </span>
                  </a>
                </li>  -->
                <li>
                  <a href="{{route('chooseseasonmaxmin')}}">
                    <span class="sidebar-mini-icon"><i class="fas fa-clipboard-list"></i></span>
                    <span class="sidebar-normal"> วิเคราะห์ผลประเมินสูงสุด ต่ำสุด </span>
                  </a>
                </li>
              </ul>
            </div>
          </li>
          

          <li>
            {{-- <a href="{{ route('x') }}"> --}}
              <a href="{{ route('logout') }}">
              <i class="nc-icon nc-box"></i>
              <p>ออกจากระบบ</p>
            </a>
          </li>
        <!-- <li> 
            <a href="../../examples/calendar.html">
              <i class="nc-icon nc-calendar-60"></i>
              <p>Calendar</p>
            </a> 
          </li> -->

        </ul>

      <div class="ps-scrollbar-x-rail" style="width: 260px; left: 0px; bottom: -151.111px;"><div class="ps-scrollbar-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps-scrollbar-y-rail" style="top: 151.111px; height: 619px; right: 0px;"><div class="ps-scrollbar-y" tabindex="0" style="top: 107px; height: 439px;"></div></div></div>
    </div>

