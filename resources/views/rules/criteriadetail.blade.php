@extends('layouts.master')
@section('title','หน้าหลัก')
@section('content')
<nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
  <div class="container-fluid">
    <div class="navbar-wrapper">
      <div class="navbar-minimize">
        <button id="minimizeSidebar" class="btn btn-icon btn-round">
          <i class="nc-icon nc-minimal-right text-center visible-on-sidebar-mini"></i>
          <i class="nc-icon nc-minimal-left text-center visible-on-sidebar-regular"></i>
        </button>
      </div>
      <div class="navbar-toggle">
        <button type="button" class="navbar-toggler">
          <span class="navbar-toggler-bar bar1"></span>
          <span class="navbar-toggler-bar bar2"></span>
          <span class="navbar-toggler-bar bar3"></span>
        </button>
      </div>
      <a class="navbar-brand" href="#pablo">ตั้งค่าเกณฑ์ให้คะแนน</a>
    </div>
  </div>
</nav>
<div class="row">
  <div class="col-md-12">
    <div class="card">
        <div class="card-header">
        <h3 class="text-center">ตั้งค่าปัจจัยรอง</h3>
          <h4 align="center">{{ $Criteria->criteria_season_name }}&ensp;{{ $Criteria->criteria_season_detail }}</h4>
          <a href="{{ route('criteriamain',$Criteria_main->criteria_id) }}" type="button" class="btn btn-danger">ย้อนกลับ</a>
          {{-- <a href="{{ route('createcriteriadetail',$id) }}" type="button" class="btn btn-success" >เพิ่มปัจจัยรอง</a> --}}
          <button type="button" class="btn btn-success"  data-toggle="modal" data-target="#edit" data-create="{{$id}}" >เพิ่มปัจจัยรอง</button>
        </div>
        <br>
          <h5 align="top">&ensp;&ensp;&ensp;ปัจจัยรองของ{{ $Criteria_main->criteria_main_name }}</h5>
        <div class="card-body">
            <table id="add" class="table">
              <thead class="thead-dark">
                <tr>
                  <th scope="col" align="center">ลำดับ</th>
                  <th scope="col" align="center">รายละเอียดปัจจัยรอง</th>
                  <th scope="col" align="center">เกณฑ์ค่าคะแนนต่ำ</th>
                  <th scope="col" align="center">เกณฑ์ค่าคะแนนกลาง</th>
                  <th scope="col" align="center">เกณฑ์ค่าคะแนนสูง</th>
                  <th scope="col" align="center">ค่าน้ำหนัก</th>
                  <th></th>
                  {{-- <th></th> --}}
                </tr>
              </thead>
              <tbody>
                @foreach ($Criteria_detail as $item)
                <tr>
                  <td>{{ $item->id_detail }}.</td>
                  <td>{{ $item->criteria_detail_name }}</td>
                  <td>{{ $item->criteria_detail_low	 }}</td>
                  <td>{{ $item->criteria_detail_middle }}</td>
                  <td>{{ $item->criteria_detail_high }}</td>
                  <td>{{ $item->criteria_detail_Weight }}</td>
                {{-- <td><a href="{{ route('editcriteriadetail',$item->id) }}" type="button" class="btn btn-warning"><i class="fas fa-user-edit"></i></i></a></td> --}}
                  <td>
                    <button type="button" class="btn btn-warning"  
                    data-id="{{$item->id}}" 
                    data-criteria_detail_name="{{$item->criteria_detail_name}}"  
                    data-low="{{$item->criteria_detail_low}}" 
                    data-hiddle="{{$item->criteria_detail_middle}}"  
                    data-high="{{$item->criteria_detail_high}}" 
                    data-Weight="{{$item->criteria_detail_Weight}}" 
                    data-toggle="modal" data-target="#editdetail" ><i class="fas fa-edit"></i></button>
                    <button type="button" class="btn btn-danger" data-id="{{$item->id}}" data-toggle="modal" data-target="#del" ><i class="fas fa-trash-alt"></i></button>
                  </td>
              </tr>
                @endforeach
              </tbody>
            </table>
        </div>
        <!-- end content-->
    </div>
    <!--  end card  -->
   </div>
</div>



    <div class="modal fade bd-example-modal-lg" id="edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">เพิ่มข้อมูลปัจจัยรอง</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>

          <form method="POST" action=" {{route('createcriteriadetailpost',[$id, $testid])}} ">

            {{ csrf_field() }}

          <div class="modal-body">

              <div class="form-group">
                <label for="title" class="col-form-label">ปัจจัยรองข้อที่</label>
                <input type="text" class="form-control" id="id_detail"  name="detail" required>
              </div>

              <div class="form-group">
                <label for="title" class="col-form-label">ปัจจัยรอง</label>
                <input type="text" class="form-control" id="title"  name="criteria_detail_name" required>
              </div>

              <div class="form-group">
                <label for="low" class="col-form-label">เกณฑ์ค่าคะแนนต่ำ</label>
                <input type="text" class="form-control" id="low"  name="criteria_detail_low" required>
              </div>

              <div class="form-group">
                <label for="middle" class="col-form-label">เกณฑ์ค่าคะแนนกลาง</label>
                <input type="text" class="form-control" id="middle"  name="criteria_detail_middle" required>
              </div>

              <div class="form-group">
                <label for="high" class="col-form-label">เกณฑ์ค่าคะแนนสูง</label>
                <input type="text" class="form-control" id="high"  name="criteria_detail_high" required>
              </div>

              <div class="form-group">
                <label for="weight" class="col-form-label">ค่าน้ำหนัก</label>
                <input type="number" class="form-control" id="weight"  step="any" name="criteria_detail_Weight" required>
              </div>
            {{-- <input type="hidden" class="form-control" id="weight"  step="any" name="id_detail" required value="{{ request()->key }}"> --}}
          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">ยกเลิก</button>
            <button type="submit" class="btn btn-primary">บันทึกข้อมูล</button>
          </div>

        </form>
        
        </div>
      </div>
    </div>


    <div class="modal fade bd-example-modal-lg" id="editdetail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">แก้ไขข้อมูลปัจจัยรอง</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>

          <form method="POST" action=" {{route('editcriteriadetail')}} ">

            {{ csrf_field() }}

          <div class="modal-body">
            <input type="hidden" name="id" id="id" value="">
              <div class="form-group">
                <label for="criteriamain" class="col-form-label">ปัจจัยรอง</label>
                <input type="text" class="form-control" id="criteria_detail_name"  name="criteria_detail_name" required>
              </div>

              <div class="form-group">
                <label for="criteriadetail" class="col-form-label">เกณฑ์ค่าคะแนนต่ำ</label>
                <input type="text" class="form-control" id="low" name="criteria_detail_low" required>
              </div>

              <div class="form-group">
                <label for="criteriamain" class="col-form-label">เกณฑ์ค่าคะแนนกลาง</label>
                <input type="text" class="form-control" id="hiddle"  name="criteria_detail_middle" required>
              </div>

              <div class="form-group">
                <label for="criteriadetail" class="col-form-label">เกณฑ์ค่าคะแนนสูง</label>
                <input type="text" class="form-control" id="high" name="criteria_detail_high" required>
              </div>

              <div class="form-group">
                <label for="criteriadetail" class="col-form-label">ค่าน้ำหนัก</label>
                <input type="number" class="form-control" id="weight"  step="any" name="criteria_detail_Weight" required>
              </div>
            
          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">ยกเลิก</button>
            <button type="submit" class="btn btn-primary">บันทึกข้อมูล</button>
          </div>

        </form>
        
        </div>
      </div>
    </div>


    <div class="modal fade bd-example-modal-lg" id="del" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">ลบข้อมูลปัจจัยรอง</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>

          <form method="post" action="{{route('deletecriteriadetail')}}">

            {{ csrf_field() }}

          <div class="modal-body">
              <input type="hidden" name="id" id="id" value="">
              <p class="text-center">คุณต้องการที่จะลบข้อมูลใช่หรือไม่</p>
          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">ยกเลิก</button>
            <button type="submit" class="btn btn-success">ลบข้อมูล</button>
          </div>

        </form>
        
        </div>
      </div>
    </div>

@endsection

@push('scripts')
<script>
  $(document).ready( function () {
      $('#add').DataTable();
  } );
</script>
@endpush
