@extends('layouts.master')
@section('title','หน้าหลัก')
@section('content')
<nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
  <div class="container-fluid">
    <div class="navbar-wrapper">
      <div class="navbar-minimize">
        <button id="minimizeSidebar" class="btn btn-icon btn-round">
          <i class="nc-icon nc-minimal-right text-center visible-on-sidebar-mini"></i>
          <i class="nc-icon nc-minimal-left text-center visible-on-sidebar-regular"></i>
        </button>
      </div>
      <div class="navbar-toggle">
        <button type="button" class="navbar-toggler">
          <span class="navbar-toggler-bar bar1"></span>
          <span class="navbar-toggler-bar bar2"></span>
          <span class="navbar-toggler-bar bar3"></span>
        </button>
      </div>
      <a class="navbar-brand" href="#pablo">ตั้งค่าเกณฑ์ให้คะแนน</a>
    </div>
  </div>
</nav>

<div class="row">
  <div class="col-md-12">
    <div class="card">
        <div class="card-header">
          <h3 class="text-center">ตั้งค่าแบบประเมิน</h3>
         
        {{-- <a a href="{{ route('createcriteria') }}" type="button" class="btn btn-success" >เพิ่มฤดูกาลเพาะปลูก</a> --}}
        <button type="button" class="btn btn-success"  data-toggle="modal" data-target="#create" >เพิ่มรอบประเมิน</button>
        {{-- <a href="{{ route('createcriteria') }}" class="btn btn-success" style="margin-left: 14px;">เพิ่มฤดูกาลเพาะปลูก</a> --}}
        </div>
        <div class="card-body">
            <table id="add" class="table">
              <thead class="thead-dark">
                <tr>
                  <th scope="col">ลำดับ</th>
                  <th scope="col">รอบประเมิน</th>
                  <th scope="col">ช่วงระยะเวลา</th>
                  <th scope="col" width="20%">รายละเอียดแบบประเมิน</th>
                  <th scope="col" width="20%">เพิ่มปัจจัยหลัก</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                @foreach ($Criteria as $key=> $item)
                <tr>
                <td>{{ $key+1 }}</td>
                  <td>{{ $item->criteria_season_name }}</td>
                  <td>{{ $item->criteria_season_detail }}</td>
                  <td>
                    <a href="{{ route('infocriteria',$item->id) }}" type="button" class="btn btn-info"><i class="fas fa-info-circle"></i></a>
                    {{-- <a href="" type="button" class="btn btn-warning"><i class="fas fa-user-edit"></i></i></a>
                    <a onclick="return confirm('คุณต้องการที่จะลบข้อมูลไหม')" href="" type="button" class="btn btn-danger"><i class="fas fa-trash"></i></a> --}}
                  </td>
                  <td>
                  <a href="{{ route('criteriamain',$item->id) }}" type="button" class="btn btn-success">เพิ่มปัจจัยหลักแบบประเมิน</a>
                  </td>
                  <td>
                    <button type="button" class="btn btn-warning"  data-id="{{$item->id}}" data-name="{{$item->criteria_season_name}}"  data-detail="{{$item->criteria_season_detail}}" data-toggle="modal" data-target="#edit" ><i class="fas fa-edit"></i></button>
                    <button type="button" class="btn btn-danger" data-id="{{$item->id}}" data-toggle="modal" data-target="#del" ><i class="fas fa-trash-alt"></i></button>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
        </div>
        <!-- end content-->
    </div>
    <!--  end card  -->
   </div>
</div>


    <div class="modal fade bd-example-modal-lg" id="create" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">เพิ่มข้อมูลรอบประเมิน</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>

          <form method="POST" action=" {{route('createcriteriapost')}} ">

            {{ csrf_field() }}

          <div class="modal-body">

              <div class="form-group">
                <label for="title" class="col-form-label">ชื่อฤดูกาล:</label>
                <input type="text" class="form-control" id="title"  name="criteria_season_name" required>
              </div>

              <div class="form-group">
                <label for="detail" class="col-form-label">กำหนดการ:</label>
                <input type="text" class="form-control" id="detail"  name="criteria_season_detail" required>
              </div>
            
          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">ยกเลิก</button>
            <button type="submit" class="btn btn-success">บันทึกข้อมูล</button>
          </div>

        </form>
        
        </div>
      </div>
    </div>

    <div class="modal fade bd-example-modal-lg" id="edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">แก้ไขข้อมูลรอบประเมิน</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>

          <form method="post" action="{{route('updatecriteria')}}">

            {{ csrf_field() }}

          <div class="modal-body">
              <input type="hidden" name="id" id="id" value="">
              <div class="form-group">
                <label for="name" class="col-form-label">ชื่อฤดูกาล:</label>
                <input type="text" class="form-control" id="name"  name="criteria_season_name"  required>
              </div>

              <div class="form-group">
                <label for="detail" class="col-form-label">กำหนดการ:</label>
              <input type="text" class="form-control" id="detail"  name="criteria_season_detail"  required>
              </div>
            
          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">ยกเลิก</button>
            <button type="submit" class="btn btn-success">บันทึกข้อมูล</button>
          </div>

        </form>
        
        </div>
      </div>
    </div>


    <div class="modal fade bd-example-modal-lg" id="del" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">ลบข้อมูลรอบประเมิน</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>

          <form method="post" action="{{route('delete')}}">

            {{ csrf_field() }}

          <div class="modal-body">
              <input type="hidden" name="id" id="id" value="">
              <p class="text-center">คุณต้องการที่จะลบข้อมูลใช่หรือไม่</p>
          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">ยกเลิก</button>
            <button type="submit" class="btn btn-success">ลบข้อมูล</button>
          </div>

        </form>
        
        </div>
      </div>
    </div>



@endsection




@push('scripts')
<script>
  $(document).ready( function () {
      $('#add').DataTable();
  } );
</script>
@endpush
