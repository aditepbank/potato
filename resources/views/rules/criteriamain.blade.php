@extends('layouts.master')
@section('title','หน้าหลัก')
@section('content')
<nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
  <div class="container-fluid">
    <div class="navbar-wrapper">
      <div class="navbar-minimize">
        <button id="minimizeSidebar" class="btn btn-icon btn-round">
          <i class="nc-icon nc-minimal-right text-center visible-on-sidebar-mini"></i>
          <i class="nc-icon nc-minimal-left text-center visible-on-sidebar-regular"></i>
        </button>
      </div>
      <div class="navbar-toggle">
        <button type="button" class="navbar-toggler">
          <span class="navbar-toggler-bar bar1"></span>
          <span class="navbar-toggler-bar bar2"></span>
          <span class="navbar-toggler-bar bar3"></span>
        </button>
      </div>
      <a class="navbar-brand" href="#pablo">ตั้งค่าเกณฑ์ให้คะแนน</a>
    </div>
  </div>
</nav>
<div class="row">
  <div class="col-md-12">
    <div class="card">
        <div class="card-header">
        <h3 class="text-center">ตั้งค่าปัจจัยหลัก</h3>
        
        <h4 align="center">{{ $Criteria->criteria_season_name }}&ensp;{{ $Criteria->criteria_season_detail }}</h4>
        <a href="{{ route('criteria') }}" type="button" class="btn btn-danger">ย้อนกลับ</a>
        {{-- <a href="{{ route('createcriteriamain',$testid) }}" type="button" class="btn btn-success">เพิ่มปัจจัยหลัก</a> --}}
        <button type="button" class="btn btn-success"  data-toggle="modal" data-target="#create" data-create="{{$testid}}" >เพิ่มปัจจัยหลัก</button>
        
        </div>
        <div class="card-body">
            <table id="add" class="table">
              <thead class="thead-dark">
                <tr>
                  {{-- <th scope="col">ฤดูกาล</th> --}}
                  <th scope="col" width="10%">ข้อ</th>
                  <th scope="col">หัวข้อปัจจัยหลัก</th>
                  <th scope="col">ค่าน้ำหนัก</th>
                  <th scope="col" width="35%">เพิ่มปัจจัยรอง</th>
                  <th scope="col"></th>
                </tr>
              </thead>
              <tbody>
                @foreach($Criteria_main as $key=> $item)
                <tr>
                  {{-- <td>{{ $item->criteria_id }}</td> --}}
                  <td>{{ $key + 1 }}.</td>
                  <td>{{ $item->criteria_main_name }}</td>
                  <td>{{ $item->criteria_main_Weight }}</td>
                  <td>
                    <a href="{{ route('criteriadetail',[$key+1,$testid, 'key' => $key+1]) }}" type="button" class="btn btn-success">เพิ่มปัจจัยรองแบบประเมิน</a>
                  </td>
                  <td>
                    <button type="button" class="btn btn-warning"  data-id="{{$item->id}}" data-criteriamain="{{$item->criteria_main_name}}"  data-criteriadetail="{{$item->criteria_main_Weight}}" data-toggle="modal" data-target="#editmain"><i class="fas fa-edit"></i></button>
                    <button type="button" class="btn btn-danger" data-id="{{$item->id}}" data-toggle="modal" data-target="#del" ><i class="fas fa-trash-alt"></i></button>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
        </div>
        <!-- end content-->
    </div>
    <!--  end card  -->
   </div>
</div>



    <div class="modal fade bd-example-modal-lg" id="create" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">เพิ่มข้อมูลปัจจัยหลัก</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>

          <form method="POST" action=" {{route('createcriteriamainpost',$testid)}} ">

            {{ csrf_field() }}

          <div class="modal-body">

              <div class="form-group">
                <label for="title" class="col-form-label">ปัจหลักข้อที่</label>
                <input type="text" class="form-control" id="id_main"  name="id_main" required>
              </div>
              
              <div class="form-group">
                <label for="title" class="col-form-label">หัวข้อปัจจัยหลัก</label>
                <input type="text" class="form-control" id="criteriamain"  name="criteria_main_name" required>
              </div>

              <div class="form-group">
                <label for="detail" class="col-form-label">ค่าน้ำหนัก</label>
                <input type="number" class="form-control" id="criteridetail"  step="any" name="criteria_main_Weight" required>
              </div>
            
          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">ยกเลิก</button>
            <button type="submit" class="btn btn-primary">บันทึกข้อมูล</button>
          </div>

        </form>
        
        </div>
      </div>
    </div>


    <div class="modal fade bd-example-modal-lg" id="editmain" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">แก้ไขข้อมูลปัจจัยหลัก</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>

          <form method="POST" action=" {{route('updatecriteriamain')}} ">

            {{ csrf_field() }}

          <div class="modal-body">
            <input type="hidden" name="id" id="id" value="">
              <div class="form-group">
                <label for="criteriamain" class="col-form-label">หัวข้อปัจจัยหลัก</label>
                <input type="text" class="form-control" id="criteriamain"  name="criteria_main_name" required>
              </div>

              <div class="form-group">
                <label for="criteriadetail" class="col-form-label">ค่าน้ำหนัก</label>
                <input type="number" class="form-control" id="criteriadetail"  step="any" name="criteria_main_Weight" required>
              </div>
            
          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">ยกเลิก</button>
            <button type="submit" class="btn btn-primary">บันทึกข้อมูล</button>
          </div>

        </form>
        
        </div>
      </div>
    </div>


    <div class="modal fade bd-example-modal-lg" id="del" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">ลบข้อมูลปัจจัยหลัก</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>

          <form method="post" action="{{route('deletemain')}}">

            {{ csrf_field() }}

          <div class="modal-body">
              <input type="hidden" name="id" id="id" value="">
              <p class="text-center">คุณต้องการที่จะลบข้อมูลใช่หรือไม่</p>
          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">ยกเลิก</button>
            <button type="submit" class="btn btn-success">ลบข้อมูล</button>
          </div>

        </form>
        
        </div>
      </div>
    </div>


@endsection

@push('scripts')
<script>
  $(document).ready( function () {
      $('#add').DataTable();
  } );
</script>
@endpush
