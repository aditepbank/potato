@extends('layouts.master')
@section('title','หน้าหลัก')
@section('content')
<nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
  <div class="container-fluid">
    <div class="navbar-wrapper">
      <div class="navbar-minimize">
        <button id="minimizeSidebar" class="btn btn-icon btn-round">
          <i class="nc-icon nc-minimal-right text-center visible-on-sidebar-mini"></i>
          <i class="nc-icon nc-minimal-left text-center visible-on-sidebar-regular"></i>
        </button>
      </div>
      <div class="navbar-toggle">
        <button type="button" class="navbar-toggler">
          <span class="navbar-toggler-bar bar1"></span>
          <span class="navbar-toggler-bar bar2"></span>
          <span class="navbar-toggler-bar bar3"></span>
        </button>
      </div>
      <a class="navbar-brand" href="#pablo">ตั้งค่าเกณฑ์ให้คะแนน</a>
    </div>
  </div>
</nav>
<div class="row">
  <div class="col-md-12">
    <div class="card">
        <div class="card-header">
          <h4 class="card-title"></h4>
          </div>
            <div class="card-body">
            
            <div class="">
           
      <div class="row">
        <div class="col-md-12">
          <h3 align="center"> แบบประเมินให้คะแนนเกษตร </h3>
          <h4 align='center'>{{ $Criteria->criteria_season_name}} {{ $Criteria->criteria_season_detail}} </h4><br>
          <form id="formq" name="formq" method="POST" action="">
            @csrf
            <table width="100%" border="1" align="center" cellpadding="0" cellspacing="0">
              <tr>
                <td width="15%" rowspan="2" align="center"><strong>รายการ</strong></td>
              </tr>
              <tr>
                <td width="15%" align="center"><strong>หลักเกณฑ์สูง</strong></td>
                <td width="15%" align="center"><strong>หลักเกณฑ์กลาง</strong></td>
                <td width="15%" align="center"><strong>หลักเกณฑ์ต่ำ</strong></td>
                <td width="15%" align="center"><strong>ค่าน้ำหนัก</strong></td>
              </tr>
              @foreach ($Criteria_main as $key=> $item)
                <tr>
                  <td height="30" colspan="7" bgcolor="#F4F4F4"><strong>&ensp;{{ $key + 1 }}. {{ $item->criteria_main_name }}</strong></td>
                </tr>
                @foreach ($item->criteria_main as $keyx=> $itemx)
                <tr>
                  <td height="30">&ensp;&ensp;{{ $key + 1 }}.{{ $keyx + 1 }} {{ $itemx->criteria_detail_name }}</td>
                  <td height="30" align="">{{ $itemx->criteria_detail_low }}</td>
                  <td height="30" align="">{{ $itemx->criteria_detail_middle }}</td>
                  <td height="30" align="">{{ $itemx->criteria_detail_high }}</td>
                  <td height="30" align="center">{{ $itemx->criteria_detail_Weight }}</td>
                </tr>
                @endforeach
              @endforeach  
            </table>
          </form>
          
        </div>
      </div>
      </div>
        <!-- end content-->
    </div>
    <!--  end card  -->
   </div>
</div>
@endsection
