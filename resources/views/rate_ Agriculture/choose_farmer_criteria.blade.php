@extends('layouts.dashboard')
@section('title','หน้าหลัก')
@section('content')


<div class="row">
  <div class="col-md-12">
    <div class="card">
        <div class="card-header">
          <h4 class="text-center">บัญขีรายชื่อเกษตรกรผู้เข้ารับการประเมิน</h4>
          <h4 align="center">รอบประเมิน&ensp;{{ $Criteria->criteria_season_detail }}</h4>
          </div>
            <div class="card-body">
            <table id="add" class="table">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">ลำดับ</th>
                        <th scope="col">ชื่อเกษตรกร</th>
                        <th scope="col">ที่อยู่</th>
                        <th class="disabled-sorting text-right">ประเมิน</th>
                    </tr>
                </thead>
                <!-- <tfoot>
                    <tr>
                        <th>ลำดับ</th>
                        <th>ชื่อเกษตรกร</th>
                        <th class="disabled-sorting text-right">ประเมิน</th>
                    </tr>
              </tfoot> -->
                <tbody>
                @foreach($Form_criteria_season as $key => $item)
                    <tr>
                        <th>{{ $key+1}}</th>
                        <td>{{ $item->getfarmer->name_prefix .''. $item->getfarmer->fname .' '. $item->getfarmer->lname}}</td>
                        <td>{{ $item->getfarmer->adderss}}</td>
                        <td class="text-right">
                            <a href="{{route('rate_famer',$item->id)}}" class="btn btn-success">ให้ค่าคะแนน</a>
                        </td>
                    </tr>
                  @endforeach
                </tbody>
            </table>

               
          

            
         
      </div>
        <!-- end content-->
    </div>
    <!--  end card  -->
   </div>
</div>



@endsection

@push('scripts')
<script>
  $(document).ready( function () {
      $('#add').DataTable();
  } );
</script>
@endpush