@extends('layouts.dashboard')
@section('title','หน้าหลัก')
@section('content')


<div class="row">
  <div class="col-md-12">
    <div class="card">
        <div class="card-header">
          <h4 class="card-title">ประเมินให้คะแนนเกษตรกร</h4>
          </div>
            <div class="card-body">
            <table id="add" class="table">
                <thead class="thead-dark">
                    <tr>
                    
                    <th scope="col">รหัสเกษตรกร</th>
                    <th scope="col">ชื่อ-สกุล</th>
                   
                    

                    <th class="disabled-sorting text-right">Actions</th>
                    </tr>
                </thead>
                <tfoot>
                <tr>
                    
                    <th>รหัสเกษตรกร</th>
                    <th>ชื่อ-สกุล</th>
                    
                    
                    
                    <th class="disabled-sorting text-right">Actions</th>
                </tr>
              </tfoot>
                <tbody>
                @foreach($famer as $item)
                    <tr>
                    
                        <th>{{ $item->famer_code}}</th>
                        <td>{{ $item->name_prefix .' '.$item->fname .' '. $item->lname}}</td>
                        

                        <td class="text-right">
                        <a href="{{route('rate_famer',$item->id)}}" class="btn btn-success">ประเมิน</a>
                        
                        </td>
                    </tr>
                  @endforeach      
                </tbody>
                </table>

               
          

            
         
      </div>
        <!-- end content-->
    </div>
    <!--  end card  -->
   </div>
</div>



@endsection

@push('scripts')
<script>
  $(document).ready( function () {
      $('#add').DataTable();
  } );
</script>
@endpush