@extends('layouts.master')
@section('title','หน้าหลัก')
@section('content')
<nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
  <div class="container-fluid">
    <div class="navbar-wrapper">
      <div class="navbar-minimize">
        <button id="minimizeSidebar" class="btn btn-icon btn-round">
          <i class="nc-icon nc-minimal-right text-center visible-on-sidebar-mini"></i>
          <i class="nc-icon nc-minimal-left text-center visible-on-sidebar-regular"></i>
        </button>
      </div>
      <div class="navbar-toggle">
        <button type="button" class="navbar-toggler">
          <span class="navbar-toggler-bar bar1"></span>
          <span class="navbar-toggler-bar bar2"></span>
          <span class="navbar-toggler-bar bar3"></span>
        </button>
      </div>
      <a class="navbar-brand" href="#pablo">ประเมินเกษตร</a>
    </div>
  </div>
</nav>
<div class="row">
  <div class="col-md-12">
    <div class="card">
        <div class="card-header">
          <h4 class="card-title"></h4>
          </div>
            <div class="card-body">
              <div class="">
                
      <div class="row">
        <div class="col-md-12">
        
          <h3 align="center"> ประเมินเกษตร </h3> 
          <h5 align="center">รอบประเมิน {{ $Criteria->criteria_season_detail}} </h5><br>
              <p>ชื่อ - นามสกุล &nbsp;&nbsp;: &nbsp;&nbsp;<b>{{ $famer->name_prefix .' '.$famer->fname .' '. $famer->lname}} </b></p>
              <p>ที่อยู่ &nbsp;&nbsp;: &nbsp;&nbsp;<b>{{ $famer->adderss}} </b></p>
              <p>โทรศัพท์ &nbsp;&nbsp;: &nbsp;&nbsp; <b>{{ $famer->phone_number}}</b> </p>
              {{-- <p>พื้นที่เพาะปลูกจำนวณ  : {{ $famer->garden_detail->total_garden}} ไร่</p> <br> --}}
          <form id="formq" name="formq" method="POST" action="">
            @csrf
            <table width="100%" border="1" align="center" cellpadding="0" cellspacing="0">
              <tr>
                <td width="15%" rowspan="2" align="center"><strong>รายการ</strong></td>
                
              </tr>
              <tr>
                <td width="15%" align="center"><strong>หลักเกณฑ์ต่ำ</strong></td>
                <td width="15%" align="center"><strong>หลักเกณฑ์กลาง</strong></td>
                <td width="15%" align="center"><strong>หลักเกณฑ์สูง</strong></td>
                <td width="5%" align="center"><strong>ต่ำ</strong></td>
                <td width="5%" align="center"><strong>กลาง</strong></td>
                <td width="5%" align="center"><strong>สูง</strong></td>
              </tr>
              @foreach ($Criteria_main as $key=> $item)
                <tr>
                  <td height="30" colspan="7" bgcolor="#F4F4F4"><strong>&ensp;{{ $key + 1 }}. {{ $item->criteria_main_name }}</strong></td>
                </tr>
                @foreach ($item->criteria_main as $keyx=> $itemx)
                <tr>
                  <td height="30">&ensp;&ensp;{{ $key + 1 }}.{{ $keyx + 1 }} {{ $itemx->criteria_detail_name }}</td>
                  <td height="30" align="">{{ $itemx->criteria_detail_low }}</td>
                  <td height="30" align="">{{ $itemx->criteria_detail_middle }}</td>
                  <td height="30" align="">{{ $itemx->criteria_detail_high }}</td>
                  <td height="30" align="center"><input type="radio" name="{{ $itemx->criteria_main_id }}_{{ $itemx->id_detail }}"  value="0.2" required/></td>
                  <td height="30" align="center"><input type="radio" name="{{ $itemx->criteria_main_id }}_{{ $itemx->id_detail }}"  value="0.6" required/></td>
                  <td height="30" align="center"><input type="radio" name="{{ $itemx->criteria_main_id }}_{{ $itemx->id_detail }}"  value="1" required/></td>
                </tr>
                @endforeach
              @endforeach  
            </table>
            <hr>
            <br>
            <button  type="submit" name="save" class="btn btn-primary">บันทึกข้อมูลการประเมิน</button>
          </form>
          <br /><br />
        </div>
      </div>
      </div>
        <!-- end content-->
    </div>
    <!--  end card  -->
   </div>
</div>
@endsection

