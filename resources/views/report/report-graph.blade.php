@extends('layouts.dashboard')
@section('title','แสดงกราฟ')
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card">
        <div class="card-header">
          <h3 class="text-center">สรุปผลการประเมินฤดูกาล</h3>

            <div class="card-body">

            <div class="content">
        <p>Simple yet flexible JavaScript charting for designers & developers. Made by our friends from
          <a target="_blank" href="https://github.com/gionkunz/chartist-js">Chartist.js</a>. Please check
          <a target="_blank" href="https://gionkunz.github.io/chartist-js/api-documentation.html">the full documentation</a>.</p>
        <div class="row">
          <div class="col-md-6">
            <div class="card card-chart">
              <div class="card-header">
                <h5 class="card-title">ดูคะแนนฤดูกาล</h5>
                <p class="card-category">Line Chart</p>
              </div>
              <div class="card-body">
                <canvas id="activeUsers"></canvas>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="card card-chart">
              <div class="card-header">
                <h5 class="card-title">NASDAQ: AAPL</h5>
                <p class="card-category">Line Chart with Points</p>
              </div>
              <div class="card-body">
                <canvas id="chart-area"></canvas>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="card card-chart">
              <div class="card-header">
                <h5 class="card-title">Views</h5>
                <p class="card-category">Bar Chart</p>
              </div>
              <div class="card-body">
                <canvas id="chartViews"></canvas>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="card car-chart">
              <div class="card-header">
                <h5 class="card-title">Activity</h5>
                <p class="card-category">Multiple Bars Chart</p>
              </div>
              <div class="card-body">
                <canvas id="chartActivity"></canvas>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4">
            <div class="card ">
              <div class="card-header ">
                <h5 class="card-title">Email Statistics</h5>
                <p class="card-category">Last Campaign Performance</p>
              </div>
              <div class="card-body ">
                <canvas id="chartEmail" class="ct-chart ct-perfect-fourth" width="456" height="300"></canvas>
              </div>
              <div class="card-footer ">
                <div class="legend">
                  <i class="fa fa-circle text-info"></i> Open
                </div>
                <hr>
                <div class="stats">
                  <i class="fa fa-calendar"></i> Number of emails sent
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-8">
            <div class="card ">
              <div class="card-header ">
                <h5 class="card-title">Users Behavior</h5>
                <p class="card-category">24 Hours performance</p>
              </div>
              <div class="card-body ">
                <canvas id=chartHours width="400" height="100"></canvas>
              </div>
              <div class="card-footer ">
                <hr>
                <div class="stats">
                  <i class="fa fa-history"></i> Updated 3 minutes ago
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
            </div>
        </div>
        <!-- end content-->
    </div>
    <!--  end card  -->
  </div>
</div>
@endsection
@push('scripts')
<script>
const dataG = [1, 1, 5, 3, 4, 9, 1, 4, 8, 6];
const chartColor = "#FFFFFF";
$(document).ready(function() {
      // Javascript method's body can be found in assets/assets-for-demo/js/demo.js
    ctx = document.getElementById('activeUsers').getContext("2d");

    gradientStroke = ctx.createLinearGradient(500, 0, 100, 0);
    gradientStroke.addColorStop(0, '#80b6f4');
    gradientStroke.addColorStop(1, chartColor);

    gradientFill = ctx.createLinearGradient(0, 170, 0, 50);
    gradientFill.addColorStop(0, "rgba(128, 182, 244, 0)");
    gradientFill.addColorStop(1, "rgba(249, 99, 59, 0.40)");

    myChart = new Chart(ctx, {
    type: "bar",
    data: {
        labels: ["นายA", "นายB", "นายC", "นายD", "นายF", "นายG", "นายH", "นายK", "นาย1", "นาย1"],
        datasets: [{
        label: "Active Users",
        borderColor: "#6bd098",
        pointRadius: 0,
        pointHoverRadius: 0,
        fill: false,
        borderWidth: 3,
        data: dataG
        }]
    },
    options: {

        legend: {

        display: false
        },

        tooltips: {
        enabled: false
        },

        scales: {
        yAxes: [{

            ticks: {
            fontColor: "#9f9f9f",
            beginAtZero: false,
            maxTicksLimit: 10,
            //padding: 20
            },
            gridLines: {
            drawBorder: false,
            zeroLineColor: "transparent",
            color: 'rgba(255,255,255,0.05)'
            }

        }],

        xAxes: [{
            barPercentage: 1.6,
            gridLines: {
            drawBorder: false,
            color: 'rgba(255,255,255,0.1)',
            zeroLineColor: "transparent",
            display: false,
            },
            ticks: {
            padding: 20,
            fontColor: "#9f9f9f"
            }
        }]
        },
    }
    });



});
</script>
@endpush
