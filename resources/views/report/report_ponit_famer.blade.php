@extends('layouts.dashboard')
@section('title','หน้าหลัก')
@section('content')


<div class="row">
  <div class="col-md-12">
    <div class="card">
        <div class="card-header">
         <h3 class="text-center">ผลการจัดลำดับความสำคัญของการคัดเลือกเกษตรกรทั้ง 15 คน เพื่อร่วมทำพันธสัญญา</h3>

            <div class="card-body">
              <table id="add" class="table text-center">
                <thead class="thead-dark">
                  <tr>
                    <th scope="col">อันดับ</th>
                    <th scope="col">ชื่อเกษตรกร</th>
                    <th scope="col">ค่าคะแนนผลิตภัณฑ์</th>
                    <th scope="col">ค่าคะแนนกระบวนการผลิต</th>
                    <th scope="col">ค่าคะแนนการขนส่ง</th>
                    <th scope="col">ค่าคะแนนการให้ความร่วมมือ</th>

                    <th scope="col">ค่าคะแนนรวม</th>
                    <th scope="col">ดูรายระเอียด</th>
                  <!-- <th scope="col">Handle</th>-->
                  </tr>
                </thead>
                <tbody>
                  @foreach ($scorce as $key=>$item)
                  <tr>
                    <th scope="row">{{$key+1}}</th>
                    <td>{{$item->famer->fname .' '.$item->famer->lname}}</td>
                    @foreach($item->score_famer_main as $keyx => $itemx)
                      <td>{{$itemx->topic_score}}</td>
                      @if($keyx >= 3)
                        @break
                      @endif
                    @endforeach
                    <th>{{$item->criteria_score}}</th>
                    <td>
                    <a href="{{ route('detailfarmerscorereport',$item->id) }}" type="button" class="btn btn-info"><i class="fas fa-info-circle"></i></a>
                    </td>
                  </tr>
                  @endforeach
                </tbody>


              </table>
              <a class="btnprn btn " target="_bank" href="{{route('report-total',$item->id)}}">Print Preview</a>
              </div>
            </div>
        <!-- end content-->
    </div>
    <!--  end card  -->
  </div>
</div>



@endsection

@push('scripts')
<script>
  $(document).ready( function () {
      $('#add').DataTable();
  } );
</script>
@endpush
