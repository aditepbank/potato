@extends('layouts.dashboard')
@section('title','หน้าหลัก')
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card">
            <div class="card-body">
              <h3 class="text-center">คัดกรองเกษตรกร</h3>
              <p>คำอธิบาย : </p>
               <p> กรุณาเลือกหัวข้อ โดยสามารถเลือกได้ 1 ข้อ เพื่อค้นหาเกษตรกรที่มีลักษณะตามที่ต้องการ</p>
               
              <form method="get" action="{{ route('select-famer') }}">
                         {{csrf_field()}}
              <hr>
              <button type="submit"class="btn btn-success">ค้นหา</button>
             
                <hr>
              <div class="row">
              
              <div class="col">
              
                <div class="form">
                  <label>ผลิตภัณฑ์</label>
                  <div class="input-group mb-3">
                    <div class="input-group-prepend">
                      <div class="input-group-text">
                        <input type="checkbox" class="css_data_item" aria-label="Checkbox for following text input" name="1" value="">ความสมบูรณ์ของหัวมันสำปะหลัง
                      </div>
                    </div>
                    <input type="text" class="form-control col" aria-label="Text input with checkbox">
                  </div>
                  
                  <div class="input-group mb-3">
                    <div class="input-group-prepend">
                      <div class="input-group-text">
                        <input type="checkbox" class="css_data_item" aria-label="Checkbox for following text input" name="2"> เปอร์เซ็นต์แป้งในมันสำปะหลัง
                      </div>
                    </div>
                    <input type="text" class="form-control col" aria-label="Text input with checkbox">
                  </div>
                  
                  <div class="input-group mb-3">
                    <div class="input-group-prepend">
                      <div class="input-group-text">
                        <input type="checkbox" class="css_data_item" aria-label="Checkbox for following text input" name="3"> คุณภาพการเพาะปลูก
                      </div>
                    </div>
                    <input type="label" class="form-control col" aria-label="Text input with checkbox">
                  </div>
                  
                  <!-- <div class="input-group mb-3">
                    <div class="input-group-prepend">
                      <div class="input-group-text">
                        <input type="checkbox" class="css_data_item" aria-label="Checkbox for following text input" name="3">
                      </div>
                    </div>
                    <input type="text" class="form-control col" aria-label="Text input with checkbox">
                  </div>  -->
                </div>
              
              
              </div>
              <div class="col">
              <div class="form">
                <label>ผลผลิต</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <div class="input-group-text">
                      <input type="checkbox" class="css_data_item" aria-label="Checkbox for following text input" name="4">มีจำนวนไร่เพาะปลูกเพียงพอ
                    </div>
                  </div>
                  <input type="label" class="form-control col" aria-label="Text input with checkbox">
                </div>
                
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <div class="input-group-text">
                      <input type="checkbox" class="css_data_item" aria-label="Checkbox for following text input" name="9">ความซื่อสัตย์ต่อการซื้อขาย
                    </div>
                  </div>
                  <input type="text" class="form-control col" aria-label="Text input with checkbox"> 
                </div>
                
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <div class="input-group-text">
                      <input type="checkbox" class="css_data_item" aria-label="Checkbox for following text input" name="10">ความมั่นคงในการซื้อขายของเกษตรกร
                    </div>
                  </div>
                  <input type="label" class="form-control col" aria-label="Text input with checkbox">
                </div>
                
                <!-- <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <div class="input-group-text">
                      <input type="checkbox" class="css_data_item" aria-label="Checkbox for following text input" name="3">
                    </div>
                  </div>
                  <input type="text" class="form-control col" aria-label="Text input with checkbox"> 
                </div> 
              </div> -->
              
              </div>
              </div>

              


              <div class="col">
              <div class="form">
              <label>ขนส่ง</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <div class="input-group-text">
                      <input type="checkbox" class="css_data_item" aria-label="Checkbox for following text input" name="5"> ศักยภาพในการขนส่ง
                    </div>
                  </div>
                  <input type="label" class="form-control col" aria-label="Text input with checkbox">
                </div>
                
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <div class="input-group-text">
                      <input type="checkbox" class="css_data_item" aria-label="Checkbox for following text input" name="11">ส่งมอบได้ตามเวลา
                    </div>
                  </div>
                  <input type="text" class="form-control col" aria-label="Text input with checkbox">
                </div>
               
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <div class="input-group-text">
                      <input type="checkbox" class="css_data_item" aria-label="Checkbox for following text input" name="12">ส่งมอบได้ตามปริมาณ
                    </div>
                  </div>
                  <input type="label" class="form-control col" aria-label="Text input with checkbox">
                </div>
               
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <div class="input-group-text">
                      <input type="checkbox" class="css_data_item" aria-label="Checkbox for following text input" name="13">ระยะห่างจากแปลงปลูกมายังโรงงาน
                    </div>
                  </div>
                  <input type="text" class="form-control col" aria-label="Text input with checkbox">
                </div> 
              </div>
              
              </div>
              <div class="col">
                <div class="form">
                <label>การให้ความร่วมมือ</label>
                  <div class="input-group mb-3">
                    <div class="input-group-prepend">
                      <div class="input-group-text">
                        <input type="checkbox" class="css_data_item" aria-label="Checkbox for following text input" name="6"> ความสัมพันธ์กับโรงงาน
                      </div>
                    </div>
                    <input type="label" class="form-control col" aria-label="Text input with checkbox">
                  </div>
                  
                  <div class="input-group mb-3">
                    <div class="input-group-prepend">
                      <div class="input-group-text">
                        <input type="checkbox" class="css_data_item" aria-label="Checkbox for following text input" name="14"> ทัศนคติต่อการพัฒนาตนเอง 
                      </div>
                    </div>
                    <input type="text" class="form-control col" aria-label="Text input with checkbox">
                  </div>
                  
                  <div class="input-group mb-3">
                    <div class="input-group-prepend">
                      <div class="input-group-text">
                        <input type="checkbox" class="css_data_item" aria-label="Checkbox for following text input" name="15"> ทัศนคติที่มีต่อโรงงาน
                      </div>
                    </div>
                    <input type="label" class="form-control col" aria-label="Text input with checkbox">
                  </div>
                  
                  <div class="input-group mb-3">
                    <div class="input-group-prepend">
                      <div class="input-group-text">
                        <input type="checkbox" class="css_data_item" aria-label="Checkbox for following text input" name="16"> ทัศนคติที่เกษตรกรมีต่อการทำพันธสัญญา
                      </div>
                    </div>
                    <input type="text" class="form-control col" aria-label="Text input with checkbox">
                  </div> 
                  
              </div>
              
              </div>

              </div>
              <!-- <button type="submit"class="btn btn primary text-center">ค้นหา</button> -->
              <br>
              <hr>
                
                  @foreach($Criteria_dt as $item)
                  <h4 class="text-center">ท่านคัดเลือกเกษตรกรในหัวข้อ : {{$item->criteria_detail_name}}</h4>
                  <h5 class="text-center"> รายละเอียด : {{$item->criteria_detail_high}}</h5>
                  @endforeach
                  </div> 
              <hr>
              <br>
              </form>
              <table class="table">
                <thead class="thead-dark">
                  <tr>
                    <th scope="col">ลำดับ</th>
                    <th scope="col">รหัสเกษตกร</th>
                    <th scope="col">ชื่อ- สกุล</th>
                    <th scope="col">เบอร์ติดต่อ</th>
                    
                  </tr>
                </thead>
                <tbody>
                @foreach($score_detail as $key=>$item)
                  <tr>
                    <th scope="row">{{$key+1}}</th>
                    <td>{{$item->famer->famer_code}}</td>
                    <td>{{$item->famer->fname.' '.$item->famer->lname}}</td>
                    <td>{{$item->famer->phone_number}}</td>
                    
                  </tr>
                @endforeach
                </tbody>
                
              </table>



              </div>
            
              
            </div>
            
        <!-- end content-->
    </div>
    <!--  end card  -->
  </div>
</div>

@endsection

@push('scripts')
<script>

  $(document).ready( function () {
    $('#add').DataTable();

    $("#btnshow").click(function(){
        $("#advanced").toggle();;
    });

  } );

</script>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>    
<script type="text/javascript">  
$(function(){        
       
    $(".css_data_item").click(function(){  // เมื่อคลิก checkbox  ใดๆ  
        if($(this).prop("checked")==true){ // ตรวจสอบ property  การ ของ   
            var indexObj=$(this).index(".css_data_item"); //   
            $(".css_data_item").not(":eq("+indexObj+")").prop( "checked", false ); // ยกเลิกการคลิก รายการอื่น  
        }  
    });  
 
    $("#form_checkbox1").submit(function(){ // เมื่อมีการส่งข้อมูลฟอร์ม  
        if($(".css_data_item:checked").length==0){ // ถ้าไม่มีการเลือก checkbox ใดๆ เลย  
            alert("NO");  
            return false;     
        }  
    });     
           
});  
</script>

@endpush
