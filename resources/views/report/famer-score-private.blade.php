@extends('layouts.master')
@section('title','หน้าหลัก')
@section('content')
<nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
  <div class="container-fluid">
    <div class="navbar-wrapper">
      <div class="navbar-minimize">
        <button id="minimizeSidebar" class="btn btn-icon btn-round">
          <i class="nc-icon nc-minimal-right text-center visible-on-sidebar-mini"></i>
          <i class="nc-icon nc-minimal-left text-center visible-on-sidebar-regular"></i>
        </button>
      </div>
      <div class="navbar-toggle">
        <button type="button" class="navbar-toggler">
          <span class="navbar-toggler-bar bar1"></span>
          <span class="navbar-toggler-bar bar2"></span>
          <span class="navbar-toggler-bar bar3"></span>
        </button>
      </div>
      <a class="navbar-brand" href="#pablo">สรุปผลการประเมินเกษตรกร</a>
    </div>
  </div>
</nav>



<div class="row">
  <div class="col-md-12">
    <div class="card">
    <div class="card-body">
      <div class="row">
        <div class="col">
          
        <a class="btn btn-primary fa fa-print" target="_bank" href="{{route('report-score',[$id,$form])}}?key={{ request()->key }}"> พิมพ์</a>
        <a class="btn btn-danger"href="{{route('reportpointfarmer',$form)}}"> < ย้อนกลับ</a>
        <div ></div>
        </div>
        <div class="col align-self-center">
        <h2 class="text-center">ผลการประเมิน</h2>
        <h4 class="text-center">รอบ : {{ $formname->criteria_season_detail }}</h4>
      </div>
      <div class="col align-self-center">
        <div class="card card-stats  float-right">
            <div class="card-body">
              <h5 class="text-center" > ลำดับที่ได้ <br>  </h5>
              <h2 class="text-center" >  {{request()->key}} </h2>
            </div>
        </div>
      </div>
    </div>
    </div>

        <div class="card-header">

          <!-- <a href="" type="button" class="btn btn-success" >เลือกฤดูกาล</div> -->
            <div class="card-body">
            <!-- <i class="fas fa-print"></i> -->

            ชื่อเกษตรกร : <b>  {{ $score_famer->famer->name_prefix.''.$score_famer->famer->fname.' '.$score_famer->famer->lname}}</b> <br>
            ที่อยู่ :<b> {{ $score_famer->famer->adderss}}</b>  <br> 
            โทรศัพท์ : <b>{{ $score_famer->famer->phone_number}} </b> 
            
            <hr>


            <form id="formq" name="formq" method="POST" action="">
            <table width="100%" border="1" align="center" cellpadding="0" cellspacing="0">
              <tr>
                <td width="15%" rowspan="2" align="center"><strong>หลักเกณฑ์</strong></td>

              </tr>
              <tr>
                <td width="15%" align="center"><strong>ค่าน้ำหนัก</strong></td>
                <td width="15%" align="center"><strong>ค่าคะแนนจากเจ้าหน้าที่</strong></td>
                <td width="15%" align="center"><strong>ค่าคะแนนรวม</strong></td>



              </tr>
              @foreach ($score_famer_main as $key=> $item)
                <tr>
                  <td height="30" colspan="3" bgcolor="#F4F4F4"><strong>&ensp;{{ $key + 1 }}. {{$item->Criteria_main->criteria_main_name}}</strong></td>
                  <td height="30" align="center" colspan="1" bgcolor="#F4F4F4"><b>{{$item->topic_score}}</b> </td>
                </tr>
                @foreach ($item->score_famer_main_detail as $keyx=> $itemx)

                <tr>
                  <td height="30">&ensp;&ensp; {{ $key + 1 }}.{{ $keyx + 1 }} {{$itemx->get_Criteria_detail->criteria_detail_name}}</td>
                  <td height="30" align="center">{{ $itemx->get_Criteria_detail->criteria_detail_Weight }}</td>
                  <td height="30" align="center">{{ $itemx->answer }}</td>
                  <td height="30" align="center">{{ $itemx->topic_score_detail }}</td>

                </tr>



                @endforeach
                @endforeach
                <td bgcolor="#F4F4F4"></td>
                <td bgcolor="#F4F4F4"></td>
                <td bgcolor="#F4F4F4"></td>
                <td height="30" align="center" colspan="5" bgcolor="#F4F4F4"><strong>&ensp;รวม : {{$score_famer->criteria_score}}</strong></td>

            </table>
            <hr>
            <br>
            <!-- <a href="#" type="submit" name="save" class="btn btn-primary">ย้อนกลับ</a> -->
            <!-- <a class="btnprn btn " target="_bank" href="">Print Preview</a> -->






          </form>

            <script type="text/javascript">
            $(document).ready(function(){
            $('.btnprn').printPage();
            });
            </script>
            </div>
          </div>
            </div>
        <!-- end content-->
    </div>
    <!--  end card  -->
  
</div>


@endsection
