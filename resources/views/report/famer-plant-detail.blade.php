<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>ข้อมูลการปลูกมัน</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
  <br>
  <div class="container">
    <h3>ข้อมูลการปลูกมัน ของ : {{$score_famer->famer->fname.' '.$score_famer->famer->lname}}</h3>
    <hr>
    <form>
      <div class="form-row">
        <div class="form-group col-md-3">
          <label for="inputEmail4">รหัสเกษตรกร</label>
          <p>{{$score_famer->famer->famer_code}}</p>
        </div>
        <div class="form-group col-md-3">
          <label for="inputPassword4">ชื่อเกษตรกร</label>
          <p>{{$score_famer->famer->fname.' '.$score_famer->famer->lname}}</p>
        </div>
        <div class="form-group col-md-3">
          <label for="inputAddress">เบอร์โทร</label>
          <p>{{$score_famer->famer->phone_number}}</p>
        </div>
      </div>
      <div class="form-row">
        <div class="form-group col-md-3">
          <label for="inputEmail4">วันเดือนปีเกิด</label>
          <p>{{$score_famer->famer->birthday}}</p>
        </div>
        <div class="form-group col-md-3">
          <label for="inputPassword4">สถานะภาพ :</label>
          <p>{{$score_famer->famer->status}}</p>
        </div>
        <div class="form-group col-md-3">
          <label for="inputAddress">ระดับการศึกษา :</label>
          <p>{{$score_famer->famer->education}}</p>
        </div>
      </div>
      <div class="form-row">
        <div class="form-group col-md-6">
          <label for="inputAddress">ที่อยู่</label>
          <p>{{$score_famer->famer->adderss}}</p>
        </div>
      </div>
      <hr>
      <br>
      <h5>ข้อมูลไร่มัน</h5>
      <br>
      <div class="form-row">

        <div class="form-group col-md-4">
          <label for="inputAddress">ข้อมูลพันธุ์ปลูกมันสำปะหลัง :
            {{$score_famer->planting_information->breed_type}}</label>
        </div>
        <div class="form-group col-md-4">
          <label for="inputAddress">วันที่เริ่มปลูก :
            {{$score_famer->planting_information->created_at}}</label>
        </div>
        <div class="form-group col-md-4">
          <label for="inputAddress">ระยะการปลูก/เมตร :
            {{$score_famer->planting_information->Planting_distance}}</label>
        </div>
        <div class="form-group col-md-4">
          <label for="inputAddress2">ลักษณะการปลูก : {{$score_famer->planting_information->Planting_characteristics}}</label>
        </div>
      </div>
      <div class="form-row">
        <div class="form-group col-md-4">
          <label for="inputAddress2">ระยะห่างโรงงาน : {{$score_famer->garden_detail->distance}}</label>
        </div>
        <div class="form-group col-md-4">
          <label for="inputCity">ละติจูด : {{$score_famer->garden_detail->latitude}} </label>
        </div>
        <div class="form-group col-md-4">
          <label for="inputCity">ลองติจูด : {{$score_famer->garden_detail->longtitude}} </label>
        </div>
      </div>
      <div class="form-row">
        <div class="form-group col-md-4">
          <label for="inputAddress2">จำนวณไร่ : {{$score_famer->garden_detail->total_garden}}</label>
        </div>
        <div class="form-group col-md-4">
          <label for="inputCity">ลักษณะดิน : {{$score_famer->garden_detail->soli_look}} </label>
        </div>
        <div class="form-group col-md-4">
          <label for="inputCity">ค่า HP : {{$score_famer->garden_detail->HP}} </label>
        </div>
      </div>
      <div class="form-row">
        <div class="form-group col-md-4">
          <label for="inputAddress2">สิ่งแวดล้อม : {{$score_famer->garden_detail->enviroment}}</label>
        </div>
        <!-- <div class="form-group col-md-4">
            <label for="inputCity">ลักษณะดิน : {{$score_famer->harvest_information->high_meter}} </label>
        </div>
        <div class="form-group col-md-4">
            <label for="inputCity">ค่า HP : {{$score_famer->harvest_information->calculate_potato}} </label>
        </div> -->
      </div>
      <hr>
      <h5>ข้อมูลจัดการแปลงปลูกมันสำปะหลัง</h5>
      <br>
      <div class="form-row">
        <div class="form-group col-md-4">
          <label for="inputAddress2">ไถดะ/ครั้ง : {{$score_famer->plantation_management_information->plow_da}}</label>
        </div>
        <div class="form-group col-md-4">
          <label for="inputCity">ไถแปล/ครั้ง : {{$score_famer->plantation_management_information->plow_pea}} </label>
        </div>
        <div class="form-group col-md-4">
          <label for="inputCity">ปุ๋ยรองพื้น : {{$score_famer->plantation_management_information->Foundation_fertilizer}} </label>
        </div>
      </div>
      <div class="form-row">
        <div class="form-group col-md-4">
          <label for="inputAddress2">ซักร่อง : {{$score_famer->plantation_management_information->Groove}}</label>
        </div>
        <div class="form-group col-md-4">
          <label for="inputCity">ประมาณ/ไร่ : {{$score_famer->plantation_management_information->about_farm}} </label>
        </div>
        <div class="form-group col-md-4">
          <label for="inputCity">การให้น้ำ : {{$score_famer->plantation_management_information->give_water}} </label>
        </div>
      </div>
      <div class="form-row">
        <div class="form-group col-md-4">
          <label for="inputAddress2">การให้น้ำ/ครั้ง : {{$score_famer->plantation_management_information->give_water_calculate}}</label>
        </div>
        <div class="form-group col-md-4">
          <label for="inputCity">ปุ๋ยแต่งหน้า : {{$score_famer->plantation_management_information->Make_up_fertilizer}} </label>
        </div>
        <div class="form-group col-md-4">
          <label for="inputCity">ใส่ปุ๋ยแต่งหน้า/ประมาณไร่ : {{$score_famer->plantation_management_information->Make_up_fertilizer_about}} </label>
        </div>
      </div>
      <div class="form-row">
        <div class="form-group col-md-4">
          <label for="inputAddress2">กำจัดวัชพืช : {{$score_famer->plantation_management_information->Weeding}}</label>
        </div>
        <div class="form-group col-md-4">
          <label for="inputCity">ประมาณคนที่ใส่ปุ๋ย/ไร่ : {{$score_famer->plantation_management_information->Weeding_about_people}} </label>
        </div>
        <div class="form-group col-md-4">
          <label for="inputCity">จำนวนครั้งที่ใส่ปุ๋ย : {{$score_famer->plantation_management_information->Weeding_about_people_cal}} </label>
        </div>
      </div>
      <div class="form-row">
        <div class="form-group col-md-4">
          <label for="inputAddress2">คาดการณ์เก็บเกี่ยวผลผลิต/เดือน : {{$score_famer->plantation_management_information->Month_harvest}}</label>
        </div>
        <!-- <div class="form-group col-md-4">
            <label for="inputCity">ปุ๋ยแต่งหน้า : {{$score_famer->planting_information->Planting_distance}} </label>
        </div>
        <div class="form-group col-md-4">
            <label for="inputCity">ใส่ปุ๋ยแต่งหน้า/ประมาณไร่ : {{$score_famer->planting_information->date_plant}} </label>
        </div> -->
      </div>

      <hr>
      <br>
      <h5>ข้อมูลการเก็บเกี่ยว</h5>
      <br>
      <div class="form-row">
        <div class="form-group col-md-4">
          <label for="inputAddress2">อายุมันสำปะหลัง/เดือน : {{$score_famer->harvest_information->age_potato}}</label>
        </div>
        <div class="form-group col-md-4">
          <label for="inputCity">ความสูง/ซม. : {{$score_famer->harvest_information->high_meter}} </label>
        </div>
        <div class="form-group col-md-4">
          <label for="inputCity">จำนวนมันสำปะหลัง หัว/ต้น : {{$score_famer->harvest_information->calculate_potato}} </label>
        </div>
      </div>
      <div class="form-row">
        <div class="form-group col-md-4">
          <label for="inputAddress2">น้ำหนัก กก./ต้น : {{$score_famer->harvest_information->weight_potato}}</label>
        </div>
        <div class="form-group col-md-4">
          <label for="inputCity">ประเมินผลผลิต : {{$score_famer->harvest_information->Evaluate_products}} </label>
        </div>
        <div class="form-group col-md-4">
          <label for="inputCity">แนวโน้มผลผลิต : {{$score_famer->harvest_information->Trend_productivity}} </label>
        </div>
      </div>
      <div class="form-row">
        <div class="form-group col-md-4">
          <label for="inputAddress2">ชื่อผู้ให้ข้อมูล : {{$score_famer->harvest_information->Provider_Name}}</label>
        </div>
        <div class="form-group col-md-4">
          <label for="inputCity">ชื่อผู้เก้บข้อมูล : {{$score_famer->harvest_information->Data_collector_name}} </label>
        </div>
        <!-- <div class="form-group col-md-4">
            <label for="inputCity">แนวโน้มผลผลิต : {{$score_famer->planting_information->date_plant}} </label>
        </div> -->
      </div>
      <hr>
      <br>
      <a href="{{route('famer-plant-info-total')}}" type="button" class="btn btn-success">ย้อนกลับ</a>
      <br>
      <hr>






      <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>

</html>