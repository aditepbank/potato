@extends('layouts.dashboard')
@section('title','หน้าหลัก')
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card">
            <div class="card-body">
            <h3 class="text-center">สรุปผลการประเมินเกษตร</h3>
            <h5> เลือกผลสรุปจากคะแนนการประเมิน </h5><br>
            <a class="btn btn-primary" id="btnshow" type="button"><i class="nc-icon nc-zoom-split"> ตัวเลือกค้นหา </i></a><br>
            <label> ค้นหาเพิ่มเติมคะแนนในแต่ละปัจจัยและเลือกจำนวนผลลัพธ์ </label>
            <div id="advanced" style="display:none">
                <div class="card">
                    <div class="card-body">
                        <form action="{{ route('criterriasearch',$cri_id) }}" method="get" role="search">
                            <div class="row">
                                <div class="col">
                                    <div class="form-group col">
                                        <label for="inputState">ปัจจัยที่ต้องการค้นหา</label>
                                        <select id="inputState" name="select" class="form-control">
                                            <option value='0'>-- กรุณาเลือกปัจจัย --</option>
                                            @foreach($criteriaMain as $value)
                                            <option value= "{{ $value->id }}">{{ $value->criteria_main_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col">
                                    <label> จำนวนข้อมูลที่ต้องการแสดง </label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="nc-icon nc-paper"></i></span>
                                        </div>
                                        <input type="number" name="quantity" class="form-control" min="0" placeholder="จำนวนข้อมูลที่ต้องการแสดง">
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-success">ค้นหา</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="card">
            <div class="card-body">
            <h4 class="text">ผลการจัดลำดับความสำคัญของการคัดเลือกเกษตรกร</h4>
              <table id="add" class="table">
                <thead class="thead-dark">
                  <tr>
                    <th scope="col">อันดับ</th>
                    <th scope="col">ชื่อเกษตรกร</th>
                    <th scope="col">ค่าคะแนนผลิตภัณฑ์</th>
                    <th scope="col">ค่าคะแนนกระบวนการผลิต</th>
                    <th scope="col">ค่าคะแนนการขนส่ง</th>
                    <th scope="col">ค่าคะแนนการให้ความร่วมมือ</th>
                    <th scope="col" >ค่าคะแนนรวม</th>
                    <th scope="col">ดูรายระเอียด</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($scorce as $key=>$item)
                  <tr>
                    <td scope="row">{{$key+1}}</td>
                    <td>{{$item->famer->fname .' '.$item->famer->lname}}</td>
                    @foreach($item->score_famer_main as $keyx => $itemx)
                      <td>{{$itemx->topic_score}}</td>
                      @if($keyx >= 3)
                        @break
                      @endif
                    @endforeach
                    <td>{{$item->criteria_score}}</td>
                    <td>
                    <a href="{{ route('detailfarmerscorereport',$item->id) }}" type="button" class="btn btn-primary btn-round"><i class="fas fa-info-circle"></i></a>
                    </td>
                </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            </div>
              <a class="btn btn-primary" target="_bank" href="#"><i class="nc-icon nc-paper"> </i>Print Preview</a>
              <a class="btn btn-default" target="_bank" href="{{route('famer-plant-info-total')}}">ข้อมูลการเพาะปลูก/ผลผลิต</a>
              </div>
        <!-- end content-->
    </div>
    <!--  end card  -->
  </div>
</div>



@endsection

@push('scripts')
<script>

  $(document).ready( function () {
    $('#add').DataTable();

    $("#btnshow").click(function(){
        $("#advanced").toggle();;
    });

  } );

</script>

@endpush
