@extends('layouts.master')
@section('title','หน้าหลัก')
@section('content')
<nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
  <div class="container-fluid">
    <div class="navbar-wrapper">
      <div class="navbar-minimize">
        <button id="minimizeSidebar" class="btn btn-icon btn-round">
          <i class="nc-icon nc-minimal-right text-center visible-on-sidebar-mini"></i>
          <i class="nc-icon nc-minimal-left text-center visible-on-sidebar-regular"></i>
        </button>
      </div>
      <div class="navbar-toggle">
        <button type="button" class="navbar-toggler">
          <span class="navbar-toggler-bar bar1"></span>
          <span class="navbar-toggler-bar bar2"></span>
          <span class="navbar-toggler-bar bar3"></span>
        </button>
      </div>
      <a class="navbar-brand" href="#pablo">สรุปผลการประเมินคุณสมบัติของเกษตรกร</a>
      
    </div>
  </div>
</nav>
<div class="row">
  <div class="col-md-12">
    <div class="card">
            <div class="card-body">
              <div class="row">
                  <div class="col">
                  <a class="btn btn-primary fa fa-print"  target="_bank" href="{{route('score-famer-print',$cri_id)}}"> พิมพ์</a>
                   <a class="btn btn-danger"href="{{route('chooseseasonreport')}}"> < ย้อนกลับ</a>

                  </div>
                  <div class="col align-self-center">
                  <h2 class="text-center">สรุปผลการประเมินเกษตรกรทั้งหมด</h2>
                  <h4 class="text-center">รอบ : {{ $Criteria->criteria_season_detail }}</h4>
                </div>
                <div class="col align-self-center">
                  
                </div>
              </div>



            <div class="card">
            <div class="card-body">
           
           <h5>คำอธิบาย</h5>
            <h6 class="text">อันดับเรียงตามคะแนนรวมจากมากไปหาน้อย</h6>
           
              <table id="add" class="table text-center">
                <thead class="thead-dark text-center">
                  <tr>
                    <th scope="col">อันดับที่ได้</th>
                    <th scope="col">ชื่อเกษตรกร</th>
                    <th scope="col">ที่อยู่</th>
                    <th scope="col">โทรศัพท์</th>
                    <th scope="col">คะแนนรวม</th>
                    <th scope="col">ดูรายละเอียด</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($scorce as $key=>$item)
                  <tr>
                    <td scope="row">{{$key+1}}</td>
                    <td>{{$item->famer->name_prefix .''.$item->famer->fname .' '.$item->famer->lname}}</td>
                    <td>{{$item->famer->adderss}}</td>
                    <td>{{$item->famer->phone_number}}</td>
                    <td>{{$item->criteria_score}}</td>
                    <td>
                    <a href="{{ route('detailfarmerscorereport',['id' => $item->id,'form' => $cri_id,'key' => $key+1]) }}" type="button" class="btn btn-primary btn-round"><i class="fas fa-info-circle"></i></a>
                    </td>
                </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            </div>
              <!-- <a class="btn btn-pinterest" target="_bank" href="#"><i class="fa fa-pinterest"></i> Print Preview</a> -->
              <!-- <a class="btn btn-default" target="_bank" href="{{route('famer-plant-info-total')}}">ข้อมูลการเพาะปลูก/ผลผลิต</a> -->
              </div>
        <!-- end content-->
    </div>
    <!--  end card  -->
  </div>
</div>



@endsection

@push('scripts')
<script>

  $(document).ready( function () {
    $('#add').DataTable();

    $("#btnshow").click(function(){
        $("#advanced").toggle();;
    });

  } );

</script>

@endpush
