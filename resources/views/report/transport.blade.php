@extends('layouts.master')
@section('title','หน้าหลัก')
@section('content')
<nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
  <div class="container-fluid">
    <div class="navbar-wrapper">
      <div class="navbar-minimize">
        <button id="minimizeSidebar" class="btn btn-icon btn-round">
          <i class="nc-icon nc-minimal-right text-center visible-on-sidebar-mini"></i>
          <i class="nc-icon nc-minimal-left text-center visible-on-sidebar-regular"></i>
        </button>
      </div>
      <div class="navbar-toggle">
        <button type="button" class="navbar-toggler">
          <span class="navbar-toggler-bar bar1"></span>
          <span class="navbar-toggler-bar bar2"></span>
          <span class="navbar-toggler-bar bar3"></span>
        </button>
      </div>
      <a class="navbar-brand" href="#pablo">เกษตรกรที่มีคุณสมบัติที่ด้อยที่สุดในแต่ละด้าน</a>
    </div>
  </div>
</nav>


<div class="row">
  <div class="col-md-12">
    <div class="card">
        <div class="card-header">
        <div class="col-lg-5 col-md-12 col-sm-6">
                <div class="card card-stats">
                  <div class="card-body ">
                  
                  <h5 class="text-left fas fa-caret-down text-danger " > เกษตรกรที่มีคุณสมบัติที่ด้อยที่สุดในแต่ละด้าน </h5>
                  
                    <div class="row">

                    </div>
                  </div>
                </div>
            </div>
            <div class="card-body">
        <a class="btn btn-danger"href="{{route('chooseseasonmin')}}"> < ย้อนกลับ</a>

            <div class="dropdown">
            <!-- <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                เลือกฤดูกาล
            </button> -->
            <hr>

            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">

                <button class="dropdown-item" value= ""></button>

            </div>
            </div>
            <div class="content">
        <div class="row">
      @foreach($array_result as $key => $item)
          <div class="col-6">
          <div class="card">
          <div class="card-header">
                <h5 class="card-title text-danger"> <i class="nc-icon nc-box-2 text-danger"></i> <b> ด้าน{{ $item['criteria_main_name'] }}</b></h5>
              </div>
              <div class="card-body">
                    <table id="" class="table text-center">
                        <thead class="text-danger">
                            <th>
                                ลำดับที่ได้
                            </th>
                            <th>
                                ชื่อเกษตรกร
                            </th>
                            <!-- <th>
                                คะแนนรวม
                            </th> -->
                            <th>
                                โทรศัพท์
                            </th>
                            <!-- <th>
                                ดูเพิ่มเติม
                            </th> -->
                        </thead>
                        <tbody>
                        @foreach ($item['results'] as $keyx => $itemx)
                            <tr>
                                <th scope="row">{{$keyx+1}}</th>
                                <td>{{$itemx->name_prefix .''.$itemx->fname .' '.$itemx->lname}}</td>
                                <!-- <td>{{$itemx->topic_score}}</td> -->
                                <td>{{$itemx->phone_number}}</td>

                            </tr>
                          @endforeach
                        </tbody>
                    </table>
                    <a  style="float:right" href="{{route('report/transport1', [$form_id, $item['id']])}}">รายละเอียด</a>
                    <!-- <a href="#">พิมพ์รายงาน</a> -->
              </div>
          </div>
          </div>
      @if($key % 2 == 1)
        </div> <!-- end row result --> <div class="row">
      @endif

    @endforeach
    </div>
    </div>
    </div>
        </div>
        <!-- end content-->
    </div>
    <!--  end card  -->
  </div>
</div>

@endsection
@push('scripts')
<script>

  $(document).ready( function () {
    $('#add').DataTable();

    $("#btnshow").click(function(){
        $("#advanced").toggle();;
    });

  } );

</script>
<script>

$(document).ready( function () {
  $('#add2').DataTable();

  $("#btnshow").click(function(){
      $("#advanced").toggle();;
  });

} );

</script>
@endpush
