@extends('layouts.master')
@section('title','หน้าหลัก')
@section('content')
<nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
  <div class="container-fluid">
    <div class="navbar-wrapper">
      <div class="navbar-minimize">
        <button id="minimizeSidebar" class="btn btn-icon btn-round">
          <i class="nc-icon nc-minimal-right text-center visible-on-sidebar-mini"></i>
          <i class="nc-icon nc-minimal-left text-center visible-on-sidebar-regular"></i>
        </button>
      </div>
      <div class="navbar-toggle">
        <button type="button" class="navbar-toggler">
          <span class="navbar-toggler-bar bar1"></span>
          <span class="navbar-toggler-bar bar2"></span>
          <span class="navbar-toggler-bar bar3"></span>
        </button>
      </div>
      <a class="navbar-brand" href="#pablo">10 ลำดับเกษตรกรที่ได้ผลประเมินสูงสุดในแต่ละด้าน</a>
    </div>
  </div>
</nav>


<div class="row">
  <div class="col-md-12">
    <div class="card">
        <div class="card-header">
          <!-- <h3 class="text-center">ดูคะแนนเกษตที่ได้สูงสุดในแต่ละด้าน</h3> -->
          <h5>&nbsp;&nbsp;คำอธิบาย&nbsp;: &nbsp;เลือกดูรายงานตามรอบประเมินที่ต้องการ</h5>

        <!-- {{-- <a a href="" type="button" class="btn btn-success" style="margin-left: 1400px;">เพิ่มฤดูกาลเพาะปลูก</a> --}} -->
        </div>
        <div class="card-body">
            <table id="add" class="table text-center">
              <thead class="thead-dark">
                <tr>
                  <th scope="col">ลำดับ</th>
                  <th scope="col">รอบประเมิน</th>
                  <th scope="col">ช่วงระยะเวลา</th>
                  <th scope="col">ดูรายละเอียด</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($Report as $key=> $item)
                <tr>
                  <td>{{ $key+1 }}</td>
                  <td>{{ $item->criteria['criteria_season_name'] }}</td>
                  <td>{{ $item->criteria['criteria_season_detail'] }}</td>
                  <td>
                    <a href=" {{ route('report-criteria-best',$item->form_criteria_id) }} " type="button" class="btn btn-success"><i class="nc-icon nc-zoom-split"></i> คลิก</a>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
        </div>
        <!-- end content-->
    </div>
    <!--  end card  -->
   </div>
</div>



@endsection

@push('scripts')
<script>
  $(document).ready( function () {
      $('#add').DataTable();
  } );
</script>
@endpush

