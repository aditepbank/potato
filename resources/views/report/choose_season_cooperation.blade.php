@extends('layouts.db_low_four')
@section('title','หน้าหลัก')
@section('content')


<div class="row">
  <div class="col-md-12">
    <div class="card">
        <div class="card-header">
          <br>
          {{-- <h3 class="text-center">รายงานผลเกษตรกรที่มีดีด้านผลิตภัณฑ์ ด้านกระบวนการผลิต แต่มีปัญหาด้านการให้ความร่วมมือ</h3> --}}
          <h5>&nbsp;&nbsp;คำอธิบาย&nbsp;: &nbsp;เลือกดูรายงานตามรอบประเมินที่ต้องการ</h5>

        {{-- <a a href="" type="button" class="btn btn-success" style="margin-left: 1400px;">เพิ่มฤดูกาลเพาะปลูก</a> --}}
        </div>
        <div class="card-body">
            <table id="add" class="table text-center">
              <thead class="thead-dark">
                <tr>
                  <th scope="col">ลำดับ</th>
                  <th scope="col">รอบการประเมิน</th>
                  <th scope="col">ช่วงระยะเวลา</th>
                  <th scope="col">ดูรายละเอียด</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($Report as $key=> $item)
                <tr>
                  <td>{{ $key+1 }}</td>
                  <td>{{ $item->criteria['criteria_season_name'] }}</td>
                  <td>{{ $item->criteria['criteria_season_detail'] }}</td>
                  <td>
                    <a href=" {{ route('farmer_cooperation_low',$item->form_criteria_id) }} " type="button" class="btn btn-success"><i class="nc-icon nc-zoom-split"></i> คลิก</a>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
        </div>
        <!-- end content-->
    </div>
    <!--  end card  -->
   </div>
</div>



@endsection

@push('scripts')
<script>
  $(document).ready( function () {
      $('#add').DataTable();
  } );
</script>
@endpush

