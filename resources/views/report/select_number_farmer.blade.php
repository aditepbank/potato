@extends('layouts.dashboard')
@section('title','หน้าหลัก')
@section('content')


<div class="row">
  <div class="col-md-12">
    <div class="card">
        <div class="card-header">
          <h3 class="text-center">กรุณากรอกจำนวนเกษตรกรที่ต้องการในการทำสัญญา</h3>
          <h4 align="center">{{ $Criteria->criteria_season_name }}&ensp;{{ $Criteria->criteria_season_detail }}</h4>
            <div class="card-body">
                <form action="{{ route('introduce',$testid)}}" method="GET">
                    <div class="form-group text-center col-4" style="margin:auto;">
                        <h4 for="selectnumberfarmers" style="font-size:16px">จำนวนเกษตรกรที่ต้องการ</h4>
                        <input min="0" type="number" class="form-control" name="amount" id="selectnumberfarmers" required>
                    </div>
                    <div class="text-center">
                        <button id="test" type="submit" class="btn btn-primary">ค้นหา</button>
                    </div>
                    
                </form>
                
              </div>
            </div>
        <!-- end content-->
    </div>
    <!--  end card  -->
  </div>
</div>



@endsection

@push('scripts')
<script>
  $(document).ready( function () {
      $('#add').DataTable();
  } );
</script>
@endpush
