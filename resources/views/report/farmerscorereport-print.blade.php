<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>รายงานผลการประเมินทั้งหมด</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/bootstrap-extended.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/colors.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/components.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/themes/dark-layout.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/themes/semi-dark-layout.css') }}">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/core/menu/menu-types/vertical-menu.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/core/colors/palette-gradient.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/pages/invoicex.css') }}">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/style.css') }}">
    <!-- END: Custom CSS-->
</head>
<body onload="javascript:window.print()">
<div class="container mt-5 mb-5">
<section class="card invoice-page">
    <div id="invoice-template" class="card-body">
        
    <div class="row">
              
              <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card card-stats">
                  <div class="card-body ">
                    <div class="row">
                      <div class="col-5 col-md-4">
                        <div class="icon-big text-center icon-warning">
                          <i class="nc-icon nc-money-coins text-success"></i>
                        </div>
                      </div>
                      <div class="col-7 col-md-8">
                        <div class="numbers">
                          <p class="card-category">รหัสเกษตรกร</p>
                          <p class="card-title">
                            </p><p>
                        </p></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
  
              <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card card-stats">
                  <div class="card-body ">
                    <div class="row">
                      <div class="col-5 col-md-4">
                        <div class="icon-big text-center icon-warning">
                          <i class="nc-icon nc-money-coins text-success"></i>
                        </div>
                      </div>
                      <div class="col-7 col-md-8">
                        <div class="numbers">
                          <p class="card-category">ชื่อเกษตรกร</p>
                          <p class="card-title"> 
                            </p><p>
                        </p></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card card-stats">
                  <div class="card-body ">
                    <div class="row">
                      <div class="col-5 col-md-4">
                        <div class="icon-big text-center icon-warning">
                          <i class="nc-icon nc-money-coins text-success"></i>
                        </div>
                      </div>
                      <div class="col-7 col-md-8">
                        <div class="numbers">
                          <p class="card-category">ที่อยู่</p>
                          <p class="card-title"> 
                            </p><p>
                        </p></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card card-stats">
                  <div class="card-body ">
                    <div class="row">
                      <div class="col-5 col-md-4">
                        <div class="icon-big text-center icon-warning">
                          <i class="nc-icon nc-money-coins text-success"></i>
                        </div>
                      </div>
                      <div class="col-7 col-md-8">
                        <div class="numbers">
                          <p class="card-category">เบอร์ติดต่อ</p>
                          <p class="card-title"> 
                            </p><p>
                        </p></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            
            </div>
          <div class="toolbar">
          </div>
          <table id="add" class="table">
                <thead class="thead-dark">
                  <tr>
                    <th scope="col">อันดับ</th>
                    <th scope="col">ชื่อเกษตรกร</th>
                    <th scope="col">ค่าคะแนนผลิตภัณฑ์</th>
                    <th scope="col">ค่าคะแนนกระบวนการผลิต</th>
                    <th scope="col">ค่าคะแนนการขนส่ง</th>
                    <th scope="col">ค่าคะแนนการให้ความร่วมมือ</th>
                    
                    <th scope="col">ค่าคะแนนรวม</th>
                    <th scope="col"></th>
                  <!-- <th scope="col">Handle</th>-->
                  </tr>
                </thead>
                <tbody>
                  @foreach ($scorce as $key=>$item)
                  <tr>
                    <th scope="row">{{$key+1}}</th>
                    <td>{{$item->famer->fname .' '.$item->famer->lname}}</td>
                    @foreach($item->score_famer_main as $keyx => $itemx)
                      <td>{{$itemx->topic_score}}</td>
                      @if($keyx >= 3)
                        @break
                      @endif
                    @endforeach
                    <td>{{$item->criteria_score}}</td>
                    <td>
                    
                    </td>
                  </tr>
                  @endforeach
                </tbody>
                
               
              </table>
          

    </div>
    <div class="col-12">
    <!-- <button class="btn btn-primary col-12 hidden-print" onclick="printElem('invoice-template')">ปริ้น</button> -->
    <a class="btn btn-info col-12 hidden-print" href="#">ย้อนกลับ</a>
    <a class="btn btn-primary col-12 hidden-print" href="javascript:window.print()">พิมพ์รายงาน</a>
</div>
</section>
</div>
</body>
</html>