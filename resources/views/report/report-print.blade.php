<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ $score_famer->famer->fname.' '.$score_famer->famer->lname}}</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/bootstrap-extended.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/colors.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/components.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/themes/dark-layout.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/themes/semi-dark-layout.css') }}">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/core/menu/menu-types/vertical-menu.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/core/colors/palette-gradient.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/pages/invoicex.css') }}">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/style.css') }}">
    <!-- END: Custom CSS-->
</head>
<body onload="javascript:window.print()">
<div class="container mt-5 mb-5">
<section class="card invoice-page">
    <div id="invoice-template" class="card-body">
        
    <div class="row">
        
        <h4 class="text-center"> รอบ : {{ $formname->criteria_season_detail }}</h4>
              <!-- <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card card-stats">
                  <div class="card-body ">
                    <div class="row">
                      <div class="col-5 col-md-4">
                        <div class="icon-big text-center icon-warning">
                          <i class="nc-icon nc-money-coins text-success"></i>
                        </div>
                      </div>
                      <div class="col-7 col-md-8">
                        <div class="numbers">
                          <h2 class="card-category">ผลการประเมิน</h2>
                          <p class="card-title">รอบ : {{ $formname->criteria_season_detail }}
                            </p><p>
                        </p></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div> -->
            <div class="col-lg-12">
            <div class="card card-stats  float-right">
                <div class="card-body">
                  <h5 class="text-center" > ลำดับที่ได้  </h5>
                  <h2>{{request()->key}}</h2>
                </div>
            </div>

            </div>
              <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card card-stats">
                  <div class="card-body ">
                    <div class="row">
                      <div class="col-5 col-md-4">
                        <div class="icon-big text-center icon-warning">
                          <i class="nc-icon nc-money-coins text-success"></i>
                        </div>
                      </div>
                      <div class="col-7 col-md-8">
                        <div class="numbers">
                          <p class="card-category">ชื่อเกษตรกร</p>
                          <p class="card-title"> {{ $score_famer->famer->fname.' '.$score_famer->famer->lname}}
                            </p><p>
                        </p></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card card-stats">
                  <div class="card-body ">
                    <div class="row">
                      <div class="col-5 col-md-4">
                        <div class="icon-big text-center icon-warning">
                          <i class="nc-icon nc-money-coins text-success"></i>
                        </div>
                      </div>
                      <div class="col-3 col-md-12">
                        <div class="numbers">
                          <p class="card-category">ที่อยู่</p>
                          <p class="card-title"> {{ $score_famer->famer->adderss}}
                            </p><p>
                        </p></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card card-stats">
                  <div class="card-body ">
                    <div class="row">
                      <div class="col-5 col-md-4">
                        <div class="icon-big text-center icon-warning">
                          <i class="nc-icon nc-money-coins text-success"></i>
                        </div>
                      </div>
                      <div class="col-7 col-md-8">
                        <div class="numbers">
                          <p class="card-category">เบอร์ติดต่อ</p>
                          <p class="card-title"> {{ $score_famer->famer->phone_number}} 
                            </p><p>
                        </p></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            
            </div>
          <div class="toolbar">
          </div>
          <form id="formq" name="formq" method="POST" action="">
            <table width="100%" border="1" align="center" cellpadding="0" cellspacing="0">
              <tr>
                <td width="15%" rowspan="2" align="center"><strong>หลักเกณฑ์</strong></td>
                
              </tr>
              <tr>
                <td width="15%" align="center"><strong>ค่าน้ำหนัก</strong></td>
                <td width="15%" align="center"><strong>ค่าคะแนนจากเจ้าหน้าที่</strong></td>
                <td width="15%" align="center"><strong>รวม</strong></td>
                
                
               
              </tr>
              @foreach ($score_famer_main as $key=> $item)
                <tr>
                  <td height="30" colspan="3" bgcolor="#F4F4F4"><strong>&ensp;{{ $key + 1 }}. {{$item->Criteria_main->criteria_main_name}}</strong></td>
                  <th height="30" align="center" colspan="1" bgcolor="#F4F4F4">{{$item->topic_score}}</th>
                </tr>
                @foreach ($item->score_famer_main_detail as $keyx=> $itemx)
                
                <tr>
                  <td height="30">&ensp;&ensp; {{ $key + 1 }}.{{ $keyx + 1 }} {{$itemx->get_Criteria_detail->criteria_detail_name}}</td>
                  <td height="30" align="center">{{ $itemx->get_Criteria_detail->criteria_detail_Weight }}</td>
                  <td height="30" align="center">{{ $itemx->answer }}</td>
                  <td height="30" align="center">{{ $itemx->topic_score_detail }}</td>
                  
                </tr>
                
                 
                @endforeach  
                @endforeach  
                <td height="30" align="right" colspan="7" bgcolor="#F4F4F4"><strong>&ensp;ค่าคะแนนรวม : {{$score_famer->criteria_score}}</strong></td>
            </table>
            <hr>
            <br>
          </form>
    </div>
    <div class="col-12">
    <!-- <button class="btn btn-primary col-12 hidden-print" onclick="printElem('invoice-template')">ปริ้น</button> -->
    <!-- <a class="btn btn-info col-12 hidden-print" href="#">ย้อนกลับ</a>
    <a class="btn btn-primary col-12 hidden-print" href="javascript:window.print()">พิมพ์รายงาน</a> -->
</div>
</section>
</div>
</body>
</html>