@extends('layouts.dashboard')
@section('title','หน้าหลัก')
@section('content')


    <div class="row">
      <div class="col-md-12">
        <div class="card">
            <div class="card-header">
              <h3 class="text-center">แนะนำเกษตรกรที่ควรทำสัญญาด้วย</h3>
                <h4 align="center">{{ $Criteria->criteria_season_name }}&ensp;{{ $Criteria->criteria_season_detail }}</h4>
              <a href="{{ route('introducefarmerschooseseason') }}" type="button" class="btn btn-danger">กลับ</a>
                <div class="card-body">
                  <table id="add" class="table text-center">
                    <thead class="thead-dark">
                      <tr>
                        <th scope="col" style="font-size:16px">ลำดับ</th>
                        <th scope="col" style="font-size:16px">ชื่อเกษตรกร</th>
                        <th scope="col" style="font-size:16px">ที่อยู่</th>
                        <th scope="col" style="font-size:16px">เบอร์โทร</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach ($score as $key=>$item)
                        <tr>
                          <th scope="row" style="font-size:16px">{{$key+1}}</th>
                          <td style="font-size:16px">{{$item->famer->fname .' '.$item->famer->lname}}</td>
                          <td style="font-size:16px">{{$item->famer->adderss  }}</td>
                          <td style="font-size:16px">{{$item->famer->phone_number}}</td>
                        </tr>
                      @endforeach
                        </tr>
                    </tbody>
                  </table>
                  </div>
                </div>
        </div>
      </div>
    </div>



@endsection

@push('scripts')
<script>
  $(document).ready( function () {
      $('#add').DataTable();
  } );
</script>
@endpush
