@extends('layouts.dashboard')
@section('title','หน้าหลัก')
@section('content')


<div class="row">
  <div class="col-md-12">
    <div class="card">
        <div class="card-header">
          <h3 class="text-center">เลือกปีที่ต้องการดูรายชื่อเกษตรกรในการทำสัญญา</h3>

        {{-- <a a href="" type="button" class="btn btn-success" style="margin-left: 1400px;">เพิ่มฤดูกาลเพาะปลูก</a> --}}
        </div>
        <div class="card-body">
            <table id="add" class="table text-center">
              <thead class="thead-dark">
                <tr>
                  <th scope="col">ลำดับ</th>
                  <th scope="col">ชื่อฤดูกาล</th>
                  <th scope="col">กำหนดการ</th>
                  <th scope="col">ดูรายชื่อเกษตรกร</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($Report as $key=> $item)
                <tr>
                  <td>{{ $key+1 }}</td>
                  <td>{{ $item->criteria['criteria_season_name'] }}</td>
                  <td>{{ $item->criteria['criteria_season_detail'] }}</td>
                  <td >
                    <a href=" {{ route('selectnumberfarmers',$item->form_criteria_id) }} " type="button" class="btn btn-success">ดูรายชื่อเกษตรกรที่ควรทำสัญญาด้วย</a>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
        </div>
        <!-- end content-->
    </div>
    <!--  end card  -->
   </div>
</div>



@endsection

@push('scripts')
<script>
  $(document).ready( function () {
      $('#add').DataTable();
  } );
</script>
@endpush

