@extends('layouts.master')
@section('title','หน้าหลัก')
@section('content')
<nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
  <div class="container-fluid">
    <div class="navbar-wrapper">
      <div class="navbar-minimize">
        <button id="minimizeSidebar" class="btn btn-icon btn-round">
          <i class="nc-icon nc-minimal-right text-center visible-on-sidebar-mini"></i>
          <i class="nc-icon nc-minimal-left text-center visible-on-sidebar-regular"></i>
        </button>
      </div>
      <div class="navbar-toggle">
        <button type="button" class="navbar-toggler">
          <span class="navbar-toggler-bar bar1"></span>
          <span class="navbar-toggler-bar bar2"></span>
          <span class="navbar-toggler-bar bar3"></span>
        </button>
      </div>
      <a class="navbar-brand" href="#pablo">วิเคราะห์ผลประเมิน</a>
    </div>
  </div>
</nav>
<div class="row">
  <div class="col-md-12">
    <div class="card">
    <div class="card-header">
        <h3 class="text-center"><b>ผลวิเคราะห์ปัจจัยที่ส่งเสริมค่าคะแนนของเกษตรกร</b> </h3>
        <h4 class="text-center">รอบการประเมิน {{ $Criteria->criteria_season_detail }}</h4>
    <a class="btn btn-primary fa fa-print" target="_bank" href="{{route('famer-max-min-print',[$id])}}"> พิมพ์</a>
    <a class="btn btn-danger"href="{{route('chooseseasonmaxmin')}}"> < ย้อนกลับ</a>


    <div class="card">
    
    <div class="card-body">

        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <h3 class="text-center text-success">เกษตรกรที่ได้ผลประเมินสูงที่สุด</h3>
                        <hr>
                    </div>
                    
                        <div class="card-body">
                        
                        <img src="{{ asset('happy_0.png')}}" class="rounded mx-auto d-block" alt="Responsive image">
                        
                    
                    <p class="text-center text-success"> <h4 class="text-center ">{{$score->famer->name_prefix.''.$score->famer->fname.' '.$score->famer->lname}}</h4></p><br>
                    
                    <h5 class="text-left ">ปัจจัยบวก ที่ส่งเสริมให้ผลประเมินดี</h5><br>

                    @foreach($score_detail as $item)
                            <h5 class="text-success"><b><i class="nc-icon nc-check-2 text-success"></i> ด้าน{{$item->get_Criteria_detail->criteria_detail_name}}</b></h5>
                            <!-- <h5 class="text-success"><b><i class="nc-icon nc-check-2 text-success"></i> ปัจจัยรองด้านเปอร์เซ็นต์แป้งในมันสำปะหลัง</b></h5> -->
                            <!-- <p><i class="nc-icon nc-check-2 text-success"></i> ด้าน......... </p>
                            <p><i class="nc-icon nc-check-2 text-success"></i> ด้าน......... </p> -->
                        @endforeach
                            <!-- <p><i class="nc-icon nc-check-2 text-success"></i> ด้าน......... </p>
                            <p><i class="nc-icon nc-check-2 text-success"></i> ด้าน......... </p> -->
                      
                        <br><hr><br>
                        <h5 class="text-center text-success">ค่าคะแนนรวม : <b>{{$score->criteria_score}}</b> </h5>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <h3 class="text-center text-danger">เกษตรกรที่ได้ผลประเมินต่ำที่สุด</h3>
                        <hr>
                    </div>
                    <div class="card-body">
                    <img src="{{ asset('sad_0.png')}}" class="rounded mx-auto d-block" alt="Responsive image">
                    
                    <p class="text-center text-danger"><h4 class="text-center">{{$score->famer->name_prefix.''.$score_min->famer->fname.' '.$score_min->famer->lname}}</h4></p><br>
                    <h5 class="text-left ">ปัจจัยลบ ที่ทำให้ผลประเมินต่ำ</h5><br>
                    
                    @foreach($score_detail_min as $item)
                            <h5 class="text-success"><b><i class="nc-icon nc-check-2 text-success"></i> ด้าน{{$item->get_Criteria_detail->criteria_detail_name}}</b></h5>
                            <!-- <h5 class="text-success"><b><i class="nc-icon nc-check-2 text-success"></i> ปัจจัยรองด้านเปอร์เซ็นต์แป้งในมันสำปะหลัง</b></h5> -->
                            <!-- <p><i class="nc-icon nc-check-2 text-success"></i> ด้าน......... </p>
                            <p><i class="nc-icon nc-check-2 text-success"></i> ด้าน......... </p> -->
                        @endforeach
                    
                        <br><hr><br>
                        <h5 class="text-center text-danger">ค่าคะแนนรวม : <b>{{$score_min->criteria_score}}</b></h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
                </div>
            </div>
        <!-- end content-->
            </div>
    <!--  end card  -->
        </div>
    </div>


@endsection
