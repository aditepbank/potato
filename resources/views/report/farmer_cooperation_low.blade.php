@extends('layouts.db_low_four')
@section('title','หน้าหลัก')
@section('content')

    <div class="row">
      <div class="col-md-4 ml-auto mr-auto">
        <div class="card card-stats">
          <div class="card-body text-center">
            <h4 class="text">เกณฑ์การคัดเลือก</h4>
          </div>
          <div class="col-2 col-md-12">
            <div class="icon-success">
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <i class="fas fa-check-circle fa-2x text-success"></i> &nbsp;&nbsp;&nbsp;&nbsp;<span>ด้านผลิตภัณฑ์ ได้ผลประเมิน ปานกลาง และสูง</span>
              <br>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <i class="fas fa-check-circle fa-2x text-success"></i> &nbsp;&nbsp;&nbsp;&nbsp;<span>ด้านกระบวนการผลิต ได้ผลประเมิน ปานกลาง และสูง</span>
              <br>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <i class="fas fa-times-circle fa-2x text-danger"></i> &nbsp;&nbsp;&nbsp;&nbsp;<span>ด้านการให้ความร่วมมือ ได้ผลประเมินต่ำเรื่องใดเรื่องหนึ่ง</span>
            </div>
          </div>
          <div class="card-footer ">
          </div>
        </div>
      </div>
    </div>

    <div class="card">
    <br>
    <h4 class="text-center">ปัจจัยที่ส่งผลให้เกษตรกรที่ตกคุณสมบัติด้านการให้ความร่วมมือ</h4>
    <br>
    <div class="row">
      <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
          <div class="card-body ">
            <div class="row">
              <div class="col-5 col-md-4">
                <div class="icon-big text-center icon-warning">
                  <i class="fas fa-hands-helping text-primary"></i>
                </div>
              </div>
              <div class="col-7 col-md-8">
                <div class="numbers">
                  <p class="card-category">ด้านความสัมพันธ์กับโรงงาน </p>
                  <p class="card-title">{{ isset($countx[11])?count($countx[11]):0 }} คน
                    </p></div>
              </div>
            </div>
          </div>
          <div class="card-footer ">
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
          <div class="card-body ">
            <div class="row">
              <div class="col-5 col-md-4">
                <div class="icon-big text-center icon-warning">
                  <i class="nc-icon nc-single-02 text-success"></i>
                </div>
              </div>
              <div class="col-7 col-md-8">
                <div class="numbers">
                  <p class="card-category">ด้านทัศนคติต่อการพัฒนาตนเอง </p>
                  <p class="card-title">{{ isset($countx[12])?count($countx[12]):0 }} คน
                    </p></div>
              </div>
            </div>
          </div>
          <div class="card-footer ">
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
          <div class="card-body ">
            <div class="row">
              <div class="col-5 col-md-4">
                <div class="icon-big text-center icon-warning">
                  <i class="fas fa-warehouse text-warning"></i>
                </div>
              </div>
              <div class="col-7 col-md-8">
                <div class="numbers">
                  <p class="card-category">ด้านทัศนคติที่มีต่อโรงงาน</p>
                  <p class="card-title">{{ isset($countx[13])?count($countx[13]):0 }} คน
                    </p><p>
                </p></div>
              </div>
            </div>
          </div>
          <div class="card-footer ">
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
          <div class="card-body ">
            <div class="row">
              <div class="col-5 col-md-4">
                <div class="icon-big text-center icon-warning">
                  <i class="fas fa-file-signature text-danger"></i>
                </div>
              </div>
              <div class="col-7 col-md-8">
                <div class="numbers">
                  <p class="card-category">ด้านทัศนคติที่เกษตรกรมีต่อการทำพันธสัญญา</p>
                  <p class="card-title">{{ isset($countx[14])?count($countx[14]):0 }} คน
                    </p><p>
                </p></div>
              </div>
            </div>
          </div>
          <div class="card-footer ">
            
          </div>
        </div>
      </div>
    </div>
    </div>

    <div class="card">
        <div class="card-body">
          <a class="btn btn-primary btn-lg" target="_bank" href="{{route('cooperation-low-print',[$id])}}"><span class="btn-label"><i class="fas fa-print"></i></i> </span>พิมพ์</a>
    <a class="btn btn-danger btn-lg"href="{{route('choose_season_cooperation_low')}}"> < ย้อนกลับ</a>

        <h3 class="text-center">รายชื่อเกษตรกรที่ตกคุณสมบัติด้านการให้ความร่วมมือ</h3>
        
            <div class="card">
                <div class="card-body">
                    <table id="add" class="table">
                        <thead class="thead-dark" cellspacing="0" width="100%">
                            <tr>
                                <th width=7%>ลำดับ</th>
                                <th width=12%>ชื่อเกษตรกร</th>
                                <th width=25%>ทีอยู่</th>
                                <th width=12%>โทรศัพท์</th>
                                <th width=30%>ปัจจัยที่ส่งผลให้เกษตรกรตกคุณสมบัติด้านการขนส่ง</th> 
                                <th width=30%>เหตุผล</th>
                            </tr>
                        </thead>
                        <tbody>
                          <?php $i = 0 ?>
                        @foreach($score_famer_main_detail as $item)
                            @php
                                if($item->criteria_detail_id <= 6) continue;
                            @endphp
                            <?php $i++ ?>

                            <tr>
                              <td>{{ $i }}.</td>
                              <td>{{ $item->name_prefix.' '.$item->fname.' '.$item->lname }}</td>
                              <td>{{ $item->adderss }}</td>
                              <td>{{ $item->phone_number }}</td>
                              <td>
                                {{ $item->criteria_detail_name  }}
                              </td>
                              <td> {{ $item->criteria_detail_low }} </td>
                            </tr>
                        @endforeach   
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script>
  $(document).ready( function () {
      $('#add').DataTable();
  } );
</script>
@endpush



