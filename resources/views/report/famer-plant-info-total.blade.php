@extends('layouts.dashboard')
@section('title','หน้าหลัก')
@section('content')


<div class="row">
  <div class="col-md-12">
    <div class="card">
        <div class="card-header">
          <h3 class="text-center">สรุปข้อมูลการปลูกมันสำปะหลัง</h3>
          
            <div class="card-body">
                <table class="table text-center">
                  <thead class="thead-dark">
                    <tr>
                      <th scope="col">ลำดับ</th>
                      <th scope="col">รหัสเกษตร</th>
                      <th scope="col">ชื่อ - สกุล</th>
                      <th scope="col">ข้อมูลพันธุ์ปลูกมันสำปะหลัง</th>
                      <th scope="col">วันที่เริ่มปลูก</th>
                      <th scope="col">ลักษณะการปลูก </th>
                      <th scope="col">ปุ๋ยแต่งหน้า :</th>
                      <th scope="col">จำนวนครั้งที่ใส่ปุ๋ย : </th>
                      <th scope="col">ความสูงของต้นมัน </th>
                      <th scope="col">น้ำหนัก กก./ต้น :</th>
                      <th scope="col">คะแนนที่ได้ </th>
                      
                      
                      <th class="disabled-sorting text-right">Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                  @foreach ($score_famer as $key=>$item)
                    <tr>
                      <th scope="row">{{$key+1}}</th>
                      <td>{{$item->famer->famer_code}}</td>
                      <td>{{$item->famer->fname.' '.$item->famer->lname}}</td>
                      <td>{{$item->planting_information->breed_type}}</td>
                      <td>{{$item->planting_information->date_plant}}</td>
                      <td>{{$item->planting_information->Planting_characteristics}}</td>
                      <td>{{$item->plantation_management_information->Make_up_fertilizer}}</td>
                      <td>{{$item->plantation_management_information->Weeding_about_people_cal}}</td>
                      <td>{{$item->harvest_information->high_meter}}</td>
                      <td>{{$item->harvest_information->weight_potato}}</td>
                      <td>{{$item->criteria_score}}</td>

                      
                      <td class="text-right">
                        <a href="{{route('famer-plant-info-detail',$item->id)}}" class="btn btn-info">เพิ่มเติม</a>
                      </td>
                      
                    </tr>
                    @endforeach
                  </tbody>
                </table>


      </div>
    </div>
        <!-- end content-->
    </div>
    <!--  end card  -->
  </div>
</div>



@endsection

@push('scripts')
<script>
  $(document).ready( function () {
      $('#add').DataTable();
  } );
</script>
@endpush