@extends('layouts.master')
@section('title','หน้าหลัก')
@section('content')
<nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
  <div class="container-fluid">
    <div class="navbar-wrapper">
      <div class="navbar-minimize">
        <button id="minimizeSidebar" class="btn btn-icon btn-round">
          <i class="nc-icon nc-minimal-right text-center visible-on-sidebar-mini"></i>
          <i class="nc-icon nc-minimal-left text-center visible-on-sidebar-regular"></i>
        </button>
      </div>
      <div class="navbar-toggle">
        <button type="button" class="navbar-toggler">
          <span class="navbar-toggler-bar bar1"></span>
          <span class="navbar-toggler-bar bar2"></span>
          <span class="navbar-toggler-bar bar3"></span>
        </button>
      </div>
      <a class="navbar-brand" href="#pablo">รายละเอียดเกษตรกร ด้าน{{$Criteria_main->criteria_main_name}}</a>
    </div>
  </div>
</nav>


<div class="row">
  <div class="col-md-12">
    <div class="card">
        <div class="card-header">
        <div class="row">
          <div class="col-md-1"> <a class="btn btn-primary fa fa-print"  target="_bank" href="{{route('criteria-max-print',[$form_id,request()->topic_id])}}"> พิมพ์</a></div>
          <div class="col-md-1"> <a class="btn btn-danger"href="{{route('report-criteria-best', $form_id)}}"> < ย้อนกลับ</a></div>
          <div class="col-md-11"> <h3 class="text-center">10 อันดับเกษตรกรที่มีคุณสมบัติเด่น ด้าน{{$Criteria_main->criteria_main_name}}</h3>
          
          </div>
        </div>
          <!-- <h3 class="text-center">10 อันดับเกษตรกรที่มีคุณสมบัตรเด่น ด้านผลิตภัณฑ์</h3> -->

            <div class="card-body">
            <div class="dropdown">
          

            <hr>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    
                       <button class="dropdown-item" value= ""></button>
                   
            </div>
            </div>
            <div class="content">
            <table class="table text-center">
                <thead class="thead-dark text-center">
               
                    <tr>
                    <th scope="col">ลำดับที่</th>
                   
                    <th scope="col">ชื่อเกษตรกร</th>
                    <th scope="col">ที่อยู่</th>
                    <th scope="col">โทรศัพท์</th>
                    <th scope="col">ปัจจัยที่ส่งผลให้ได้ผลประเมินดี</th>
                    </tr>
                    
                </thead>
                
                
            <tbody>
            @php
                $i = 1;
            @endphp
            @foreach($resultx as $key=>$item)
                <tr>
                    <td scope="row">{{$i}}</td>
                 
                    <th>{{$item->name_prefix.' '.$item->fname.' '.$item->lname}}</th>
                    <td>{{$item->adderss}}</td>
                    <td>{{$item->phone_number}}</td>
                    <th>
                        @foreach($data[$item->famer_id] as $item)
                          <p>{{ $item }} </p>
                        @endforeach
                    </th>
                    
                    
                    
                    
                    
                </tr>
                @php
                            $i++;
                            if($i > 10)break;
                            @endphp
                @endforeach
              </tbody>
            
            </table>
            </div>
        </div>
        <!-- end content-->
    </div>
    <!--  end card  -->
  </div>
</div>
<!-- <script>
    $(document).ready(function() {
      // Javascript method's body can be found in assets/assets-for-demo/js/demo.js
      demo.initChartPageCharts();
    });
  </script> -->
@endsection
