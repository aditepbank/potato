<?php


use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Faker\Generator as Faker;


class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $fake)
    {
        for($i=1;$i<=10;$i++){
            DB::table('users')->insert([
                
                'email' => Str::random(10).'@gmail.com',
                'fname' => $fake->firstName,
                'lname' => $fake->lastName,
                'password' => bcrypt('password'),
            ]);

        }
        
    }
}
