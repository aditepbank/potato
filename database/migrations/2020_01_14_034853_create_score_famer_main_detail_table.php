<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScoreFamerMainDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('score_famer_main_detail', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('score_criteria_main_id');
            $table->integer('famer_id');
            $table->integer('form_id');
            $table->integer('topic_id');
            $table->integer('criteria_detail_id');
            $table->float('topic_score_detail');
            $table->float('answer');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('score_famer_main_detail');
    }
}
