<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlantationManagementInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plantation_management_information', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('famer_id');
            $table->integer('plow_da');
            $table->integer('plow_pea');
            $table->string('Foundation_fertilizer', 100);
            $table->integer('Groove');
            $table->integer('about_farm');
            $table->string('give_water', 100);
            $table->integer('give_water_calculate');
            $table->string('Make_up_fertilizer', 100);
            $table->string('Make_up_fertilizer_about', 100);
            $table->string('Weeding', 100);
            $table->string('Weeding_about_people', 100);
            $table->integer('Weeding_about_people_cal');
            $table->string('Month_harvest', 100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plantation_management_information');
    }
}
