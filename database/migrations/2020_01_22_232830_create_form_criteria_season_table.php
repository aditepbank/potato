<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormCriteriaSeasonTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('form_criteria_season', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('famer_id');
            $table->integer('criteria_id');
            $table->enum('status', ['start','success']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('form_criteria_season');
    }
}
