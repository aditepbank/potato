<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateManagementGardenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('management_gargen', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('famer_id');
            $table->integer('Plow'); 
            $table->integer('plowpal'); 
            $table->integer('Groove'); 
            $table->string('Foundation_fertilizer', 100);
            $table->string('about', 100); 
            $table->string('Irrigation', 100); 
            $table->integer('Calculate');
            $table->string('Make-up_fertilizer', 100);
            $table->string('about_2', 100); 
            $table->integer('Calculate_2');
            $table->string('Weeding', 100); 
            $table->string('About_people', 100); 
            $table->integer('Calculate_3');
            $table->string('Expected_harvest_month', 100); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('management_gargen');
    }
}
