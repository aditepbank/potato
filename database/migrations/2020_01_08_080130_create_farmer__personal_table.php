<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFarmerPersonalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('famer', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('famer_code');
            $table->enum('name_prefix',['นาย', 'นาง', 'นางสาว']);
            $table->string('fname', 100);
            $table->string('lname', 100);
            $table->enum('gender', ['male', 'female']);
            $table->string('status', 100);
            $table->dateTime('birthday');
            $table->integer('number_chil');
            $table->string('adderss', 100);
            $table->string('education', 100);
            $table->string('phone_number', 10);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('famer_Personal');
    }
}
