<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScoreFamerMainTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('score_famer_main', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('famer_id');
            $table->integer('form_id');
            $table->integer('score_id');
            $table->integer('topic');
            $table->float('topic_score');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('score_famer_main');
    }
}
