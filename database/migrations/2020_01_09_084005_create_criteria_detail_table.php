<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCriteriaDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {   
        //กกกกก
        Schema::create('criteria_detail', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_detail');
            $table->integer('id_season');
            $table->text('criteria_detail_name');
            $table->text('criteria_detail_low');
            $table->text('criteria_detail_middle');
            $table->text('criteria_detail_high');
            $table->float('criteria_detail_Weight', 8,3);
            $table->text('criteria_main_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('criteria_detail');
    }
}
