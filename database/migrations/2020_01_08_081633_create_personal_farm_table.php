<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePersonalFarmTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('garden_detail', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('famer_id');
            $table->integer('distance');
            $table->string('latitude', 100);
            $table->string('longtitude', 100);
            $table->integer('total_garden');
            $table->string('soli_look', 100);
            $table->string('HP', 100);
            $table->string('enviroment', 100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personal_farm');
    }
}
