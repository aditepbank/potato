<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCriteriaMainTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('criteria_main', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_main');
            $table->text('criteria_main_name');
            $table->float('criteria_main_Weight', 8,3);
            $table->text('criteria_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('criteria_main');
    }
}
