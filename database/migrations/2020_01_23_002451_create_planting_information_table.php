<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlantingInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('planting_information', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('famer_id');
            $table->string('breed_type', 100);
            $table->string('Planting_distance', 100);
            $table->dateTime('date_plant');
            $table->string('Planting_characteristics', 100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('planting_information');
    }
}
