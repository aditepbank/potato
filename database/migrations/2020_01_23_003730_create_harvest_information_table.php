<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHarvestInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('harvest_information', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('famer_id');
            $table->string('age_potato', 100);
            $table->integer('high_meter');
            $table->integer('calculate_potato');
            $table->float('weight_potato');
            $table->string('Evaluate_products', 100);
            $table->string('Trend_productivity', 100);
            $table->string('Provider_Name', 100);
            $table->string('Data_collector_name', 100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('harvest_information');
    }
}
