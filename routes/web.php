<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});
// return view('home');

route::get('dashboard','DashboardController@test')->name('dashboard');
route::get('dashboard/get_data/{id}','DashboardController@get_data_dashboard')->name('dashboard.get_data_dashboard');

route::get('logout', function () {
    Auth::logout();
    return redirect()->route('login');
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


// form criteria season
route::get('formcriteriaseason','CriteriaSeasonController@index')->name('formcriteriaseason');
route::get('addfamerformcriteriaseason/{id}','CriteriaSeasonController@addfamer')->name('addfamer');
route::post('addfamer/formcriteriaseason/{id}','CriteriaSeasonController@store')->name('addfamerpost');






// Reportscore
route::get('report','RateController@scorce')->name('farmerscorereport');
route::get('report/detailfarmerscorereport/{id}/{form}','ReportController@detailfarmerscorereport')->name('detailfarmerscorereport');
route::get('plant-info-total','ReportController@plan_total')->name('famer-plant-info-total');
Route::GET('score/report/{id}/{form}','ReportController@report_score_famer')->name('report-score');
// เลือกฤดูกาลเพื่อที่จะดูสรุปการประเมิน
route::get('choose/season/report','ReportController@chooseseasonreport')->name('chooseseasonreport');
route::get('choose/season/reportx','ReportController@chooseseasonreportx')->name('chooseseasonreportx');
// ดูคะแนนเกษตรกร
route::get('plant-info-detail/{id}','ReportController@plan_detail')->name('famer-plant-info-detail');
Route::get('report/graph','ReportController@showGraph')->name('report-graph');
Route::get('report/score-famer-total/{id}','ReportController@report_score_famer_total')->name('report-total');
route::get('report/point/farmer/{id}','ReportController@reportpointfarmer')->name('reportpointfarmer');
route::get('report/score-famer','ReportController@reportpointfarmer_total')->name('reportpointfarmer-total');
route::get('report/point/transport/{id}','ReportController@reportpointtransport')->name('reportpointtransport');

route::get('report/point/index','ReportController@reportpointindex')->name('reportpointindex');
// แนะนำเกษตรกร
route::get('introducefarmers/chooseseason','ReportController@introducefarmerschooseseason')->name('introducefarmerschooseseason');
route::get('select/numberfarmers/{id}','ReportController@selectnumberfarmers')->name('selectnumberfarmers');
route::get('introduce/farmer/{id}','ReportController@introduce')->name('introduce');
route::get('report-criteria-best/{id}','ReportController@report_criteria_best')->name('report-criteria-best');

route::get('criterria/search/{id}','ReportController@criterriasearch')->name('criterriasearch');
// เกษตรกรที่มีความร่วมมือต่ำ
route::get('/choooseseasoncooperationlow','ReportController@choose_season_cooperation_low')->name('choose_season_cooperation_low');
route::get('/farmer_cooperation_low/{id}','ReportController@farmer_cooperation_low')->name('farmer_cooperation_low');

route::get('/chooseseasonmax','ReportController@chooseseasonmax')->name('chooseseasonmax');
route::get('/chooseseasonmin','ReportController@chooseseasonmin')->name('chooseseasonmin');
route::get('/chooseseasonmaxmin','ReportController@chooseseasonmaxmin')->name('chooseseasonmaxmin');


route::get('select-famer','ReportController@select_famer')->name('select-famer');
route::get('report-product-best/form_id/{form_id}/topic/{topic_id}','ReportController@report_product_best')->name('report-product-best');


Route::get('report/transport/{id}', 'ReportController@indextransport')->name('report/transport');
Route::get('report/transport1/form_id/{form_id}/topic/{topic_id}', 'ReportController@indextransport1')->name('report/transport1');


Route::get('report-cuttom/{id}', 'ReportController@report_cuttom')->name('report-cuttom');


//rate_Agriculture
Route::get('/rate_famer/{id}', 'RateController@rate_famer')->name('rate_famer');
Route::post('/rate_famer/{id}', 'RateController@rate_famer_cal');
Route::get('/rate_Agriculture', 'RateController@index')->name('rate_Agriculture-list');
Route::get('/rate_add_point', 'RateController@create')->name('rate_add_point-get');
// เลือกฤดูกาลที่จะประเมินเกษตรกร
Route::get('chooseseasoncriteria', 'RateController@choose')->name('choose');
// เลือกรายชื่อเกษตรกรเพื่อประเมิน
Route::get('choosefarmercriteria/{id}', 'RateController@choosefarmer')->name('choosefarmer');




//Famer
Route::get('/famer-list','FamerController@index')->name('famer-list');
Route::get('/famer-list-information/{id}','FamerController@information')->name('famer-list-information');
Route::get('/famer-add','FamerController@create')->name('famer-add');
Route::post('/famer-add-post','FamerController@store')->name('famer-add-post');
Route::POST('/famer-edit/{id}','FamerController@update')->name('famer-update-post');
Route::GET('/famer-edit/{id}','FamerController@edit')->name('famer-edit-get');
Route::get('/famer-add-plan-info/{id}','PlantingInformationController@create')->name('famer-add-plan-info');
Route::post('/famer-add-plan-info-post/{id}','PlantingInformationController@store')->name('famer-add-plan-info-post');
Route::get('/famer-list-plan','FamerController@index_plan')->name('famer-list_plan');
// ลบเกษตรกร
Route::get('famer-delete/{id}', 'FamerController@destroy')->name('famer-delete');








//rules
Route::get('criteria', 'CriteriaController@index')->name('criteria');

// สร้างฟอร์มการประเมิน
Route::get('create/criterai', 'CriteriaController@createcriteria')->name('createcriteria');
route::post('create/criteria/post','CriteriaController@store')->name('createcriteriapost');
route::post('update/criteria','CriteriaController@update')->name('updatecriteria');
route::post('deletecriteria','CriteriaController@destroy')->name('delete');
// สร้างปัจจัยหลักในการประเมิน
route::get('criteriamain/{id}','CriteriaMainController@index')->name('criteriamain');
// route::get('criteriamain/{id}','CriteriaMainController@back')->name('criteriamainback');
route::get('create/criteriamain/{id}','CriteriaMainController@createcriteriamain')->name('createcriteriamain');
route::post('create/criteriamain/post/{id}','CriteriaMainController@store')->name('createcriteriamainpost');
route::post('update/criteriamain','CriteriaMainController@update')->name('updatecriteriamain');
route::post('deletecriteriamain','CriteriaMainController@destroy')->name('deletemain');
// สร้างปัจจัยรองในการประเมิน
route::get('criteriadetail/{testid}/{id}','CriteriaDetailController@index')->name('criteriadetail');
route::get('create/criteriadetail/{id}','CriteriaDetailController@createcriteriadetail')->name('createcriteriadetail');
route::post('create/criteriadetail/post/{testid}/{id}','CriteriaDetailController@store')->name('createcriteriadetailpost');
Route::post('edit/criteriadetail','CriteriaDetailController@update')->name('editcriteriadetail');
route::post('deletecriteriadetail','CriteriaDetailController@destroy')->name('deletecriteriadetail');
// ข้อมูลฟอร์มการประเมินแต่ละฤดูกาล
route::get('info/criteria/{id}','CriteriaController@info')->name('infocriteria');


Route::get('/rules_add', 'RulesController@rulesadd')->name('rules_add');




Route::get('line-chart', 'ChartController@pre_chart');


//factory
Route::get('/factory-detail', 'FactoryController@index')->name('factory-detail');


route::get('report/test','DashboardController@index')->name('x');

//print
route::get('score-famer/print/{id}','PrintController@score_famer')->name('score-famer-print');
route::get('criteria-max/print/form_id/{form_id}/topic/{topic_id}','PrintController@criteria_max')->name('criteria-max-print');
route::get('criteria-min/print/form_id/{form_id}/topic/{topic_id}','PrintController@criteria_min')->name('criteria-min-print');
Route::get('famer-max-min/print/{id}', 'PrintController@famer_max_min')->name('famer-max-min-print');
route::get('point-transport-low/print/{id}','PrintController@transport_low')->name('transport-low-print');
route::get('cooperation-low/print/{id}','PrintController@cooperation_low')->name('cooperation-low-print');


